webpackJsonp([1],{

/***/ 10:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ParamService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__langage_fr_FR_json__ = __webpack_require__(313);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__langage_fr_FR_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__langage_fr_FR_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__langage_en_GB_json__ = __webpack_require__(314);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__langage_en_GB_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__langage_en_GB_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__langage_es_ES_json__ = __webpack_require__(315);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__langage_es_ES_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__langage_es_ES_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__langage_el_GR_json__ = __webpack_require__(316);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__langage_el_GR_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__langage_el_GR_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_common_http__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser__ = __webpack_require__(19);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// the parameter script







var ParamService = /** @class */ (function () {
    function ParamService(httpClient, sanitizer) {
        this.httpClient = httpClient;
        this.sanitizer = sanitizer;
        this.tableduration = {};
        this.tablerobot = {};
        this.mail = {};
        this.emotion = {};
        this.phone = {};
        this.currentduration = {};
        this.olddata = {};
        this.walkuser = {};
        this.tablewalklogs = {};
        this.users = [];
        this.tablewalkuser = {};
        this.durationNS = {}; //NS for not send
        this.tabledurationNS = {};
        this.maillist = [];
        this.phonenumberlist = [];
        this.cpt = 0;
        this.sendduration = false;
    }
    ParamService.prototype.getDataRobot = function () {
        var _this = this;
        this.httpClient.get("http://localhost/ionicDB/getrobot.php").subscribe(function (data) {
            //console.log("robot:"+data);
            _this.robot = data[0];
            _this.langage = _this.robot.langage;
            if (_this.langage === "fr-FR") {
                _this.datatext = __WEBPACK_IMPORTED_MODULE_0__langage_fr_FR_json___default.a;
            }
            else if (_this.langage === "en-GB") {
                _this.datatext = __WEBPACK_IMPORTED_MODULE_1__langage_en_GB_json___default.a;
            }
            else if (_this.langage === "el-GR") {
                _this.datatext = __WEBPACK_IMPORTED_MODULE_3__langage_el_GR_json___default.a;
            }
            else if (_this.langage === "es-ES") {
                _this.datatext = __WEBPACK_IMPORTED_MODULE_2__langage_es_ES_json___default.a;
            }
            if (_this.robot.tokenASAPA == null) {
                var headers = { 'X-Shapes-Key': '7Msbb3w^SjVG%j' };
                console.log("testapiloginstart");
                var body = { "email": _this.robot.loginASAPA, "password": _this.robot.passASAPA };
                _this.httpClient.post("https://kubernetes.pasiphae.eu/shapes/asapa/auth/login", body, { headers: headers }).subscribe(function (data) {
                    console.log(data["items"]);
                    //console.log(data.items);
                    console.log(data["items"][0].token);
                    _this.tokenAsapa = data["items"][0].token;
                    _this.savetok();
                    //enregistrer token
                    //console.log(data[items][0]);
                }, function (error) {
                    console.log('error : ', error);
                });
            }
            else {
                _this.tokenAsapa = _this.robot.tokenASAPA;
            }
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.updateRobot = function () {
        this.tablerobot.action = "update";
        this.tablerobot.langage = this.robot.langage;
        this.tablerobot.serialnumber = this.serialnumber;
        this.tablerobot.name = this.name;
        this.tablerobot.allowspeech = this.robot.allowspeech;
        this.tablerobot.send_pic = this.robot.send_pic;
        this.tablerobot.password = this.robot.password;
        this.httpClient
            .post("http://localhost/ionicDB/updaterobotpassword.php", JSON.stringify(this.tablerobot))
            .subscribe(function (data) {
            console.log(data);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.getPhone = function () {
        var _this = this;
        this.httpClient.get("http://localhost/ionicDB/getphone.php").subscribe(function (data) {
            if (_this.phonenumberlist.length === 0) {
                for (var value in data) {
                    _this.phonenumberlist.push(data[value].phonenumber);
                }
            }
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.addMail = function (m) {
        this.mail.action = "insert";
        this.mail.mail = m;
        this.mail.serialnumber = this.serialnumber;
        this.httpClient
            .post("http://localhost/ionicDB/addmail.php", JSON.stringify(this.mail))
            .subscribe(function (data) {
            console.log(data);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.addUser = function (user, login, password) {
        this.tablewalkuser.action = "insert";
        this.tablewalkuser.username = user;
        this.tablewalkuser.login = login;
        this.tablewalkuser.password = password;
        this.httpClient
            .post("http://localhost/ionicDB/guidedwalk/addusershapes.php", JSON.stringify(this.tablewalkuser))
            .subscribe(function (data) {
            console.log(data);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.addEmotion = function (sad, happy, anger, surprise, neutral) {
        var _this = this;
        this.emotion.action = "insert";
        this.emotion.sad_emotion = sad;
        this.emotion.happy_emotion = happy;
        this.emotion.anger_emotion = anger;
        this.emotion.surprise_emotion = surprise;
        this.emotion.neutral_emotion = neutral;
        this.emotion.time_emotion = new Date().toLocaleTimeString();
        console.log(this.emotion);
        this.httpClient
            .post("http://localhost/ionicDB/emotions/addemotion.php", JSON.stringify(this.emotion))
            .subscribe(function (data) {
            console.log(data);
            _this.senddataemotion();
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.senddataemotion = function () {
        var _this = this;
        this.httpClient
            .post("http://localhost/ionicDB/emotions/getemotion.php", "")
            .subscribe(function (data) {
            console.log(data);
            console.log(data[0].id_emotion);
            var datenow = new Date(Date.now());
            console.log(datenow.toISOString());
            var dateemotion = datenow;
            var listtime = data[0].time_emotion.split(':');
            dateemotion.setHours(listtime[0], listtime[1], listtime[2]);
            console.log(dateemotion.toISOString());
            var body = {
                "anger_emotion": data[0].anger_emotion,
                "devId": _this.robot.devID_ASAPA,
                "happy_emotion": data[0].happy_emotion,
                "id_emotion": data[0].id_emotion,
                "id_walklogs": data[0].id_walklogs,
                "neutral_emotion": data[0].neutral_emotion,
                "sad_emotion": data[0].sad_emotion,
                "surprise_emotion": data[0].surprise_emotion,
                "time": datenow.toISOString(),
                "time_emotion": dateemotion.toISOString()
            };
            console.log(body);
            var headers = { 'X-Authorisation': _this.tokenAsapa };
            _this.httpClient.post("https://symbiote.shapes.f-in.io:9071/v1/kompai/emotion", body, { headers: headers }).subscribe(function (data) {
                console.log(data);
                //enregistrer token
                //console.log(data[items][0]);
            }, function (error) {
                console.log('error : ', error);
            });
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.senddatawalking = function () {
        var _this = this;
        this.httpClient
            .post("http://localhost/ionicDB/getdatawalking.php", "")
            .subscribe(function (data) {
            console.log(data);
            console.log(data[0].walklogs_id);
            var testdate = new Date(data[0].walk_date);
            console.log(testdate);
            var datenow = new Date(Date.now());
            console.log(datenow.toISOString());
            var headers = { 'X-Authorisation': _this.tokenAsapa };
            var body = {
                "devId": _this.robot.devID_ASAPA,
                "time": data[0].walk_date,
                "walk_date": data[0].walk_date,
                "walk_map": data[0].walk_map,
                "walk_meters": data[0].walk_meters,
                "walk_numberofpauses": data[0].walk_numberofpauses,
                "walk_round": data[0].walk_round,
                "walk_speed": data[0].walk_speed,
                "walk_time": testdate.toISOString(),
                "walk_timeofpauses": data[0].walk_timeofpauses,
                "walklogs_id": data[0].walklogs_id,
                "walkuser_id": data[0].walkuser_id
            };
            _this.httpClient.post("https://symbiote.shapes.f-in.io:9071/v1/kompai/walking", body, { headers: headers }).subscribe(function (data) {
                console.log(data);
                //enregistrer token
                //console.log(data[items][0]);
            }, function (error) {
                console.log('error : ', error);
            });
            console.log(body);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.deleteMail = function (m) {
        this.mail.action = "delete";
        this.mail.mail = m;
        this.httpClient
            .post("http://localhost/ionicDB/deletemail.php", JSON.stringify(this.mail))
            .subscribe(function (data) {
            console.log(data);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.deleteUser = function (user) {
        this.tablewalkuser.action = "delete";
        this.tablewalkuser.username = user;
        this.httpClient
            .post("http://localhost/ionicDB/guidedwalk/deleteusershapes.php", JSON.stringify(this.tablewalkuser))
            .subscribe(function (data) {
            console.log(data);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.addPhone = function (m) {
        this.phone.action = "insert";
        this.phone.phonenumber = m;
        this.phone.serialnumber = this.serialnumber;
        this.httpClient
            .post("http://localhost/ionicDB/addphone.php", JSON.stringify(this.phone))
            .subscribe(function (data) {
            console.log(data);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.deletePhone = function (m) {
        this.phone.action = "delete";
        this.phone.phonenumber = m;
        this.httpClient
            .post("http://localhost/ionicDB/deletephone.php", JSON.stringify(this.phone))
            .subscribe(function (data) {
            console.log(data);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.getMail = function () {
        var _this = this;
        this.httpClient.get("http://localhost/ionicDB/getmail.php").subscribe(function (data) {
            if (_this.maillist.length === 0) {
                for (var value in data) {
                    _this.maillist.push(data[value].mail);
                }
            }
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.getWalkUser = function () {
        var _this = this;
        console.log("get user");
        this.httpClient.get("http://localhost/ionicDB/guidedwalk/getwalkusershapes.php").subscribe(function (data) {
            _this.users = [];
            for (var value in data) {
                _this.users.push(data[value].username);
            }
            _this.walkuser = data;
            console.log(data);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.getWalkLogs = function () {
        return this.httpClient.get("http://localhost/ionicDB/guidedwalk/getwalklogs.php");
    };
    ParamService.prototype.getWalkLogsByUser = function (e) {
        //console.log(e);
        return this.httpClient.get("http://localhost/ionicDB/guidedwalk/getwalklogsbyid.php/?user=" + e);
    };
    ParamService.prototype.addwalklog = function (map, speed) {
        this.tablewalklogs.action = "insert";
        //this.tablewalklogs.walkuser_id = user;
        this.tablewalklogs.walk_meters = 0;
        this.tablewalklogs.walk_numberofpauses = 0;
        this.tablewalklogs.walk_time = 0;
        this.tablewalklogs.walk_timeofpauses = 0;
        this.tablewalklogs.walk_map = map;
        //this.tablewalklogs.walk_round = round;
        this.tablewalklogs.walk_speed = speed;
        this.tablewalklogs.date = new Date().toLocaleDateString("fr-CA");
        this.httpClient
            .post("http://localhost/ionicDB/guidedwalk/addwalklog.php", JSON.stringify(this.tablewalklogs))
            .subscribe(function (data) {
            console.log(data);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.updatewalklog = function () {
        this.tablewalklogs.action = "update";
        this.httpClient
            .post("http://localhost/ionicDB/guidedwalk/updatewalklog.php", JSON.stringify(this.tablewalklogs))
            .subscribe(function (data) {
            console.log(data);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.addDuration = function () {
        this.tableduration.action = "insert";
        this.tableduration.serialnumber = this.serialnumber;
        this.tableduration.round = this.currentduration.round;
        this.tableduration.battery = this.currentduration.battery;
        this.tableduration.walk = this.currentduration.walk;
        this.tableduration.patrol = this.currentduration.patrol;
        this.tableduration.date = this.currentduration.date;
        this.httpClient
            .post("http://localhost/ionicDB/addduration.php", JSON.stringify(this.tableduration))
            .subscribe(function (data) {
            console.log(data);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.getDurationNS = function () {
        var _this = this;
        this.httpClient.get("http://localhost/ionicDB/duration/getduration.php").subscribe(function (data) {
            _this.durationNS = data; // NS =not send
            //console.log(data);
            //console.log(this.durationNS.length);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.updateDurationNS = function (id) {
        this.tabledurationNS.action = "update";
        this.tabledurationNS.id_duration = id;
        this.httpClient
            .post("http://localhost/ionicDB/duration/updateduration.php", JSON.stringify(this.tabledurationNS))
            .subscribe(function (data) {
            console.log(data);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.getDuration = function () {
        var _this = this;
        this.httpClient.get("http://localhost/ionicDB/getduration.php").subscribe(function (data) {
            _this.duration = data;
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.updateDuration = function () {
        this.tableduration.action = "update";
        this.tableduration.serialnumber = this.serialnumber;
        this.tableduration.round = this.currentduration.round;
        this.tableduration.battery = this.currentduration.battery;
        this.tableduration.walk = this.currentduration.walk;
        this.tableduration.patrol = this.currentduration.patrol;
        this.tableduration.date = this.currentduration.date;
        this.httpClient
            .post("http://localhost/ionicDB/updateduration.php", JSON.stringify(this.tableduration))
            .subscribe(function (data) {
            console.log(data);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.getBattery = function () {
        var _this = this;
        this.httpClient.get("http://localhost/ionicDB/getbattery.php").subscribe(function (data) {
            _this.battery = data[0];
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.fillData = function () {
        this.name = this.robot.name;
        this.allowspeech = this.robot.allowspeech;
        this.maildatapassw = atob(this.robot.maildatapass);
        this.mailrobotpassw = atob(this.robot.mailrobotpass);
        this.localhost = this.robot.localhost;
        this.serialnumber = this.robot.serialnumber;
        this.robotmail = this.robot.mailrobot;
        this.datamail = this.robot.maildata;
        this.datamaillist = ["data@kompai.com"];
        this.source = this.sanitizer.bypassSecurityTrustResourceUrl(this.datatext.URL_assistancemobility);
        if (this.duration.length > 0) {
            if (this.duration[this.duration.length - 1].date ===
                new Date().toLocaleDateString("fr-CA")) {
                this.currentduration.date = this.duration[this.duration.length - 1].date;
                this.currentduration.round = this.duration[this.duration.length - 1].round;
                this.currentduration.battery = this.duration[this.duration.length - 1].battery;
                this.currentduration.patrol = this.duration[this.duration.length - 1].patrol;
                this.currentduration.walk = this.duration[this.duration.length - 1].walk;
                this.currentduration.toolbox = this.duration[this.duration.length - 1].toolbox;
            }
            else {
                if (!this.sendduration) {
                    this.sendduration = true;
                    this.init_currentduration();
                    this.addDuration();
                }
            }
        }
        else {
            if (!this.sendduration) {
                this.sendduration = true;
                this.init_currentduration();
                this.addDuration();
            }
        }
    };
    ParamService.prototype.init_currentduration = function () {
        this.currentduration.round = 0;
        this.currentduration.battery = 0;
        this.currentduration.patrol = 0;
        this.currentduration.walk = 0;
        this.currentduration.toolbox = 0;
        this.currentduration.date = new Date().toLocaleDateString("fr-CA");
    };
    ParamService.prototype.cptDuration = function () {
        if (this.currentduration.date === new Date().toLocaleDateString("fr-CA")) {
            this.cpt = parseInt(this.currentduration.walk);
            this.currentduration.walk = this.cpt + 2;
            this.updateDuration();
        }
        else {
            this.init_currentduration();
            this.addDuration();
        }
    };
    // ASAPA function
    ParamService.prototype.savetok = function () {
        var tableupdate = {};
        tableupdate.action = "update";
        tableupdate.tokenASAPA = this.tokenAsapa;
        var dateno = new Date(Date.now());
        tableupdate.tokenASAPA_date = dateno.toISOString().slice(0, 10);
        console.log(tableupdate.tokenASAPA_date);
        this.httpClient
            .post("http://localhost/ionicDB/settoken.php", JSON.stringify(tableupdate))
            .subscribe(function (data) {
            console.log(data);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_4__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser__["c" /* DomSanitizer */]])
    ], ParamService);
    return ParamService;
}());

//# sourceMappingURL=param.service.js.map

/***/ }),

/***/ 115:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StartAssistancePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_api_service__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_alert_service__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_popup_service__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_param_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_speech_service__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__walk_walk__ = __webpack_require__(220);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var StartAssistancePage = /** @class */ (function () {
    function StartAssistancePage(speech, popup, alert, param, navCtrl, api) {
        var _this = this;
        this.speech = speech;
        this.popup = popup;
        this.alert = alert;
        this.param = param;
        this.navCtrl = navCtrl;
        this.api = api;
        this.videoWidth = 0;
        this.videoHeight = 0;
        this.walkPage = __WEBPACK_IMPORTED_MODULE_7__walk_walk__["a" /* WalkPage */];
        this.api.metercovered = 0;
        this.updateinterv = setInterval(function () { return _this.getUpdate(); }, 500);
        this.speechinterv = setInterval(function () { return _this.sayorder(); }, 5000);
        this.api.inUse = true;
        this.timecountdown = 3;
        this.speech.getVoice();
        // declecnche la popup de permission camera si besoin
        this.firstcount = false;
    }
    StartAssistancePage.prototype.ngOnInit = function () {
    };
    StartAssistancePage.prototype.ionViewWillLeave = function () {
        clearInterval(this.updateinterv);
        clearInterval(this.speechinterv);
    };
    StartAssistancePage.prototype.ionViewDidEnter = function () {
        this.timecountdown = 3;
    };
    StartAssistancePage.prototype.sayorder = function () {
        if (this.api.position_state < 2) {
            this.speech.speak(this.param.datatext.closer);
        }
    };
    StartAssistancePage.prototype.getUpdate = function () {
        var _this = this;
        if (!this.api.walker_states.detected) {
            this.api.position_state = 0;
            if (this.firstcount) {
                clearInterval(this.intervalcountdown);
                this.firstcount = false;
            }
            if (this.api.walker_states.status != 1) {
                //console.log("arreter marche******");
                this.api.walkercmdstartdetect();
            }
        }
        else {
            if (this.api.walker_states.person["X"] > this.api.selectWalkZone) {
                this.api.position_state = 1;
                if (this.firstcount) {
                    clearInterval(this.intervalcountdown);
                    this.firstcount = false;
                }
            }
            else {
                this.api.position_state = 2;
                if (!this.firstcount) {
                    this.timecountdown = 3;
                    this.firstcount = true;
                    this.intervalcountdown = setInterval(function () { return _this.updateCountdown(); }, 1300);
                }
            }
        }
    };
    StartAssistancePage.prototype.updateCountdown = function () {
        if (this.api.position_state === 2) {
            if (this.timecountdown > 0) {
                this.speech.speak(this.timecountdown.toString());
                this.api.okToast(this.timecountdown.toString(), 1300);
            }
            else if (this.timecountdown == 0) {
                this.api.okToast(this.param.datatext.btn_walk, 2000);
                this.speech.speak(this.param.datatext.walk);
            }
            else {
                clearInterval(this.intervalcountdown);
                if (this.api.selectMode === "destination") {
                    this.api.reachHttp(this.api.selectDestination);
                    this.api.walkercmdstart(this.api.selectSpeed);
                }
                else {
                    //console.log("moderonde");
                    var x2 = this.api.round_selected.Locations[this.api.cptpoi].Location.Pose.X;
                    var y2 = this.api.round_selected.Locations[this.api.cptpoi].Location.Pose.Y;
                    var x1 = this.api.localization_status.positionx;
                    var y1 = this.api.localization_status.positiony;
                    this.api.puttrajectory(x1, y1, x2, y2);
                    this.api.walkercmdstart(this.api.selectSpeed);
                }
                if (!this.api.resume) {
                    this.Walk_page();
                    this.param.tablewalklogs.walk_time = 0;
                    this.param.tablewalklogs.walk_timeofpauses = 0;
                    if (this.api.selectMode === "round") {
                        console.log("addwalkdata");
                        this.param.addwalklog(this.api.name_current_map, this.api.selectSpeed);
                    }
                }
                else {
                    this.navCtrl.pop();
                }
            }
        } //badpose
        if (this.timecountdown < -1) {
            clearInterval(this.intervalcountdown);
        }
        this.timecountdown--;
    };
    StartAssistancePage.prototype.onclickstop = function (ev) {
        ev.preventDefault();
        this.api.inUse = false;
        clearInterval(this.intervalcountdown);
        this.api.walkercmdstartdetect();
        this.navCtrl.pop();
    };
    StartAssistancePage.prototype.Walk_page = function () {
        this.navCtrl.push(this.walkPage);
    };
    StartAssistancePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: "page-startassistance",template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\guidedwalk\src\pages\start_assistance\start_assistance.html"*/'<!-- mobility support html template -->\n\n<ion-header no-border>\n\n  <headpage pageName="start_assistance"></headpage>\n\n</ion-header>\n\n\n\n<ion-content padding id="body">\n\n  <ion-card text-center class="big_card">\n\n    <ion-grid no-padding class="heightstyle">\n\n      <ion-row class="heightstyle" id="firstrow">\n\n\n\n        <ion-col col-3>  \n\n          <button (mouseup)="onclickstop($event)" class="btnstop">\n\n            <ion-icon color="light" name="hand" class="icon_style">\n\n              <p class="legende">{{this.param.datatext.btn_stop}}</p>\n\n            </ion-icon>\n\n          </button> \n\n\n\n        </ion-col>\n\n\n\n        <ion-col *ngIf="api.position_state===2" no-padding class="goodpose" >\n\n       \n\n        </ion-col>\n\n        <ion-col *ngIf="api.position_state!==2" no-padding class="badpose" >\n\n        \n\n        </ion-col>\n\n\n\n      </ion-row>\n\n\n\n    </ion-grid>\n\n  </ion-card>\n\n</ion-content>'/*ion-inline-end:"C:\Users\laele\Documents\KomApp\guidedwalk\src\pages\start_assistance\start_assistance.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_6__services_speech_service__["a" /* SpeechService */],
            __WEBPACK_IMPORTED_MODULE_4__services_popup_service__["a" /* PopupService */],
            __WEBPACK_IMPORTED_MODULE_3__services_alert_service__["a" /* AlertService */],
            __WEBPACK_IMPORTED_MODULE_5__services_param_service__["a" /* ParamService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__services_api_service__["a" /* ApiService */]])
    ], StartAssistancePage);
    return StartAssistancePage;
}());

//# sourceMappingURL=start_assistance.js.map

/***/ }),

/***/ 132:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 132;

/***/ }),

/***/ 174:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 174;

/***/ }),

/***/ 18:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export Walkassit */
/* unused harmony export Emotion */
/* unused harmony export Person */
/* unused harmony export Origin */
/* unused harmony export Docking */
/* unused harmony export Battery */
/* unused harmony export Anticollision */
/* unused harmony export Statistics */
/* unused harmony export Iostate */
/* unused harmony export Navigation */
/* unused harmony export Exercise */
/* unused harmony export Localization */
/* unused harmony export Differential */
/* unused harmony export Round */
/* unused harmony export Trajectory */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApiService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__alert_service__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__param_service__ = __webpack_require__(10);
// Allows Http communication with komnav
// cf Marc komnav_http_manual for more information
// https://drive.google.com/drive/u/3/folders/1-18kyKv6Ep-fzPU1F-dp0tQ1r8usyuZ7
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var Walkassit = /** @class */ (function () {
    function Walkassit() {
    }
    return Walkassit;
}());

var Emotion = /** @class */ (function () {
    function Emotion() {
    }
    return Emotion;
}());

var Person = /** @class */ (function () {
    function Person() {
    }
    return Person;
}());

var Origin = /** @class */ (function () {
    function Origin() {
    }
    return Origin;
}());

var Docking = /** @class */ (function () {
    function Docking() {
    }
    return Docking;
}());

/*
{ Docking status
  "Unknown    ",
  "Undocked   ",
  "Docking    ",
  "Undocking  ",
  "Error      "
};*/
var Battery = /** @class */ (function () {
    function Battery() {
    }
    return Battery;
}());

var Anticollision = /** @class */ (function () {
    function Anticollision() {
    }
    return Anticollision;
}());

var Statistics = /** @class */ (function () {
    function Statistics() {
    }
    return Statistics;
}());

var Iostate = /** @class */ (function () {
    function Iostate() {
    }
    return Iostate;
}());

var Navigation = /** @class */ (function () {
    function Navigation() {
    }
    return Navigation;
}());

// The status can have the following values:
// • 0 - Waiting: the robot is ready for a new destination speciﬁed with a PUT /navigation/destination request.
// • 1 - Following: the robot is following the trajectory computed to join the destination.
// • 2 - Aiming: the robot has completed the trajectory and is now rotating to aim the destination accurately
// • 3 - Translating: the robot has ﬁnished aiming and is now translating to reach the destination
// • 4 - Rotating: this is the last rotation, executed only if the destination was speciﬁed with “Rotate” : true
// • 5 - Error: indicates that the robot has encountered an error that prevent edit to join the required destination.
// This can be cleared by sending a new destination.
var Exercise = /** @class */ (function () {
    function Exercise() {
    }
    return Exercise;
}());

var Localization = /** @class */ (function () {
    function Localization() {
    }
    return Localization;
}());

var Differential = /** @class */ (function () {
    function Differential() {
    }
    return Differential;
}());

var Round = /** @class */ (function () {
    function Round() {
    }
    return Round;
}());

var Trajectory = /** @class */ (function () {
    function Trajectory() {
    }
    return Trajectory;
}());

var ApiService = /** @class */ (function () {
    function ApiService(toastCtrl, app, httpClient, alert, param) {
        this.toastCtrl = toastCtrl;
        this.app = app;
        this.httpClient = httpClient;
        this.alert = alert;
        this.param = param;
        this.selectSpeed = 0.5;
        this.selectMode = "round";
        this.selectUser = "anonymous";
        this.selectWalkZone = 0.5;
        this.position_state = 0; //0 ?  1 bad 2 good
        this.resume = false;
        this.cptpoi = 0;
        this.socketok = false;
        this.recoface = false;
        this.facedetected = false;
        this.useradded = false;
        this.is_connected = false; // to know if we are connected to the robot
        this.towardDocking = false; //to know if the robot is moving toward the docking
        this.traveldebut = 0;
        this.statusRobot = 10;
        this.metercovered = 0;
        this.cpt_jetsonOK = 0;
        this.appStart = false;
        this.close_app = false;
        this.appOpened = false;
        this.inUse = false; // to know if the enslavement of the legs is active or not
        this.robotok = false;
    }
    ApiService.prototype.instanciate = function () {
        this.socketok = true;
        var apiserv = this;
        ////////////////////// localization socket
        this.selectUser = this.param.datatext.anonymous;
        this.localization_status = new Localization();
        this.localization_socket = new WebSocket(this.wsskomnav + this.param.localhost + "/api/localization/socket", "json");
        this.localization_socket.onerror = function (event) {
            console.log("error localization socket");
        };
        this.localization_socket.onopen = function (event) {
            console.log("localization socket opened");
            this.onmessage = function (event) {
                var test = JSON.parse(event.data);
                apiserv.localization_status.positionx = test["Pose"]["X"];
                apiserv.localization_status.positiony = test["Pose"]["Y"];
                apiserv.localization_status.positiont = test["Pose"]["T"];
            };
            this.onclose = function () {
                console.log("localization socket closed");
            };
        };
        ////////////////////// differential socket
        this.differential_status = new Differential();
        this.differential_socket = new WebSocket(this.wsskomnav + this.param.localhost + "/api/differential/socket", "json");
        this.differential_socket.onerror = function (event) {
            console.log("error differential socket");
        };
        this.differential_socket.onopen = function (event) {
            console.log("differential socket opened");
            this.onmessage = function (event) {
                var test = JSON.parse(event.data);
                apiserv.differential_status.status = test["Status"];
                apiserv.differential_status.TargetAngularSpeed = test["TargetAngularSpeed"];
                apiserv.differential_status.TargetLinearSpeed = test["TargetLinearSpeed"];
                apiserv.differential_status.CurrentAngularSpeed = test["CurrentAngularSpeed"];
                apiserv.differential_status.CurrentLinearSpeed = test["CurrentLinearSpeed"];
            };
            this.onclose = function () {
                console.log("differential socket closed");
            };
        };
        ////////////////////// navigation socket
        this.navigation_status = new Navigation();
        this.navigation_socket = new WebSocket(this.wsskomnav + this.param.localhost + "/api/navigation/socket", "json");
        this.navigation_socket.onerror = function (event) {
            console.log("error navigation socket");
        };
        this.navigation_socket.onopen = function (event) {
            console.log("navigation socket opened");
            this.onmessage = function (event) {
                var test = JSON.parse(event.data);
                apiserv.navigation_status.status = test["Status"];
                apiserv.navigation_status.avoided = test["Avoided"];
                apiserv.navigation_status.trajectory = test["Trajectory"];
            };
            this.onclose = function () {
                console.log("navigation socket closed");
            };
        };
        ////////////////////// exercise socket
        this.exercise_status = new Exercise();
        this.exercise_socket = new WebSocket(this.wsskomnav + this.param.localhost + "/api/exercise/socket", "json");
        this.exercise_socket.onerror = function (event) {
            console.log("error exercise socket");
        };
        this.exercise_socket.onopen = function (event) {
            console.log("exercise socket opened");
            this.onmessage = function (event) {
                var test = JSON.parse(event.data);
                apiserv.exercise_status.status = test["Status"];
                apiserv.exercise_status.maxSpeed = test["MaxSpeed"];
                apiserv.exercise_status.trajectory = test["Trajectory"];
            };
            this.onclose = function () {
                console.log("exercise socket closed");
            };
        };
        ////////////////////// docking socket
        this.docking_status = new Docking();
        this.docking_socket = new WebSocket(this.wsskomnav + this.param.localhost + "/api/docking/socket", "json");
        this.docking_socket.onerror = function (event) {
            console.log("error docking socket");
        };
        this.docking_socket.onopen = function (event) {
            console.log("docking socket opened");
            this.onmessage = function (event) {
                var test = JSON.parse(event.data);
                apiserv.docking_status.status = test["Status"];
                apiserv.docking_status.detected = test["Detected"];
            };
            this.onclose = function () {
                console.log("docking socket closed");
            };
        };
        ////////////////////// anticollision socket
        this.anticollision_status = new Anticollision();
        this.anticollision_socket = new WebSocket(this.wsskomnav + this.param.localhost + "/api/anticollision/socket", "json");
        this.anticollision_socket.onerror = function (event) {
            console.log("error anticollision socket");
        };
        this.anticollision_socket.onopen = function (event) {
            console.log("anticollision socket opened");
            this.onmessage = function (event) {
                var test = JSON.parse(event.data);
                apiserv.anticollision_status.timestamp = test["Timestamp"];
                apiserv.anticollision_status.enabled = test["Enabled"];
                apiserv.anticollision_status.locked = test["Locked"];
                apiserv.anticollision_status.forward = test["Forward"];
                apiserv.anticollision_status.right = test["Right"];
                apiserv.anticollision_status.left = test["Left"];
            };
            this.onclose = function () {
                console.log("anticollision socket closed");
            };
        };
        ////////////////////// statistics socket
        this.statistics_status = new Statistics();
        this.statistics_socket = new WebSocket(this.wsskomnav + this.param.localhost + "/api/statistics/socket", "json");
        this.statistics_socket.onerror = function (event) {
            console.log("error statistic socket");
        };
        this.statistics_socket.onopen = function (event) {
            console.log("statistic socket opened");
            this.onmessage = function (event) {
                var test = JSON.parse(event.data);
                apiserv.statistics_status.totalTime = test["TotalTime"];
                apiserv.statistics_status.totalDistance = test["TotalDistance"];
                apiserv.statistics_status.timestamp = test["Timestamp"];
            };
            this.onclose = function () {
                console.log("statistic socket closed");
            };
        };
        ////////////////////walker socket
        this.walker_states = new Walkassit();
        this.walker_socket = new WebSocket(this.wsskomnav + this.param.localhost + "/api/walker/socket", "json");
        this.walker_socket.onerror = function (event) {
            console.log("error walker socket");
        };
        this.walker_socket.onopen = function (event) {
            console.log("walker socket opened");
            this.onmessage = function (event) {
                var test = JSON.parse(event.data);
                apiserv.walker_states.detected = test["Detected"];
                apiserv.walker_states.origin = test["Origin"];
                apiserv.walker_states.timestamp = test["Timestamp"];
                apiserv.walker_states.person = test["Person"];
                apiserv.walker_states.status = test["Status"];
                apiserv.walker_states.barsAngle = test["BarsAngle"];
                apiserv.walker_states.targetAngularSpeed = test["TargetAngularSpeed"];
                apiserv.walker_states.activationDistance = test["ActivationDistance"];
            };
            this.onclose = function () {
                console.log("walker socket closed");
            };
        };
        ///////////////////////// battery socket
        this.battery_status = new Battery();
        this.battery_socket = new WebSocket(this.wsskomnav + this.param.localhost + "/api/battery/socket", "json");
        this.battery_socket.onerror = function (event) {
            console.log("error battery socket");
        };
        this.battery_socket.onopen = function (event) {
            console.log("battery socket opened");
            this.onmessage = function (event) {
                var test = JSON.parse(event.data);
                apiserv.battery_status.autonomy = test["Autonomy"];
                apiserv.battery_status.current = test["Current"];
                apiserv.battery_status.remaining = test["Remaining"];
                apiserv.battery_status.status = test["Status"];
                apiserv.battery_status.timestamp = test["Timestamp"];
                apiserv.battery_status.voltage = test["Voltage"];
            };
            this.onclose = function () {
                console.log("battery socket closed");
            };
        };
        //////////////////////////////// io socket
        this.b_pressed = false;
        this.btnPush = false;
        this.io_status = new Iostate();
        this.io_socket = new WebSocket(this.wsskomnav + this.param.localhost + "/api/io/socket", "json");
        this.io_socket.onerror = function (event) {
            console.log("error iosocket");
        };
        this.io_socket.onopen = function (event) {
            console.log("io socket opened");
            this.onclose = function () {
                console.log("io socket closed");
            };
        };
        this.io_socket.onmessage = function (event) {
            var test = JSON.parse(event.data);
            apiserv.io_status.timestamp = test["Timestamp"];
            apiserv.io_status.dIn = test["DIn"];
            apiserv.io_status.aIn = test["AIn"];
            if (apiserv.io_status.dIn[2] || apiserv.io_status.dIn[8]) {
                //smart button and button A of the game pad
                if (!apiserv.b_pressed) {
                    apiserv.b_pressed = true;
                    apiserv.btnPush = true;
                }
            }
            else {
                if (apiserv.b_pressed) {
                    apiserv.b_pressed = false;
                }
            }
        };
        ///////////////////////// emotions socket
        this.emotion_status = new Emotion();
        this.emotion_socket = new WebSocket("ws://localhost:7007/", "json");
        this.emotion_socket.onerror = function (event) {
            console.log("error emotion socket");
        };
        this.emotion_socket.onopen = function (event) {
            console.log("emotion socket opened");
            this.onmessage = function (event) {
                var test = JSON.parse(event.data);
                apiserv.emotion_status.id = test["id"];
                apiserv.emotion_status.neutral = Math.round(test["neutral"] * 100);
                apiserv.emotion_status.sad = Math.round(test["sad"] * 100);
                apiserv.emotion_status.happy = Math.round(test["happy"] * 100);
                apiserv.emotion_status.anger = Math.round(test["anger"] * 100);
                apiserv.emotion_status.surprise = Math.round(test["surprise"] * 100);
            };
            this.onclose = function () {
                console.log("emotion socket closed");
            };
        };
        ///////////////////////// trajectory socket
        this.trajectory_status = new Trajectory();
        this.trajectory_socket = new WebSocket(this.wsskomnav + this.param.localhost + "/api/trajectory/socket", "json");
        this.trajectory_socket.onerror = function (event) {
            console.log("error trajectory socket");
        };
        this.trajectory_socket.onopen = function (event) {
            console.log("trajectory socket opened");
            this.onmessage = function (event) {
                var test = JSON.parse(event.data);
                //console.log(test);
                apiserv.trajectory_status.Control = test["Control"];
                apiserv.trajectory_status.Status = test["Status"];
                apiserv.trajectory_status.DistanceCovered = test["DistanceCovered"];
            };
            this.onclose = function () {
                console.log("trajectory socket closed");
            };
        };
        //////////////////////////////// ROS TOPIC connection
        this.ros = new ROSLIB.Ros({
            url: this.wssros + this.param.robot.rosip
        });
        this.ros.on('connection', function () {
            console.log('Connected to ros websocket server.');
        });
        this.ros.on('error', function (error) {
            console.log('Error connecting to ros websocket server: ', error);
        });
        this.ros.on('close', function () {
            console.log('Connection to ros websocket server closed.');
        });
        //////////////////////////////// ROS TOPIC cam
        this.listener_camera = new ROSLIB.Topic({
            ros: this.ros,
            name: 'facecog/face_detection_image/compressed',
            //name : '/top_webcam/image_raw/compressed',
            //name : '/fisheye_cam/image_raw',
            //name : '/D435_camera_FWD/color/image_raw',
            //name : '/fisheye_cam/compressed',
            messageType: 'sensor_msgs/CompressedImage'
        });
        //////////////////////////////// ROS TOPIC recoface
        this.listener_recoface = new ROSLIB.Topic({
            ros: this.ros,
            name: '/facecog/user_id',
            messageType: 'std_msgs/String'
        });
    };
    ApiService.prototype.mailAddInformationWalk = function () {
        var now = new Date().toLocaleString("en-GB", {
            day: "numeric",
            month: "numeric",
            year: "numeric",
            hour: "numeric",
            minute: "numeric",
            second: "numeric",
        });
        return ("<br> Time : " +
            now +
            "<br> Serial number : " +
            this.param.serialnumber +
            "<br> Battery remaining: " +
            this.battery_status.remaining +
            "%" +
            "<br> Battery state : " +
            this.battery_status.status +
            "<br> Battery voltage : " +
            this.battery_status.voltage +
            "<br> Battery current : " +
            this.battery_status.current +
            "<br> Battery autonomy : " +
            this.battery_status.autonomy +
            "<br> Docking state : " +
            this.docking_status.status +
            "<br> Status Forward : " +
            this.anticollision_status.forward +
            "<br> Status Right : " +
            this.anticollision_status.right +
            "<br> Status Left : " +
            this.anticollision_status.left +
            "<br> Status Reverse : " +
            this.anticollision_status.reverse +
            "<br> Navigation state : " +
            this.navigation_status.status +
            "<br> Differential state : " +
            this.differential_status.status +
            "<br> Odometer : " +
            this.statistics_status.totalDistance +
            "<br> Position X : " +
            this.localization_status.positionx +
            "<br> Position Y : " +
            this.localization_status.positiony +
            "<br> Position T : " +
            this.localization_status.positiont +
            "<br> Application : 1" +
            "<br> Walker Status : " +
            this.walker_states.status +
            "<br> Walker Detect : " +
            this.walker_states.detected +
            "<br> Meter covered : " +
            this.metercovered +
            "<br>");
    };
    ApiService.prototype.mailAddInformationBasic = function () {
        var now = new Date().toLocaleString("en-GB", {
            day: "numeric",
            month: "numeric",
            year: "numeric",
            hour: "numeric",
            minute: "numeric",
            second: "numeric",
        });
        return ("<br> Time : " +
            now +
            "<br> Serial number : " +
            this.param.serialnumber +
            "<br> Battery remaining: " +
            this.battery_status.remaining +
            "%" +
            "<br> Battery state : " +
            this.battery_status.status +
            "<br> Battery voltage : " +
            this.battery_status.voltage +
            "<br> Battery current : " +
            this.battery_status.current +
            "<br> Battery autonomy : " +
            this.battery_status.autonomy +
            "<br> Docking state : " +
            this.docking_status.status +
            "<br> Status Forward : " +
            this.anticollision_status.forward +
            "<br> Status Right : " +
            this.anticollision_status.right +
            "<br> Status Left : " +
            this.anticollision_status.left +
            "<br> Status Reverse : " +
            this.anticollision_status.reverse +
            "<br> Navigation state : " +
            this.navigation_status.status +
            "<br> Differential state : " +
            this.differential_status.status +
            "<br> Odometer : " +
            this.statistics_status.totalDistance +
            "<br> Position X : " +
            this.localization_status.positionx +
            "<br> Position Y : " +
            this.localization_status.positiony +
            "<br> Position T : " +
            this.localization_status.positiont +
            "<br> Application : 1" +
            "<br>");
    };
    ApiService.prototype.checkrobot = function () {
        var _this = this;
        this.httpClient
            .get(this.httpskomnav + this.param.localhost + "/api/battery/state")
            .subscribe(function (data) {
            _this.robotok = true;
        }, function (err) {
            console.log(err);
            _this.robotok = false;
        });
    };
    ApiService.prototype.jetsonOK = function () {
        var _this = this;
        this.checkInternet();
        this.alert.checkwifi(this.wifiok);
        this.httpClient
            .get(this.httpskomnav + this.param.localhost + "/api/battery/state", {
            observe: "response",
        })
            .subscribe(function (resp) {
            if (_this.statusRobot === 2) {
                _this.alert.appError(_this.mailAddInformationBasic());
                document.location.reload(); //if error then refresh the page and relaunch websocket
            }
            if (resp.status === 200) {
                _this.cpt_jetsonOK += 1;
                if (_this.walker_states.status === 2) {
                    // knee laser in error
                    _this.statusRobot = 1;
                    _this.connectionLost();
                    _this.walkercmdstopdetect(); //if we don't do that the knee laser will go wrong
                }
                else {
                    _this.statusRobot = 0; // everything is ok
                    _this.connect();
                }
            }
            else {
                _this.cpt_jetsonOK += 1;
                if (_this.cpt_jetsonOK > 5) {
                    _this.statusRobot = 2; //no connection
                    _this.connectionLost();
                }
            }
        }, function (err) {
            console.log(err); //no connection
            _this.cpt_jetsonOK += 1;
            if (_this.cpt_jetsonOK > 5) {
                _this.statusRobot = 2; //no connection
                _this.connectionLost();
            }
        });
    };
    ApiService.prototype.isConnectedInternet = function () {
        return __awaiter(this, void 0, void 0, function () {
            var response, text, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        return [4 /*yield*/, fetch('http://localhost/ionicDB/internetping.php')];
                    case 1:
                        response = _a.sent();
                        return [4 /*yield*/, response.text()];
                    case 2:
                        text = _a.sent();
                        //console.log(text);
                        // Analyse du texte pour déterminer la connectivité Internet
                        return [2 /*return*/, text.trim() === 'true']; // Renvoie true si le texte est 'true', sinon false
                    case 3:
                        error_1 = _a.sent();
                        console.error('Error checking internet connectivity:', error_1);
                        return [2 /*return*/, false];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    ApiService.prototype.checkInternet = function () {
        var _this = this;
        this.isConnectedInternet().then(function (connecte) {
            if (connecte) {
                //console.log("L'ordinateur est connecté à Internet");
                _this.wifiok = true;
            }
            else {
                //console.log("L'ordinateur n'est pas connecté à Internet");
                _this.wifiok = false;
            }
        });
    };
    ApiService.prototype.walkercmdstart = function (speed) {
        // start legs enslavement
        console.log("walk assist start");
        var test = {
            Guided: true,
            Assist: true,
            Enable: true,
            MaxSpeed: speed,
            OriginX: this.selectWalkZone
        };
        var jsonstring = JSON.stringify(test);
        this.walker_socket.send(jsonstring);
    };
    ApiService.prototype.walkercmdstartdetect = function () {
        console.log("walk detect active");
        // start leg detection
        var test = {
            Guided: true,
            Assist: false,
            Enable: true,
        };
        var jsonstring = JSON.stringify(test);
        this.walker_socket.send(jsonstring);
    };
    ApiService.prototype.walkercmdstopdetect = function () {
        console.log("walk total stop");
        // stop leg enslavement + detection
        var test = {
            Guided: false,
            Assist: false,
            Enable: false,
        };
        var jsonstring = JSON.stringify(test);
        this.walker_socket.send(jsonstring);
    };
    ApiService.prototype.connectHttp = function () {
        this.httpClient
            .put(this.httpskomnav + this.param.localhost + "/api/docking/connect", {
            observe: "response",
        })
            .subscribe(function (resp) {
            console.log(resp);
            console.log("connectOK");
        }, function (err) {
            console.log(err);
            console.log("connectPBM");
        });
    };
    ApiService.prototype.disconnectHttp = function () {
        this.httpClient
            .put(this.httpskomnav + this.param.localhost + "/api/docking/disconnect", {
            observe: "response",
        })
            .subscribe(function (resp) {
            console.log(resp);
            console.log("disconnectOK");
        }, function (err) {
            console.log(err);
            console.log("disconnectPBM");
        });
    };
    ApiService.prototype.abortNavHttp = function () {
        // stop the navigation
        this.httpClient
            .put(this.httpskomnav + this.param.localhost + "/api/navigation/abort", {
            observe: "response",
        })
            .subscribe(function (resp) {
            console.log(resp);
            console.log("abortNavOK");
        }, function (err) {
            console.log(err);
            console.log("abortNavPBM");
        });
    };
    ApiService.prototype.abortDockingHttp = function () {
        this.httpClient
            .put(this.httpskomnav + this.param.localhost + "/api/docking/abort", {
            observe: "response",
        })
            .subscribe(function (resp) {
            console.log(resp);
            console.log("abortdockingOK");
        }, function (err) {
            console.log(err);
            console.log("abortdockingPBM");
        });
    };
    ApiService.prototype.reachHttp = function (poiname) {
        this.httpClient
            .put(this.httpskomnav +
            this.param.localhost +
            "/api/navigation/destination/name/" +
            poiname, { observe: "response" })
            .subscribe(function (resp) {
            console.log(resp);
            console.log("reachOK");
        }, function (err) {
            console.log(err);
            console.log("reachPBM");
        });
    };
    ApiService.prototype.getCurrentMap = function () {
        var _this = this;
        this.httpClient
            .get(this.httpskomnav + this.param.localhost + "/api/maps/current/properties")
            .subscribe(function (data) {
            //console.log(data);
            _this.mapdata = data;
            //console.log(this.mapdata.Id);
            if (_this.mapdata) {
                _this.id_current_map = _this.mapdata.Id;
                _this.name_current_map = _this.mapdata.Name;
            }
            console.log("get current map ok");
        }, function (err) {
            console.log(err);
        });
    };
    ApiService.prototype.ngOnInit = function () { };
    ApiService.prototype.connect = function () {
        this.is_connected = true;
    };
    ApiService.prototype.connectionLost = function () {
        this.is_connected = false;
    };
    ApiService.prototype.okToast = function (m, n) {
        var toast = this.toastCtrl.create({
            message: m,
            duration: n,
            position: "middle",
            cssClass: "toastam",
        });
        toast.present();
    };
    ApiService.prototype.getCurrentLocations = function () {
        var _this = this;
        //avoir les poi de la map actuelle
        this.httpClient
            .get(this.httpskomnav + this.param.localhost + "/api/maps/current/locations")
            .subscribe(function (data) {
            console.log("get locations ok");
            _this.all_locations = data;
            //console.log(data);
        }, function (err) {
            console.log(err);
        });
    };
    // pour affichage map
    ApiService.prototype.getImgMapHttp = function () {
        return this.httpClient.get(this.httpskomnav + this.param.localhost + "/api/maps/current/image", { responseType: "blob" });
    };
    ApiService.prototype.getRoundList = function () {
        var _this = this;
        this.httpClient
            .get(this.httpskomnav + this.param.localhost + "/api/rounds/list")
            .subscribe(function (data) {
            _this.rounddata = data;
            if (_this.id_current_map && _this.rounddata) {
                _this.rounds_current_map = _this.rounddata.filter(function (round) { return round.Map === _this.id_current_map; });
            }
        }, function (err) {
            console.log(err);
        });
    };
    ApiService.prototype.launchRound = function () {
        this.httpClient
            .put(this.httpskomnav +
            this.param.localhost +
            "/api/exercise/start/" +
            this.selectRound, { observe: "response" })
            .subscribe(function (resp) {
            console.log(resp);
            console.log("launchROK");
        }, function (err) {
            console.log(err);
            console.log("launchRPBM");
        });
    };
    ApiService.prototype.pauseRound = function () {
        this.httpClient
            .put(this.httpskomnav +
            this.param.localhost +
            "/api/exercise/pause", { observe: "response" })
            .subscribe(function (resp) {
            console.log(resp);
            console.log("pauseROK");
        }, function (err) {
            console.log(err);
            console.log("pauseRPBM");
        });
    };
    ApiService.prototype.resumeRound = function () {
        this.httpClient
            .put(this.httpskomnav +
            this.param.localhost +
            "/api/exercise/resume", { observe: "response" })
            .subscribe(function (resp) {
            console.log(resp);
            console.log("resumeROK");
        }, function (err) {
            console.log(err);
            console.log("resumeRPBM");
        });
    };
    ApiService.prototype.puttrajectory = function (x1, y1, x2, y2) {
        var body = JSON.stringify([{
                Start: {
                    X: x1,
                    Y: y1
                },
                End: {
                    X: x2,
                    Y: y2
                },
                MaxSpeed: 1
            }]);
        console.log(body);
        this.httpClient
            .put(this.httpskomnav + this.param.localhost + "/api/trajectory/follow", body)
            .subscribe(function (response) {
            console.log(response);
            console.log("trajectory ok");
        }, function (err) {
            console.log(err);
        });
    };
    ApiService.prototype.putStopDetecthttp = function () {
        var body = JSON.stringify({
            Guided: false,
            Assist: false,
            Enable: false,
        });
        console.log(body);
        this.httpClient
            .put(this.httpskomnav + this.param.localhost + "/api/walker/command", body)
            .subscribe(function (response) {
            console.log(response);
            console.log("stop detect ok");
        }, function (err) {
            console.log(err);
        });
    };
    ApiService.prototype.launchreco = function (start) {
        var _this = this;
        console.log("reco launch");
        var serviceRecoface = new ROSLIB.Service({
            ros: this.ros,
            name: '/toggle_face_recognition_node',
            serviceType: 'std_srvs/SetBool'
        });
        var request = new ROSLIB.ServiceRequest({
            data: start,
        });
        serviceRecoface.callService(request, function (result) {
            console.log("*********service recoface****************");
            console.log(result);
        });
        if (start) {
            console.log("reco true");
            this.listener_recoface.subscribe(function (message) {
                //console.log('Received message on ' + this.listener_recoface.name + ': ' + message.data);
                _this.preco = message.data;
            });
        }
        else {
            console.log("reco false");
            this.listener_recoface.unsubscribe();
            this.listener_camera.unsubscribe();
        }
    };
    ApiService.prototype.addUserJetson = function (name) {
        console.log("adduserjet");
        var serviceAddUser = new ROSLIB.Service({
            ros: this.ros,
            name: '/add_user_data',
            serviceType: 'kompai_ros/add_user_data_server'
        });
        var request = new ROSLIB.ServiceRequest({
            user_id_add: name,
        });
        var apiserv = this;
        serviceAddUser.callService(request, function (result) {
            console.log("*********service add user****************");
            console.log(result.result);
            if (result.result) {
                apiserv.useradded = true;
            }
        });
    };
    ApiService.prototype.deleteUserJetson = function (name) {
        var serviceDeleteUser = new ROSLIB.Service({
            ros: this.ros,
            name: '/remove_user',
            serviceType: 'kompai_ros/add_user_data_server'
        });
        var request = new ROSLIB.ServiceRequest({
            user_id_add: name,
        });
        serviceDeleteUser.callService(request, function (result) {
            console.log("*********service delete user****************");
            console.log(result);
        });
    };
    ApiService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */],
            __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_3__alert_service__["a" /* AlertService */],
            __WEBPACK_IMPORTED_MODULE_4__param_service__["a" /* ParamService */]])
    ], ApiService);
    return ApiService;
}());

//# sourceMappingURL=api.service.js.map

/***/ }),

/***/ 218:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(217);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(214);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_first_first__ = __webpack_require__(219);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_api_service__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_alert_service__ = __webpack_require__(23);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen, api, alert) {
        var _this = this;
        this.api = api;
        this.alert = alert;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_first_first__["a" /* FirstPage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
            platform.pause.subscribe(function () {
                // stop the enslavement of the legs if we do something else on the robot tablet
                //this.api.walkercmdstopdetect();
                _this.api.abortNavHttp();
                window.close();
            });
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\guidedwalk\src\app\app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n\n'/*ion-inline-end:"C:\Users\laele\Documents\KomApp\guidedwalk\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */], __WEBPACK_IMPORTED_MODULE_5__services_api_service__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_6__services_alert_service__["a" /* AlertService */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 219:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FirstPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_param_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_api_service__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_alert_service__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__choose_assistance_choose_assistance__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__recoface_recoface__ = __webpack_require__(221);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_popup_service__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__services_speech_service__ = __webpack_require__(36);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var FirstPage = /** @class */ (function () {
    function FirstPage(loadingCtrl, speech, popup, param, alert, api, navCtrl) {
        this.loadingCtrl = loadingCtrl;
        this.speech = speech;
        this.popup = popup;
        this.param = param;
        this.alert = alert;
        this.api = api;
        this.navCtrl = navCtrl;
        this.choosePage = __WEBPACK_IMPORTED_MODULE_5__choose_assistance_choose_assistance__["a" /* ChooseAssistancePage */];
        this.recoPage = __WEBPACK_IMPORTED_MODULE_6__recoface_recoface__["a" /* RecofacePage */];
        this.cpt_open = 0;
        this.loading = this.loadingCtrl.create({});
        this.loading.present(); //loading animation display
    }
    FirstPage.prototype.ngOnInit = function () {
        var _this = this;
        this.appopen = setInterval(function () { return _this.appOpen(); }, 900);
        this.speech.getVoice();
    };
    FirstPage.prototype.appOpen = function () {
        if (this.param.localhost != undefined) {
            this.api.checkrobot();
        }
        this.api.checkInternet();
        this.alert.checkwifi(this.api.wifiok);
        this.cpt_open += 1;
        if (this.cpt_open === 15 && !this.api.robotok) {
            this.popup.startFailedAlert();
            this.speech.speak(this.param.datatext.cantstart);
        }
        else if (this.cpt_open === 15 && this.api.robotok) {
            this.popup.relocAlert();
            this.speech.speak(this.param.datatext.relocAlert_title + ". " + this.param.datatext.relocAlert_message);
        }
        if (!this.api.appOpened) {
            this.param.getDataRobot();
            this.param.getWalkUser();
            this.param.getMail();
            this.param.getPhone();
            this.param.getDurationNS();
            this.param.getDuration();
            this.param.getBattery();
            if (this.param.robot &&
                this.param.duration &&
                this.param.maillist &&
                this.param.phonenumberlist) {
                this.param.fillData();
                if (this.param.robot.httpskomnav == 0) {
                    this.api.httpskomnav = "http://";
                    this.api.wsskomnav = "ws://";
                }
                else {
                    this.api.httpskomnav = "https://";
                    this.api.wsskomnav = "wss://";
                }
                if (this.param.robot.httpsros == 0) {
                    this.api.wssros = "ws://";
                }
                else {
                    this.api.wssros = "wss://";
                }
            }
            if (this.api.robotok && this.param.langage && this.param.maillist) {
                this.api.getCurrentMap();
                if (!this.api.socketok) {
                    this.api.instanciate();
                }
                if (this.api.mapdata) {
                    this.api.appOpened = true;
                    this.api.getRoundList();
                }
            }
        }
        else if (this.api.appOpened && this.cpt_open > 6) {
            this.api.abortNavHttp();
            this.alert.appOpen(this.api.mailAddInformationBasic());
            if (this.api.recoface) {
                this.api.launchreco(true);
                this.navCtrl.setRoot(this.recoPage);
            }
            else {
                this.navCtrl.setRoot(this.choosePage);
            }
            this.loading.dismiss(); // destroy loading animation
            clearInterval(this.appopen);
            if (this.popup.alert_blocked) {
                this.popup.alert_blocked.dismiss();
            }
            console.log(this.cpt_open);
        }
    };
    FirstPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: "page-first",template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\guidedwalk\src\pages\first\first.html"*/'<ion-header no-border>\n\n \n\n</ion-header>\n\n<ion-content padding >\n\n  <ion-card text-center class="big_card" >\n\n      <ion-grid class="heightstyle">\n\n       \n\n      </ion-grid> \n\n\n\n  </ion-card>\n\n \n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\laele\Documents\KomApp\guidedwalk\src\pages\first\first.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_8__services_speech_service__["a" /* SpeechService */],
            __WEBPACK_IMPORTED_MODULE_7__services_popup_service__["a" /* PopupService */],
            __WEBPACK_IMPORTED_MODULE_2__services_param_service__["a" /* ParamService */],
            __WEBPACK_IMPORTED_MODULE_4__services_alert_service__["a" /* AlertService */],
            __WEBPACK_IMPORTED_MODULE_3__services_api_service__["a" /* ApiService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */]])
    ], FirstPage);
    return FirstPage;
}());

//# sourceMappingURL=first.js.map

/***/ }),

/***/ 220:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WalkPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_api_service__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_alert_service__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_popup_service__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_param_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_speech_service__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__start_assistance_start_assistance__ = __webpack_require__(115);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var WalkPage = /** @class */ (function () {
    function WalkPage(speech, popup, alert, param, navCtrl, api) {
        var _this = this;
        this.speech = speech;
        this.popup = popup;
        this.alert = alert;
        this.param = param;
        this.navCtrl = navCtrl;
        this.api = api;
        this.arrowinfo = "f";
        this.direction = "f";
        this.speechcpt = 0;
        this.detectobstacle = false;
        this.walkPage = WalkPage_1;
        this.startPage = __WEBPACK_IMPORTED_MODULE_7__start_assistance_start_assistance__["a" /* StartAssistancePage */];
        this.cptdetected = 0;
        this.acknowledge = false;
        this.donttalk = false;
        this.api.metercovered = 0;
        this.updateinterv = setInterval(function () { return _this.getUpdate(); }, 500);
        this.updatetimes = setInterval(function () { return _this.getUpdatetimes(); }, 1000);
        this.speechinterv = setInterval(function () { return _this.sayorder(); }, 500);
        this.api.inUse = true;
        this.speech.getVoice();
        // declecnche la popup de permission camera si besoin
        this.finish = false;
        this.selectOptionspeed = {
            title: this.param.datatext.choose_speed,
        };
        this.selectOptionsWalkZone = {
            title: this.param.datatext.choose_walkzone,
        };
        this.is_sendmail = false;
    }
    WalkPage_1 = WalkPage;
    WalkPage.prototype.ngOnInit = function () {
    };
    WalkPage.prototype.ionViewWillLeave = function () {
    };
    WalkPage.prototype.ionViewDidEnter = function () {
        this.onpause = false;
        this.donttalk = false;
    };
    WalkPage.prototype.onlyClick_Pause = function (ev) {
        // Call preventDefault() to prevent any further handling
        ev.preventDefault();
        this.btnpause();
        this.speech.speak(this.param.datatext.wantstop);
    };
    WalkPage.prototype.onlyClick_Stop = function (ev) {
        // Call preventDefault() to prevent any further handling
        ev.preventDefault();
        this.btnstop();
    };
    WalkPage.prototype.getDistance = function (x1, y1, x2, y2) {
        var y = x2 - x1;
        var x = y2 - y1;
        var res = Math.sqrt(x * x + y * y);
        //console.log(res);
        return res;
    };
    WalkPage.prototype.sayorder = function () {
        if (this.api.walker_states.status === 3) {
            this.speechcpt = this.speechcpt + 1;
            if (this.api.anticollision_status.forward === 0) {
                this.detectobstacle = false;
            }
            if (this.api.selectMode === "round") {
                var x1 = this.api.round_selected.Locations[this.api.cptpoi].Location.Pose.X;
                var y1 = this.api.round_selected.Locations[this.api.cptpoi].Location.Pose.Y;
                var x2 = this.api.localization_status.positionx;
                var y2 = this.api.localization_status.positiony;
                var dist = this.getDistance(x1, y1, x2, y2);
                if (this.api.position_state === 2 && dist < 1.4 && dist > 1.1 && !this.donttalk) {
                    this.speech.speak(this.param.datatext.slowdown);
                    this.donttalk = true;
                }
            }
            if (this.api.anticollision_status.forward === 2 && !this.detectobstacle) {
                this.detectobstacle = true;
                this.speechcpt = 0;
                this.speech.speak(this.param.datatext.obstacle);
                this.api.okToast(this.param.datatext.obstacle, 4000);
            }
            else if (this.api.anticollision_status.forward === 2 && this.speechcpt > 20) {
                this.detectobstacle = false;
            }
            else if (this.arrowinfo === "f" && this.direction !== "f") {
                this.speechcpt = 0;
                this.direction = "f";
                this.speech.speak(this.param.datatext.gof);
                this.donttalk = false;
            }
            else if (this.arrowinfo === "l" && this.direction !== "l") {
                this.speechcpt = 0;
                this.direction = "l";
                this.speech.speak(this.param.datatext.gol);
                this.donttalk = false;
            }
            else if (this.arrowinfo === "r" && this.direction !== "r") {
                this.speechcpt = 0;
                this.direction = "r";
                this.speech.speak(this.param.datatext.gor);
                this.donttalk = false;
            }
        }
    };
    WalkPage.prototype.getUpdatetimes = function () {
        //console.log(this.param.tablewalklogs.walk_time);
        if (!this.finish) {
            this.update_walktimeofpauses();
            this.update_walktime();
        }
    };
    WalkPage.prototype.sendHelp = function (ev) {
        var _this = this;
        ev.preventDefault();
        this.popup.askHelp();
        this.btnpause();
        // change the color and logo of the box
        if (!this.is_sendmail) {
            this.is_sendmail = true;
            // for (let num of this.param.phonenumberlist) {
            //   this.alert.sendsmsperso(this.param.datatext.smsSOS, num);
            // }
            // capture l'image et l'envoie via mail
            this.alert.SOS_c();
            this.alert.mobAssistSOS(this.api.mailAddInformationWalk());
            setTimeout(function () {
                _this.is_sendmail = false;
            }, 15000);
        }
    };
    WalkPage.prototype.getUpdate = function () {
        this.getDetect();
        this.getMeter();
    };
    WalkPage.prototype.btnpause = function () {
        this.onpause = true;
        if (this.api.selectMode === "destination") {
            this.api.walkercmdstartdetect();
            console.log("pause abortnav");
            this.api.abortNavHttp();
        }
        else {
            this.api.walkercmdstartdetect();
            //this.api.abortNavHttp();
            this.param.tablewalklogs.walk_speed = this.api.selectSpeed;
            this.param.tablewalklogs.walk_meters = this.api.metercovered;
            this.param.updatewalklog();
        }
        if (!this.finish) {
            this.param.tablewalklogs.walk_numberofpauses++;
        }
    };
    WalkPage.prototype.btnresume = function (ev) {
        ev.preventDefault();
        this.api.inUse = true;
        this.api.resume = true;
        this.navCtrl.push(this.startPage);
        this.api.walkercmdstartdetect();
    };
    WalkPage.prototype.btnstop = function () {
        this.api.cptpoi = 1;
        this.api.resume = false;
        this.api.inUse = false;
        this.api.walkercmdstartdetect();
        console.log("stop abortnav");
        this.api.abortNavHttp();
        this.navCtrl.popToRoot();
        //this.param.senddatawalking();
        clearInterval(this.updateinterv);
        clearInterval(this.updatetimes);
    };
    WalkPage.prototype.update_walktime = function () {
        if (this.api.walker_states.status === 3) {
            this.param.tablewalklogs.walk_time++;
            if (this.param.tablewalklogs.walk_time >= 15 && this.param.tablewalklogs.walk_time % 15 === 0) {
                console.log("add emotion");
                this.param.addEmotion(this.api.emotion_status.sad, this.api.emotion_status.happy, this.api.emotion_status.anger, this.api.emotion_status.surprise, this.api.emotion_status.neutral);
            }
        }
    };
    WalkPage.prototype.update_walktimeofpauses = function () {
        if (this.api.walker_states.status != 3) {
            this.onpause = true;
            this.param.tablewalklogs.walk_timeofpauses++;
        }
    };
    WalkPage.prototype.getMeter = function () {
        if (this.api.traveldebut > 0) {
            this.api.metercovered =
                this.api.statistics_status.totalDistance - this.api.traveldebut;
        }
        else {
            this.api.traveldebut = this.api.statistics_status.totalDistance;
        }
    };
    WalkPage.prototype.getDetect = function () {
        if (this.api.walker_states.status === 3) {
            if (this.api.walker_states.person["X"] > this.api.walker_states.activationDistance || !this.api.walker_states.detected) {
                this.api.position_state = 1; //bad
            }
            else {
                this.api.position_state = 2; //in walkzone
            }
            if (!this.api.walker_states.detected) {
                this.cptdetected = this.cptdetected + 1;
            }
            else {
                this.cptdetected = 0;
            }
            if (this.api.trajectory_status.Status === 3) {
                this.acknowledge = false;
            }
        }
        if (this.api.walker_states.targetAngularSpeed < 0.28 && this.api.walker_states.targetAngularSpeed > -0.28) {
            this.arrowinfo = "f";
        }
        else if (this.api.walker_states.targetAngularSpeed >= 0.28) {
            this.arrowinfo = "l";
        }
        else {
            this.arrowinfo = "r";
        }
        // if smart bouton is pressed or the bouton A of the gamepad is pressed
        if (this.cptdetected > 6 ||
            this.api.btnPush ||
            !this.api.is_connected) {
            if (this.api.walker_states.status === 3 && this.param.tablewalklogs.walk_time > 2) {
                console.log("pause à cause du bouton malin");
                this.btnpause();
                this.cptdetected = 0;
            }
            this.api.btnPush = false;
        }
        if ((this.api.selectMode === "destination" && this.api.navigation_status.status === 5)) {
            this.btnstop();
            this.popup.errorNavAlert();
            this.alert.naverror(this.api.mailAddInformationBasic());
        }
        else if (this.api.selectMode === "round" && (this.api.round_selected.Locations.length > (this.api.cptpoi + 1)) && !this.onpause && (this.api.trajectory_status.Status < 2) && !this.acknowledge) {
            //go next poi
            console.log("next poi");
            console.log(this.api.cptpoi);
            this.acknowledge = true;
            this.api.cptpoi = this.api.cptpoi + 1;
            var x2 = this.api.round_selected.Locations[this.api.cptpoi].Location.Pose.X;
            var y2 = this.api.round_selected.Locations[this.api.cptpoi].Location.Pose.Y;
            var x1 = this.api.localization_status.positionx;
            var y1 = this.api.localization_status.positiony;
            this.api.puttrajectory(x1, y1, x2, y2);
        }
        else if (this.api.selectMode === "round" && (this.api.trajectory_status.Status < 2) && (this.api.round_selected.Locations.length - 1) === this.api.cptpoi && !this.finish) {
            this.finish = true;
            this.api.okToast(this.param.datatext.bravo, 3000);
            this.speech.speak(this.param.datatext.finishexo);
            this.btnpause();
        }
        else if (this.api.selectMode === "destination" && this.api.navigation_status.status === 2 && this.param.tablewalklogs.walk_time > 3 && !this.finish) {
            this.finish = true;
            this.api.okToast(this.param.datatext.bravo, 3000);
            this.speech.speak(this.param.datatext.atdestination);
            this.btnpause();
        }
    };
    WalkPage.prototype.onSliderRelease = function (ev, id) {
        ev.preventDefault();
        id.open();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("select1"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Select */])
    ], WalkPage.prototype, "select1", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("select2"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Select */])
    ], WalkPage.prototype, "select2", void 0);
    WalkPage = WalkPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: "page-walk",template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\guidedwalk\src\pages\walk\walk.html"*/'<!-- home page html template -->\n\n\n\n<ion-header no-border>\n\n  <headpage pageName="walk"></headpage>\n\n</ion-header>\n\n\n\n<ion-content padding id="body">\n\n  <ion-card text-center class="big_card">\n\n    <ion-grid no-padding class="heightstyle">\n\n      <ion-row class="heightstyle" id="firstrow">\n\n\n\n        <ion-col col-sm-3>\n\n\n\n          <ion-item class="itemselectspeed" *ngIf="api.walker_states.status!==3">\n\n            <ion-label fixed style="color:white; margin-left: 10px;">{{param.datatext.speed}}</ion-label>\n\n            <ion-select #select1 okText={{param.datatext.btn_ok}} cancelText={{param.datatext.btn_cancel}}\n\n              class="myCustomSelectPOI" [selectOptions]="selectOptionspeed" [(ngModel)]="api.selectSpeed" (mouseup)="onSliderRelease($event,select1)">\n\n              <ion-option [value]=0.1>10 cm/s</ion-option>\n\n              <ion-option [value]=0.2>20 cm/s</ion-option>\n\n              <ion-option [value]=0.3>30 cm/s</ion-option>\n\n              <ion-option [value]=0.4>40 cm/s</ion-option>\n\n              <ion-option [value]=0.5>50 cm/s</ion-option>\n\n              <ion-option [value]=0.6>60 cm/s</ion-option>\n\n              <ion-option [value]=0.7>70 cm/s</ion-option>\n\n\n\n            </ion-select>\n\n          </ion-item>\n\n          <ion-item class="itemselectspeed" *ngIf="api.walker_states.status!==3">\n\n            <ion-label fixed style=" color:white; margin-left: 10px; ">{{param.datatext.walkzone}} </ion-label>\n\n            <ion-select #select2 okText={{this.param.datatext.btn_ok}} cancelText={{this.param.datatext.btn_cancel}}\n\n              class="myCustomSelectPOI" [selectOptions]="selectOptionsWalkZone" [(ngModel)]="api.selectWalkZone"\n\n              placeholder={{param.datatext.choose_walkzone}} (mouseup)="onSliderRelease($event,select2)">\n\n              <ion-option [value]=0.4>40 cm</ion-option>\n\n              <ion-option [value]=0.45>45 cm</ion-option>\n\n              <ion-option [value]=0.5>50 cm</ion-option>\n\n              <ion-option [value]=0.55>55 cm</ion-option>\n\n              <ion-option [value]=0.6>60 cm</ion-option>\n\n              <ion-option [value]=0.65>65 cm</ion-option>\n\n              <ion-option [value]=0.70>70 cm</ion-option>\n\n\n\n            </ion-select>\n\n          </ion-item>\n\n          <!-- the Go button is disabled if the robot is not connected or if it detects no one -->\n\n          <button *ngIf="api.walker_states.status!=3" (mouseup)="btnresume($event)"\n\n            [disabled]="api.walker_states.status===2 || !api.is_connected || this.finish" class="btnwalk">\n\n            <ion-icon color="light" name="walk" class="icon_style">\n\n              <p class="legende">\n\n                {{this.param.datatext.btn_walk}}\n\n              </p>\n\n            </ion-icon>\n\n          </button>\n\n          <button *ngIf="api.walker_states.status===3" (mouseup)="onlyClick_Pause($event)" class="btnpause">\n\n            <ion-icon color="light" name="hand" class="icon_style">\n\n              <p class="legende">{{this.param.datatext.btn_pause}}</p>\n\n            </ion-icon>\n\n          </button>\n\n          <button *ngIf="api.walker_states.status!=3" (mouseup)="onlyClick_Stop($event)" class="btnstop">\n\n            <ion-icon color="light" name="hand" class="icon_style">\n\n              <p class="legende">{{this.param.datatext.btn_stop}}</p>\n\n            </ion-icon>\n\n          </button>\n\n\n\n        </ion-col>\n\n\n\n        <ion-col no-padding class="colmeter">\n\n\n\n          <ion-item no-padding no-lines class="item_style">\n\n            <ion-row>\n\n\n\n              <ion-col col-sm-6 no-padding>\n\n                <p class="distance_text">\n\n                  {{this.param.datatext.timewalk}}\n\n                </p>\n\n                <p class="distance">\n\n                  {{this.param.tablewalklogs.walk_time}} s\n\n                </p>\n\n              </ion-col>\n\n              <ion-col col-sm-6 no-padding>\n\n                <p class="distance_text">\n\n                  {{this.param.datatext.distance_covered}}\n\n                </p>\n\n                <p class="distance">\n\n                  {{api.metercovered}} m\n\n                </p>\n\n              </ion-col>\n\n            </ion-row>\n\n\n\n\n\n          </ion-item>\n\n          <ion-item no-padding no-lines class="item_style">\n\n\n\n            <ion-icon *ngIf="api.walker_states.status===3 && this.arrowinfo===\'l\'" class="arrow_style"\n\n              name="arrow-round-back"></ion-icon>\n\n            <ion-icon *ngIf="api.walker_states.status===3 && this.arrowinfo===\'r\'" class="arrow_style"\n\n              name="arrow-round-forward"></ion-icon>\n\n            <ion-icon *ngIf="api.walker_states.status===3 && this.arrowinfo===\'f\'" class="arrow_style"\n\n              name="arrow-round-up"></ion-icon>\n\n            <p *ngIf="api.walker_states.status!=3" class="distance_text">\n\n              {{this.param.datatext.timeofpauses}}\n\n            </p>\n\n            <p *ngIf="api.walker_states.status!=3" class="distance">\n\n              {{this.param.tablewalklogs.walk_timeofpauses}} s\n\n            </p>\n\n          </ion-item>\n\n        </ion-col>\n\n\n\n        <ion-col col-sm-3 no-padding>\n\n          <button *ngIf="api.walker_states.status!=3" (mouseup)="sendHelp($event)" [disabled]="is_sendmail"\n\n            class="btnhelp">\n\n            <ion-icon top color="light" name="call" class="icon_style">\n\n              <p class="legende">{{this.param.datatext.btn_help}}</p>\n\n            </ion-icon>\n\n          </button>\n\n          <button *ngIf="api.walker_states.status===3" (mouseup)="sendHelp($event)" [disabled]="is_sendmail"\n\n            class="btnhelp2">\n\n            <ion-icon top color="light" name="call" class="icon_style">\n\n              <p class="legende">{{this.param.datatext.btn_help}}</p>\n\n            </ion-icon>\n\n          </button>\n\n          <div *ngIf="api.walker_states.status===3 && api.position_state===2" no-padding class="goodpose"></div>\n\n          <div *ngIf="api.walker_states.status===3 && api.position_state===1" no-padding class="badpose"></div>\n\n        </ion-col>\n\n      </ion-row>\n\n    </ion-grid>\n\n  </ion-card>\n\n</ion-content>'/*ion-inline-end:"C:\Users\laele\Documents\KomApp\guidedwalk\src\pages\walk\walk.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_6__services_speech_service__["a" /* SpeechService */],
            __WEBPACK_IMPORTED_MODULE_4__services_popup_service__["a" /* PopupService */],
            __WEBPACK_IMPORTED_MODULE_3__services_alert_service__["a" /* AlertService */],
            __WEBPACK_IMPORTED_MODULE_5__services_param_service__["a" /* ParamService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__services_api_service__["a" /* ApiService */]])
    ], WalkPage);
    return WalkPage;
    var WalkPage_1;
}());

//# sourceMappingURL=walk.js.map

/***/ }),

/***/ 221:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RecofacePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_param_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_api_service__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_alert_service__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__choose_assistance_choose_assistance__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_popup_service__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_speech_service__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_common_http__ = __webpack_require__(51);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var RecofacePage = /** @class */ (function () {
    function RecofacePage(loadingCtrl, speech, popup, param, alert, api, httpClient, navCtrl) {
        var _this = this;
        this.loadingCtrl = loadingCtrl;
        this.speech = speech;
        this.popup = popup;
        this.param = param;
        this.alert = alert;
        this.api = api;
        this.httpClient = httpClient;
        this.navCtrl = navCtrl;
        this.choosePage = __WEBPACK_IMPORTED_MODULE_5__choose_assistance_choose_assistance__["a" /* ChooseAssistancePage */];
        this.hidel = false;
        this.hidep = false;
        this.cptnoreco = 0;
        this.speech.getVoice();
        this.api.listener_camera.subscribe(function (message) {
            console.log('Received message on ' + _this.api.listener_camera.name + ': ');
            _this.imgRos = document.getElementById('imgcompressed');
            //console.log(message);
            var bufferdata = _this.convertDataURIToBinary2(message.data);
            var blob = new Blob([bufferdata], { type: 'image/jpeg' });
            var blobUrl = URL.createObjectURL(blob);
            _this.imgRos.src = blobUrl;
        });
    }
    RecofacePage.prototype.convertDataURIToBinary2 = function (dataURI) {
        var raw = window.atob(dataURI);
        var rawLength = raw.length;
        var array = new Uint8Array(new ArrayBuffer(rawLength));
        for (var i = 0; i < rawLength; i++) {
            array[i] = raw.charCodeAt(i);
        }
        return array;
    };
    RecofacePage.prototype.ngOnInit = function () {
        var _this = this;
        this.recointerv = setInterval(function () { return _this.scanface(); }, 500);
        this.authentify = "connect";
        setTimeout(function () {
            _this.speech.speak(_this.param.datatext.getclose);
        }, 1000);
    };
    RecofacePage.prototype.scanface = function () {
        var _this = this;
        if (this.cptnoreco > 25) {
            this.cptnoreco = 0;
            this.popup.showToastRed(this.param.datatext.noreco, 5000, "bottom");
            this.speech.speak(this.param.datatext.noreco);
        }
        //console.log("reco de visage en cours");
        var errors = new Set(["-1", "-2", "-3", "-4"]);
        var name = this.api.preco;
        if (this.authentify == "connect") {
            if (!errors.has(name)) {
                var index = this.param.users.indexOf(name);
                if (index !== -1) {
                    this.api.selectUser = name;
                    this.api.launchreco(false);
                    clearInterval(this.recointerv);
                    this.loading = this.loadingCtrl.create({});
                    this.loading.present();
                    this.popup.showToast(this.param.datatext.hi + name + ". " + this.param.datatext.nicetosee, 5000, "bottom");
                    this.speech.speak(this.param.datatext.hi + name);
                    this.cptnoreco = 0;
                    setTimeout(function () {
                        _this.loading.dismiss();
                        _this.navCtrl.setRoot(_this.choosePage);
                    }, 3000);
                }
                else {
                    this.cptnoreco += 1;
                    console.log("name not in bdd tablet");
                }
            }
            else {
                console.log(name);
                if (name == "-1") {
                    this.cptnoreco += 1;
                }
            }
        }
        else {
            console.log("scan reco register");
        }
    };
    RecofacePage.prototype.cleantext = function (text) {
        //text=text.replace(new RegExp("[<>&$§µ£#*_|`¤~(){}°²%/]", "g"),'');
        text = text.replace(new RegExp("[\"]", "g"), '');
        text = text.replace(new RegExp(/\\/, "g"), '');
        text = text.replace(new RegExp(/[\[\]]/, "g"), '');
        return text;
    };
    RecofacePage.prototype.addUser = function (ev) {
        ev.preventDefault();
        console.log(this.param.users);
        var index = this.param.users.indexOf(this.username);
        if (index !== -1) {
            this.popup.showToastRed(this.param.datatext.userexist, 3000, "bottom");
        }
        else {
            this.loading = this.loadingCtrl.create({});
            this.loading.present();
            this.addfacejetson();
        }
    };
    RecofacePage.prototype.handleChangeSeg = function () {
        var _this = this;
        console.log(this.authentify);
        if (this.authentify == "connect") {
            this.recointerv = setInterval(function () { return _this.scanface(); }, 500);
            this.speech.speak(this.param.datatext.getclose);
        }
        else {
            clearInterval(this.recointerv);
            this.speech.speak(this.param.datatext.register);
        }
    };
    RecofacePage.prototype.anonymous = function (ev) {
        ev.preventDefault();
        clearInterval(this.recointerv);
        this.api.launchreco(false);
        this.navCtrl.setRoot(this.choosePage);
    };
    RecofacePage.prototype.addfacejetson = function () {
        var _this = this;
        this.speech.speak(this.param.datatext.getclose);
        //console.log("add face jetson")
        setTimeout(function () {
            _this.api.addUserJetson(_this.username);
        }, 3000);
        setTimeout(function () {
            if (_this.api.useradded) {
                _this.param.users.push(_this.username);
                _this.param.addUser(_this.username, _this.login, _this.password);
                _this.popup.showToast(_this.param.datatext.useradd, 3000, "bottom");
            }
            else {
                _this.popup.showToastRed(_this.param.datatext.failscan, 3000, "bottom");
                _this.speech.speak(_this.param.datatext.failscan);
            }
        }, 5000);
        setTimeout(function () {
            _this.param.getWalkUser();
            _this.loading.dismiss();
            if (_this.api.useradded) {
                _this.api.useradded = false;
                _this.authentify = "connect";
            }
        }, 6000);
    };
    RecofacePage.prototype.clicValidate = function (ev) {
        var _this = this;
        ev.preventDefault();
        this.username = this.cleantext(this.username);
        this.login = this.cleantext(this.login);
        this.password = this.cleantext(this.password);
        if (this.authentify == "connect") {
            var index = this.param.users.indexOf(this.username);
            console.log(index);
            if (index !== -1) {
                var headers = { 'X-Shapes-Key': '7Msbb3w^SjVG%j' };
                console.log("testapiloginstart");
                var body = { "email": this.login, "password": this.password };
                this.httpClient.post("https://kubernetes.pasiphae.eu/shapes/asapa/auth/login", body, { headers: headers }).subscribe(function (data) {
                    console.log(data);
                    //enregistrer token
                    //console.log(data[items][0]);
                    //this.token = data.items[0].token;
                    _this.popup.showToast(_this.param.datatext.hi + name + ". " + _this.param.datatext.nicetosee, 3000, "bottom");
                    clearInterval(_this.recointerv);
                    _this.api.selectUser = _this.username;
                    _this.param.tablewalklogs.walkuser_id = _this.username;
                    console.log(_this.param.tablewalklogs.walkuser_id);
                    _this.navCtrl.setRoot(_this.choosePage);
                    _this.api.launchreco(false);
                    // text connexion
                }, function (error) {
                    console.log('error : ', error);
                    _this.speech.speak(_this.param.datatext.errorreco);
                    _this.popup.showToastRed(_this.param.datatext.notfound, 5000, "bottom");
                });
            }
            else {
                this.speech.speak(this.param.datatext.errorreco);
                this.popup.showToastRed(this.param.datatext.notfound, 5000, "bottom");
            }
        }
        else {
            var headers = { 'X-Shapes-Key': '7Msbb3w^SjVG%j' };
            console.log("testapiloginstart");
            var body = { "email": this.login, "password": this.password };
            this.httpClient.post("https://kubernetes.pasiphae.eu/shapes/asapa/auth/login", body, { headers: headers }).subscribe(function (data) {
                console.log(data);
                //this.token = data.items[0].token;
                //mofid text enregistrement
                //this.popup.showToast(this.param.datatext.useradd,3000,"middle");
                _this.addUser(ev);
            }, function (error) {
                console.log('error : ', error);
                _this.speech.speak(_this.param.datatext.errorreco);
                _this.popup.showToastRed(_this.param.datatext.notfound, 5000, "bottom");
            });
        }
    };
    RecofacePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: "page-recoface",template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\guidedwalk\src\pages\recoface\recoface.html"*/'<ion-header no-border>\n\n  <headpage pageName="recoface"></headpage>\n\n</ion-header>\n\n<ion-content padding>\n\n  <ion-card text-center class="big_card">\n\n    <ion-grid class="heightstyle">\n\n      <ion-row class="heightstyle">\n\n        <ion-col col-5 class="heightstyle2">\n\n          <ion-row>\n\n          <ion-segment id="test" [(ngModel)]="authentify" (ionChange)="handleChangeSeg()">\n\n            <ion-segment-button value="connect" style="font-size: x-large">\n\n              {{param.datatext.connection}}\n\n            </ion-segment-button>\n\n            <ion-segment-button value="login" style="font-size: x-large">\n\n              {{param.datatext.registration}}\n\n            </ion-segment-button>\n\n          </ion-segment>\n\n       \n\n        <ion-item style=" margin-top: 20px !important;">\n\n          <ion-label style="font-size: large">{{param.datatext.uname}} : </ion-label>\n\n          <ion-input  [(ngModel)]="username"  [clearInput]="true"  style="font-size: large" ></ion-input>\n\n        </ion-item>\n\n      \n\n        <ion-item style=" margin-top: 20px !important;">\n\n          <ion-label style="font-size: large">{{param.datatext.loginshapes}} : </ion-label>\n\n          <ion-input  [(ngModel)]="login"  [clearInput]="true"  style="font-size: large" [type]="hidel ? \'password\' : \'text\'"></ion-input>\n\n          <ion-icon style="font-size: large;" slot="end" [name]="hidel ? \'eye\' : \'eye-off\'" (click)="hidel = !hidel"></ion-icon>\n\n        </ion-item>\n\n      \n\n        <ion-item style=" margin-top: 20px !important;">\n\n          <ion-label style="font-size: large">{{param.datatext.pswshapes}} : </ion-label>\n\n          <ion-input  [(ngModel)]="password" [clearInput]="true"  style="font-size: large"   [type]="hidep ? \'password\' : \'text\'"></ion-input>\n\n          <ion-icon style="font-size: large;" slot="end" [name]="hidep ? \'eye\' : \'eye-off\'" (click)="hidep = !hidep"></ion-icon>\n\n        </ion-item>\n\n        \n\n        <button\n\n        ion-button\n\n        [color]="buttonColor"\n\n        class="btn_cam"\n\n        (mouseup)="clicValidate($event)"\n\n      >\n\n      {{param.datatext.valide}}\n\n      </button></ion-row>\n\n\n\n      <ion-row >\n\n      <button\n\n      (mouseup)="anonymous($event)"\n\n      style="\n\n        width: 100%;\n\n        color: rgb(26, 156, 195);\n\n        text-decoration: underline;\n\n        font-size: large;\n\n      "\n\n      ion-button\n\n      clear\n\n    >\n\n      <div>{{param.datatext.anonym}}</div>\n\n    </button>\n\n\n\n      </ion-row>\n\n     \n\n         \n\n        </ion-col>\n\n\n\n        <ion-col col-7 class="container">\n\n\n\n          \n\n            <img class="cssimg" id="imgcompressed" />\n\n          \n\n\n\n        </ion-col>\n\n      </ion-row>\n\n    </ion-grid>\n\n\n\n  </ion-card>\n\n \n\n\n\n\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\laele\Documents\KomApp\guidedwalk\src\pages\recoface\recoface.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_7__services_speech_service__["a" /* SpeechService */],
            __WEBPACK_IMPORTED_MODULE_6__services_popup_service__["a" /* PopupService */],
            __WEBPACK_IMPORTED_MODULE_2__services_param_service__["a" /* ParamService */],
            __WEBPACK_IMPORTED_MODULE_4__services_alert_service__["a" /* AlertService */],
            __WEBPACK_IMPORTED_MODULE_3__services_api_service__["a" /* ApiService */],
            __WEBPACK_IMPORTED_MODULE_8__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */]])
    ], RecofacePage);
    return RecofacePage;
}());

//# sourceMappingURL=recoface.js.map

/***/ }),

/***/ 222:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HeadpageComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__pages_choose_assistance_choose_assistance__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_api_service__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_tuto_tuto__ = __webpack_require__(223);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_popup_service__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_alert_service__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_param_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_param_param__ = __webpack_require__(235);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_walklogs_walklogs__ = __webpack_require__(241);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__angular_platform_browser__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__services_speech_service__ = __webpack_require__(36);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};













var HeadpageComponent = /** @class */ (function () {
    function HeadpageComponent(toastCtrl, alert, param, popup, navCtrl, speech, api, sanitizer) {
        this.toastCtrl = toastCtrl;
        this.alert = alert;
        this.param = param;
        this.popup = popup;
        this.navCtrl = navCtrl;
        this.speech = speech;
        this.api = api;
        this.sanitizer = sanitizer;
        this.choosePage = __WEBPACK_IMPORTED_MODULE_1__pages_choose_assistance_choose_assistance__["a" /* ChooseAssistancePage */];
        this.tutoPage = __WEBPACK_IMPORTED_MODULE_4__pages_tuto_tuto__["a" /* TutoPage */];
        this.paramPage = __WEBPACK_IMPORTED_MODULE_8__pages_param_param__["a" /* ParamPage */];
        this.walklogPage = __WEBPACK_IMPORTED_MODULE_9__pages_walklogs_walklogs__["a" /* WalkLogsPage */];
        this.api.appStart = false;
        this.microon = false;
        this.cpt_battery = 0;
        this.cpt_open = 0;
        if (this.param.allowspeech == 1) {
            this.api.volumeState = 1;
        }
        else {
            this.api.volumeState = 0;
        }
    }
    HeadpageComponent.prototype.ionViewWillLeave = function () {
        clearInterval(this.update);
        clearInterval(this.lowbattery);
        clearInterval(this.source);
    };
    HeadpageComponent.prototype.ngOnDestroy = function () {
        clearInterval(this.update);
        clearInterval(this.lowbattery);
        clearInterval(this.source);
    };
    HeadpageComponent.prototype.onTouchHelp = function (ev) {
        ev.preventDefault();
        if (!this.api.towardDocking && !this.api.inUse) {
            this.alert.displayTuto(this.api.mailAddInformationBasic());
            this.api.walkercmdstartdetect();
            this.navCtrl.push(this.tutoPage);
        }
    };
    HeadpageComponent.prototype.onTouchVolume = function (ev) {
        ev.preventDefault();
        this.speech.synth.cancel();
        if (this.api.volumeState === 0) {
            this.api.volumeState = 1;
            this.param.allowspeech = 1;
        }
        else {
            this.api.volumeState = 0;
            this.param.allowspeech = 0;
        }
    };
    HeadpageComponent.prototype.updateBattery = function () {
        if (this.api.statusRobot === 2) {
            // battery logo will not apppear
            this.api.batteryState = 10;
        }
        if (this.api.battery_status.status < 2) {
            //charging
            this.api.batteryState = 4;
        }
        else if (this.api.battery_status.status === 3 ||
            this.api.battery_status.remaining <= this.param.battery.critical) {
            //critical
            this.api.batteryState = 0;
        }
        else if (this.api.battery_status.remaining > this.param.battery.high) {
            //hight
            this.api.batteryState = 3;
        }
        else if (this.api.battery_status.remaining > this.param.battery.low) {
            //mean
            this.api.batteryState = 2;
        }
        else if (this.api.battery_status.remaining <= this.param.battery.low) {
            //low
            this.api.batteryState = 1;
        }
        else {
            // battery logo will not apppear
            this.api.batteryState = 10;
        }
    };
    HeadpageComponent.prototype.onTouchStatus = function (ev) {
        ev.preventDefault();
        if (this.api.statusRobot === 2) {
            this.popup.statusRedPresent();
        }
        else {
            this.okToast(this.param.datatext.statusGreenPresent_message);
        }
    };
    HeadpageComponent.prototype.accessparam = function () {
        this.navCtrl.push(this.paramPage);
    };
    HeadpageComponent.prototype.accesswalklog = function () {
        this.navCtrl.push(this.walklogPage);
    };
    HeadpageComponent.prototype.onTouchParam = function (ev) {
        ev.preventDefault();
        //this.speech.speak("Bonjour les amis. Comment ça va aujourd'hui ! ");
        if (!this.api.towardDocking && !this.api.inUse) {
            if (this.pageName === "param" || this.pageName === "sms" || this.pageName === "mail" || this.pageName === "langue") {
                console.log("already");
            }
            else {
                //this.navCtrl.push(this.paramPage);
                this.popup.askpswd();
            }
        }
    };
    HeadpageComponent.prototype.onTouchWalkLogs = function (ev) {
        ev.preventDefault();
        //this.navCtrl.push(this.walklogPage);
        if (!this.api.towardDocking && !this.api.inUse) {
            if (this.pageName === "param" || this.pageName === "sms" || this.pageName === "mail" || this.pageName === "langue") {
                console.log("already");
            }
            else {
                //this.navCtrl.push(this.paramPage);
                this.popup.askpswdWalk();
            }
        }
    };
    HeadpageComponent.prototype.onTouchBattery = function (ev) {
        ev.preventDefault();
        this.okToast(this.param.datatext.battery + " : " + this.api.battery_status.remaining + "%");
        //     if(!this.api.towardDocking && !this.api.inUse){
        //       if(this.pageName==="free_assistance" )
        //       {
        //         if(this.api.docking_status.status !=3){ // if not on docking
        //           this.api.walkercmdstopdetect();
        //           if(this.api.batteryState===0){
        //             this.popup.lowBattery();
        //           }
        //           else{
        //             this.popup.goDockingConfirm();
        //           }
        //         }
        //         if(this.api.docking_status.status ===3){ //if on docking
        //           this.api.walkercmdstopdetect();
        //           this.popup.leaveDockingConfirm();
        //         }
        //       }
        //       else{
        //         this.clicOnMenu();
        //       }
        //     }else{
        //       this.okToast(this.param.datatext.battery+" : "+this.api.battery_status.remaining+"%")
        //   }
    };
    HeadpageComponent.prototype.onTouchWifi = function (ev) {
        ev.preventDefault();
        if (!this.api.wifiok) {
            this.okToast(this.param.datatext.nointernet);
        }
        else {
            this.okToast(this.param.datatext.AlertConnectedToI);
        }
    };
    HeadpageComponent.prototype.okToast = function (m) {
        var toast = this.toastCtrl.create({
            message: m,
            duration: 3000,
            position: 'middle',
            cssClass: "toastok"
        });
        toast.present();
    };
    HeadpageComponent.prototype.monitoringBattery = function () {
        if (this.api.batteryState === 0) {
            this.popup.lowBattery();
            this.alert.lowBattery(this.api.mailAddInformationBasic());
            this.alert.Battery_c();
            if (!this.api.towardDocking) {
                this.api.walkercmdstopdetect();
            }
            clearInterval(this.lowbattery);
        }
    };
    HeadpageComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.updateB = setInterval(function () { return _this.updateBand(); }, 500);
        this.popup.onSomethingHappened1(this.accessparam.bind(this));
        this.popup.onSomethingHappened3(this.accesswalklog.bind(this));
        // bouton 's text (Return / Exit), to the left of the header, depends on the page you are on
        if (this.pageName === "recoface") {
            this.update = setInterval(function () { return _this.getUpdate(); }, 500); //update hour, battery every 1/2 secondes
            this.lowbattery = setInterval(function () { return _this.monitoringBattery(); }, 60000);
            this.source = setInterval(function () { return _this.api.jetsonOK(); }, 2000);
            this.textBoutonHeader = this.param.datatext.quit;
            this.textHeadBand = this.param.datatext.auth;
        }
        else if (this.pageName === "choose_assistance" && !this.api.recoface) {
            this.update = setInterval(function () { return _this.getUpdate(); }, 500); //update hour, battery every 1/2 secondes
            this.lowbattery = setInterval(function () { return _this.monitoringBattery(); }, 60000);
            this.source = setInterval(function () { return _this.api.jetsonOK(); }, 2000);
            this.textBoutonHeader = this.param.datatext.quit;
        }
        else if (this.pageName === "choose_assistance") {
            this.textBoutonHeader = this.param.datatext.quit;
        }
        else if (this.pageName === "param") {
            this.textBoutonHeader = this.param.datatext.return;
            this.textHeadBand = this.param.datatext.param;
        }
        else if (this.pageName === "sms") {
            this.textBoutonHeader = this.param.datatext.return;
            this.textHeadBand = this.param.datatext.receivesms;
        }
        else if (this.pageName === "mail") {
            this.textBoutonHeader = this.param.datatext.return;
            this.textHeadBand = this.param.datatext.receivemail;
        }
        else if (this.pageName === "langue") {
            this.textBoutonHeader = this.param.datatext.return;
            this.textHeadBand = this.param.datatext.changelangage;
        }
        else if (this.pageName === "password") {
            this.textBoutonHeader = this.param.datatext.return;
            this.textHeadBand = this.param.datatext.editpswd;
        }
        else if (this.pageName === "start_assistance") {
            this.textBoutonHeader = this.param.datatext.return;
            this.textHeadBand = this.param.datatext.gobehindrobot;
        }
        else if (this.pageName === "walk") {
            this.textBoutonHeader = this.param.datatext.return;
            this.textHeadBand = this.param.datatext.walkInProgress;
        }
        else if (this.pageName === "walklogs") {
            this.textBoutonHeader = this.param.datatext.return;
            this.textHeadBand = this.param.datatext.walklog.toUpperCase();
        }
        else if (this.pageName === "user") {
            this.textBoutonHeader = this.param.datatext.return;
            this.textHeadBand = this.param.datatext.users.toUpperCase();
        }
        else {
            this.textBoutonHeader = this.param.datatext.return;
            this.textHeadBand = this.param.datatext.tutorial;
        }
    };
    HeadpageComponent.prototype.updateBand = function () {
        if (this.api.towardDocking) {
            this.textHeadBand = this.param.datatext.godocking;
        }
        if (this.pageName === "choose_assistance") {
            if (this.api.inUse) {
                this.textHeadBand = this.param.datatext.walkInProgress;
            }
            else {
                if (this.api.selectMode == "destination") {
                    this.textHeadBand = this.param.datatext.choosedestination;
                }
                else {
                    this.textHeadBand = this.param.datatext.chooseround;
                }
            }
        }
        else if (this.pageName === "start_assistance") {
            if (this.api.position_state !== 2) {
                this.textHeadBand = this.param.datatext.gobehindrobot;
            }
            else if (this.api.position_state === 2) {
                this.textHeadBand = this.param.datatext.wait;
            }
        }
    };
    HeadpageComponent.prototype.getUpdate = function () {
        if (!this.api.towardDocking) {
            if (this.api.walker_states.status === 1) {
                this.api.appStart = true;
            }
            if (this.pageName === "choose_assistance" && this.api.walker_socket.readyState === 1 && this.api.walker_states.status === 0 && !this.api.appStart) {
                //initialize the detection
                //this.api.walkercmdstartdetect();
                this.api.appStart = true;
            }
        }
        if (this.api.close_app) {
            clearInterval(this.update);
        }
        this.updateBattery();
        var now = new Date().toLocaleString('fr-FR', { hour: 'numeric', minute: 'numeric' });
        this.api.hour = now;
        if (this.api.statusRobot === 0 && this.api.batteryState === 10) {
            this.cpt_battery += 1;
        }
        else {
            this.cpt_battery = 0;
        }
        if (this.cpt_battery > 10) {
            this.api.statusRobot = 2;
        }
    };
    HeadpageComponent.prototype.clicOnMenu = function (ev) {
        ev.preventDefault();
        // the action depends on the page you are on
        if (this.pageName === 'tuto' || this.pageName === 'param' || this.pageName === 'walklogs') {
            //this.api.walkercmdstartdetect();
            this.navCtrl.popToRoot();
        }
        else if (this.pageName === 'sms' || this.pageName === 'mail' || this.pageName === 'langue' || this.pageName === 'password' || this.pageName === 'user') {
            this.navCtrl.pop();
        }
        else if (this.pageName === 'start_assistance' || this.pageName === 'walk') {
            //this.api.walkercmdstopdetect();
            //this.navCtrl.popToRoot();
        }
        else {
            if (this.api.towardDocking) {
                this.api.abortNavHttp();
                this.api.towardDocking = false;
                this.popup.quitConfirm();
            }
            else {
                this.api.close_app = true;
                this.alert.appClosed(this.api.mailAddInformationBasic());
                //stop the enslavement of the legs and quit the app
                this.api.walkercmdstopdetect();
                setTimeout(function () {
                    window.close();
                }, 500);
            }
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], HeadpageComponent.prototype, "pageName", void 0);
    HeadpageComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'headpage',template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\guidedwalk\src\components\headpage\headpage.html"*/'<!-- Html component of the HMI header with the Back / Exit button, the time, the sound buttons, microphone, parameters etc ...-->\n\n\n\n<ion-navbar hideBackButton>\n\n  <ion-buttons left class="btn_menu_size">\n\n    <button  ion-button solid large class="btn_menu" (mouseup)="clicOnMenu($event)" [disabled]="this.pageName===\'walk\'"> \n\n      <!-- the text of the button depends on the page you are on -->\n\n      <font class="font_menu_size" >{{this.textBoutonHeader}}</font>\n\n    </button>\n\n  </ion-buttons>\n\n\n\n  <ion-title text-center><font class="hour_size">{{api.hour}}</font></ion-title>\n\n\n\n  <ion-buttons right>\n\n    <button ion-button class="btn_header" (mouseup)="onTouchHelp($event)"  >\n\n      <img class="imgicon" src="./assets/imgs/help.png"/>\n\n    </button>\n\n    <button ion-button class="btn_header" (mouseup)="onTouchParam($event)"   >\n\n      <img class="imgicon" src="./assets/imgs/parameter.png"/>\n\n    </button>\n\n    <button ion-button class="btn_header" (mouseup)="onTouchWalkLogs($event)"  >\n\n      <img class="imgicon" src="./assets/imgs/walkdata.png"/>\n\n    </button>\n\n    <!-- <button ion-button disabled class="btn_header" >\n\n      <img *ngIf="!microon" class="imgicon" src="./assets/imgs/microoff.png"/>\n\n      <img *ngIf="microon" class="imgicon" src="./assets/imgs/microon.png"/>\n\n    </button> -->\n\n    <button ion-button class="btn_header"  (mouseup)="onTouchVolume($event)">\n\n      <img *ngIf="api.volumeState === 0" class="imgicon" src="./assets/imgs/volumemute.png"/>\n\n      <img *ngIf="api.volumeState === 1" class="imgicon" src="./assets/imgs/volumelow.png"/>\n\n      <img *ngIf="api.volumeState === 2" class="imgicon" src="./assets/imgs/volumehight.png"/>\n\n    </button>\n\n    <button ion-button class="btn_header" (mouseup)="onTouchWifi($event)" >\n\n      <img *ngIf="!api.wifiok" class="imginternet" src="./assets/imgs/wifioff.png"/>\n\n      <img *ngIf="api.wifiok" class="imginternet" src="./assets/imgs/wifion.png"/>\n\n    </button>\n\n    <button ion-button class="btn_header" (mouseup)="onTouchStatus($event)">\n\n      <img  *ngIf="api.statusRobot===0" class="imgwifi" src="./assets/imgs/statusgreen.png"/>\n\n      <img  *ngIf="api.statusRobot===1" class="imgwifi" src="./assets/imgs/statusorange.png"/>\n\n      <img  *ngIf="api.statusRobot===2" class="imgwifi" src="./assets/imgs/statusred.png"/>\n\n    </button>\n\n    <button  ion-button class="btn_header" (mouseup)="onTouchBattery($event)" >\n\n      <img *ngIf="api.batteryState === 0" class="imgbattery" src="./assets/imgs/batteryoff.png"/>\n\n      <img *ngIf="api.batteryState === 1" class="imgbattery" src="./assets/imgs/batterylow.png"/>\n\n      <img *ngIf="api.batteryState === 2" class="imgbattery" src="./assets/imgs/batterymean.png"/>\n\n      <img *ngIf="api.batteryState === 3" class="imgbattery" src="./assets/imgs/batteryhight.png"/>\n\n      <img *ngIf="api.batteryState === 4" class="imgbattery" src="./assets/imgs/batterycharge.png"/>\n\n    </button>\n\n  </ion-buttons>\n\n\n\n</ion-navbar>\n\n\n\n<ion-toolbar no-padding>\n\n  <div class="scroll_parent">\n\n    <ion-title class="scroll">\n\n      <font class="scroll_text">{{this.textHeadBand}}</font>\n\n    </ion-title>\n\n  </div>\n\n\n\n</ion-toolbar>\n\n'/*ion-inline-end:"C:\Users\laele\Documents\KomApp\guidedwalk\src\components\headpage\headpage.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* ToastController */], __WEBPACK_IMPORTED_MODULE_6__services_alert_service__["a" /* AlertService */], __WEBPACK_IMPORTED_MODULE_7__services_param_service__["a" /* ParamService */], __WEBPACK_IMPORTED_MODULE_5__services_popup_service__["a" /* PopupService */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_11__services_speech_service__["a" /* SpeechService */], __WEBPACK_IMPORTED_MODULE_3__services_api_service__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_10__angular_platform_browser__["c" /* DomSanitizer */]])
    ], HeadpageComponent);
    return HeadpageComponent;
}());

//# sourceMappingURL=headpage.js.map

/***/ }),

/***/ 223:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TutoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_param_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_pdfjs_dist_webpack_js__ = __webpack_require__(322);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_pdfjs_dist_webpack_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_pdfjs_dist_webpack_js__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var TutoPage = /** @class */ (function () {
    function TutoPage(sanitizer, param) {
        this.sanitizer = sanitizer;
        this.param = param;
        this.pageNum = 1;
        this.pdfSrc = "https://vadimdez.github.io/ng2-pdf-viewer/assets/pdf-test.pdf";
        this.PDFJSViewer = __WEBPACK_IMPORTED_MODULE_3_pdfjs_dist_webpack_js__;
        this.currPage = 1; //Pages are 1-based not 0-based
        this.numPages = 0;
        this.thePDF = null;
        this.pageRendering = false;
        this.pageNumPending = null;
    }
    TutoPage.prototype.ngOnInit = function () {
    };
    TutoPage.prototype.ionViewDidLoad = function () {
        // deuxième méthode
        this.launchdoc();
    };
    TutoPage.prototype.launchdoc = function () {
        var _this = this;
        //This is where you start
        __WEBPACK_IMPORTED_MODULE_3_pdfjs_dist_webpack_js__["getDocument"](this.param.datatext.URL_assistancemobility).then(function (pdf) {
            //Set PDFJS global object (so we can easily access in our page functions
            _this.thePDF = pdf;
            //How many pages it has
            _this.numPages = pdf.numPages;
            //Start with first page
            pdf.getPage(1).then(function (page) { _this.handlePages(page); });
        });
    };
    TutoPage.prototype.handlePages = function (page) {
        var _this = this;
        //This gives us the page's dimensions at full scale
        var viewport = page.getViewport(2);
        //We'll create a canvas for each page to draw it on
        var canvas = document.createElement("canvas");
        canvas.style.display = "block";
        var context = canvas.getContext('2d');
        canvas.height = viewport.height;
        canvas.width = viewport.width;
        //Draw it on the canvas
        page.render({ canvasContext: context, viewport: viewport });
        //Add it to the web page
        //document.body.appendChild( canvas );
        //console.log(document.getElementById("ionbody"));
        document.getElementById("ionbody").appendChild(canvas);
        var line = document.createElement("hr");
        document.getElementById("ionbody").appendChild(line);
        //console.log(this.currPage);
        //Move to next page
        this.currPageAdd();
        if (this.thePDF !== null && this.currPage <= this.numPages) {
            this.thePDF.getPage(this.currPage).then(function (page) { _this.handlePages(page); });
        }
    };
    TutoPage.prototype.currPageAdd = function () {
        this.currPage = this.currPage + 1;
    };
    TutoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-tuto',template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\guidedwalk\src\pages\tuto\tuto.html"*/'<!-- home page html template -->\n\n<ion-header no-border>\n\n  <headpage pageName="tuto"></headpage>\n\n</ion-header>\n\n\n\n<ion-content padding >\n\n  <ion-scroll scrollY="true" style="height:100%;width:80%;left:50%;transform:translateX(-50%); " >\n\n    <div class="center" style="height:100%;">\n\n      <div id="ionbody" style="display: flex;flex-grow: 1;  flex-direction: column;background: #ddd;overflow-y: auto;" width="60%" height="100%">\n\n\n\n      </div>\n\n    </div>\n\n  </ion-scroll>\n\n</ion-content>'/*ion-inline-end:"C:\Users\laele\Documents\KomApp\guidedwalk\src\pages\tuto\tuto.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["c" /* DomSanitizer */], __WEBPACK_IMPORTED_MODULE_2__services_param_service__["a" /* ParamService */]])
    ], TutoPage);
    return TutoPage;
}());

//# sourceMappingURL=tuto.js.map

/***/ }),

/***/ 23:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AlertService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__param_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__(51);
// to send mail and sms alert
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AlertService = /** @class */ (function () {
    function AlertService(app, param, http) {
        this.app = app;
        this.param = param;
        this.http = http;
        this.videoWidth = 0;
        this.videoHeight = 0;
        this.constraints = {
            //audio: false,
            //video: {deviceId: "f9860b260e18d7fd1687d8567e21b49c4bb26f87606020741717e11f6179f409"},
            video: true,
            facingMode: "environment",
            width: { ideal: 1920 },
            height: { ideal: 1080 }
        };
    }
    AlertService.prototype.ngOnInit = function () {
    };
    AlertService.prototype.handleError = function (error) {
        console.log('Error: ', error);
    };
    // function send mail without image
    // bod : corps du mail
    // suj : sujet du mail
    // dest : adresse mail du destinataire
    AlertService.prototype.sendmailPerso = function (bod, suj, dest, data, usern, passw, robotn) {
        var _this = this;
        //console.log("sendmail");
        var usermailData = {
            username: usern,
            email: data,
            bcc: dest,
            date: new Date(Date.now()).toDateString(),
            subject: suj,
            message: bod,
            password: passw,
            robotname: robotn
        };
        var link = "http://localhost/ionicDB/SendMail/sendmail.php";
        var postParams = JSON.stringify(usermailData);
        //console.log(postParams);
        if (this.allowmail) {
            this.http.post(link, postParams).subscribe(function (response) {
                //console.log(response);
                //console.log(this.param.durationNS);
                if (response == "OK") {
                    var s = _this.param.serialnumber + ' : Duration App';
                    if (suj == s) {
                        _this.param.updateDurationNS(_this.param.durationNS[0].id_duration);
                        _this.param.durationNS.shift();
                        //console.log(this.param.durationNS);
                    }
                }
            }, function (error) {
                console.log(error);
            });
        }
    };
    AlertService.prototype.sendAlertData = function (bod, suj) {
        this.sendmailPerso(bod, suj, this.param.datamaillist, this.param.datamail, this.param.datamail, this.param.maildatapassw, this.param.datamail);
    };
    AlertService.prototype.sendAlertClient = function (bod, suj) {
        if (this.param.maillist.length) {
            this.sendmailPerso(bod, suj, this.param.maillist, this.param.datamail, this.param.robotmail, this.param.mailrobotpassw, this.param.name);
        }
    };
    AlertService.prototype.checkwifi = function (bool) {
        this.allowmail = bool;
    };
    // function send mail with image
    // bod : corps du mail
    // suj : sujet du mail
    // dest : adresse mail du destinataire
    // url : image attachment
    AlertService.prototype.sendmailImgperso = function (url, bod, suj, dest, data, usern, passw, robotn) {
        //console.log("sendmail");
        var usermailData = {
            username: usern,
            email: data,
            bcc: dest,
            date: new Date(Date.now()).toDateString(),
            subject: suj,
            message: bod,
            password: passw,
            robotname: robotn,
            url: url
        };
        var link = "http://localhost/ionicDB/SendMail/sendmailImg.php";
        var postParams = JSON.stringify(usermailData);
        //console.log(postParams);
        if (this.allowmail) {
            this.http.post(link, postParams).subscribe(function (response) {
                //console.log(response);
            });
        }
    };
    AlertService.prototype.sendAlertImgData = function (url, bod, suj) {
        this.sendmailImgperso(url, bod, suj, this.param.datamail, this.param.datamail, this.param.datamail, this.param.maildatapassw, this.param.datamail);
    };
    AlertService.prototype.sendAlertImgClient = function (url, bod, suj) {
        if (this.param.maillist.length) {
            this.sendmailImgperso(url, bod, suj, this.param.maillist, this.param.datamail, this.param.robotmail, this.param.mailrobotpassw, this.param.name);
        }
    };
    // function send sms
    AlertService.prototype.sendsmsperso = function (mess, num) {
        messageAenvoyer = mess;
        numTelToElement = num;
        envoyerSMS();
    };
    AlertService.prototype.appError = function (info) {
        this.sendAlertData('<br> An error occurred, the round application has been reload automatically' +
            '<br> Code alert : 1' +
            info, this.param.serialnumber + ' : Error (App Guided Walk)');
    };
    AlertService.prototype.charging = function (info) {
        this.sendAlertData('<br> The robot has docked successfully' +
            '<br> Code alert : 2' +
            info, this.param.serialnumber + ' : Charging');
    };
    AlertService.prototype.noLongerBlocked = function (info) {
        this.sendAlertData('<br> The robot is no longer blocked' +
            '<br> Code alert : 3' +
            info, this.param.serialnumber + ' : Automatic release');
    };
    AlertService.prototype.manualintervention = function (info) {
        this.sendAlertData('<br> Someone unlocked the robot' +
            '<br> Code alert : 25' +
            info, this.param.serialnumber + ' : Manual release');
    };
    AlertService.prototype.appOpen = function (info) {
        this.sendAlertData('<br> Someone oppened the walkassist app.' +
            '<br> Code alert : 24' +
            info, this.param.serialnumber + ' : App opened (Guided walk)');
    };
    AlertService.prototype.duration = function (olddata, info) {
        console.log("duration alert");
        this.sendAlertData('<br> Here is a count of the duration of use of the apps in minutes' +
            '<br> Date duration : ' + olddata.date +
            '<br> Round duration : ' + olddata.round +
            '<br> Patrol duration : ' + olddata.patrol +
            '<br> Walk duration : ' + olddata.walk +
            '<br> Battery duration : ' + olddata.battery +
            '<br> Toolbox duration : ' + olddata.toolbox +
            '<br> Code alert : 26' +
            info, this.param.serialnumber + ' : Duration App');
    };
    AlertService.prototype.naverror = function (info) {
        this.sendAlertData('<br> A navigation error has occurred. The robot may be lost. The nominal current may have been exceeded.' +
            '<br> Code alert : 4' +
            info, this.param.serialnumber + ' : Navigation error');
    };
    AlertService.prototype.lowBattery = function (info) {
        this.sendAlertData('<br> The robot must be sent to the docking station.' +
            '<br> Code alert : 5' +
            info, this.param.serialnumber + ' : Battery Critical');
    };
    AlertService.prototype.errorblocked = function (info) {
        this.sendAlertData('<br> The robot has been stopped because the rated current is exceeded' +
            '<br> Code alert : 6' +
            info, this.param.serialnumber + ' : High current');
    };
    AlertService.prototype.robotLost = function (info) {
        this.sendAlertData('<br> The robot is lost. It must be relocated' +
            '<br> Code alert : 7' +
            info, this.param.serialnumber + ' : Robot Lost');
    };
    AlertService.prototype.blockingdocking = function (info) {
        this.sendAlertData('<br> The robot encountered an obstacle while heading for the docking station' +
            '<br> Code alert : 8' +
            info, this.param.serialnumber + " : Can't reach docking");
    };
    AlertService.prototype.mobAssistSOS = function (info) {
        this.sendAlertData('<br> Somebody is asking for help' +
            '<br> Code alert : 9' +
            info, this.param.serialnumber + ' : SOS');
    };
    AlertService.prototype.mobAssistActivated = function (info) {
        this.sendAlertData('<br> Somebody starts walking with Kompai' +
            '<br> Code alert : 22' +
            info, this.param.serialnumber + ' : Mobility assistance activated');
    };
    AlertService.prototype.appInUse = function (info) {
        this.sendAlertData('<br> Somebody is walking with Kompai' +
            '<br> Code alert : 23' +
            info, this.param.serialnumber + ' : Mobility assistance in use');
    };
    AlertService.prototype.displayTuto = function (info) {
        this.sendAlertData('<br> Someone is reading the tuto' +
            '<br> Code alert : 10' +
            info, this.param.serialnumber + ' : Tuto opened');
    };
    AlertService.prototype.leaveDocking = function (info) {
        this.sendAlertData('<br> Kompai is no longer on docking' +
            '<br> Code alert : 11' +
            info, this.param.serialnumber + ' : Leave docking');
    };
    AlertService.prototype.askCharge = function (info) {
        this.sendAlertData('<br> The robot must move towards docking' +
            '<br> Code alert : 12' +
            info, this.param.serialnumber + ' : Charging request');
    };
    AlertService.prototype.appClosed = function (info) {
        this.sendAlertData('<br> Somebody has closed the round application' +
            '<br> Code alert : 13' +
            info, this.param.serialnumber + ' : App Guided walk closed');
    };
    AlertService.prototype.blocking = function (info) {
        this.sendAlertData('<br> The robot is blocked by an obstacle' +
            '<br> Code alert : 19' +
            info, this.param.serialnumber + ' : Blocking');
    };
    AlertService.prototype.SOS = function (info) {
        this.sendAlertData('<br> Somebody is asking for help' +
            '<br> Code alert : 9' +
            info, this.param.serialnumber + ' : SOS');
    };
    /////////////////////////////////// Mail client
    AlertService.prototype.SOS_c = function () {
        this.sendAlertClient(this.param.datatext.mailSOS_body, this.param.datatext.mailSOS_suj);
    };
    AlertService.prototype.Battery_c = function () {
        this.sendAlertClient(this.param.datatext.mailBattery_body, this.param.datatext.mailBattery_suj);
    };
    AlertService.prototype.Blocked_c = function (url) {
        this.sendAlertImgClient(url, this.param.datatext.mailBlocked_body, this.param.datatext.mailBlocked_suj);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('video'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], AlertService.prototype, "videoElement", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('canvas'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], AlertService.prototype, "canvas", void 0);
    AlertService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */], __WEBPACK_IMPORTED_MODULE_2__param_service__["a" /* ParamService */], __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["a" /* HttpClient */]])
    ], AlertService);
    return AlertService;
}());

//# sourceMappingURL=alert.service.js.map

/***/ }),

/***/ 235:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ParamPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_api_service__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_mail_mail__ = __webpack_require__(236);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__sms_sms__ = __webpack_require__(237);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__langue_langue__ = __webpack_require__(238);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_param_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__password_password__ = __webpack_require__(239);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__user_user__ = __webpack_require__(240);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var ParamPage = /** @class */ (function () {
    function ParamPage(navCtrl, api, param) {
        this.navCtrl = navCtrl;
        this.api = api;
        this.param = param;
        this.mailPage = __WEBPACK_IMPORTED_MODULE_3__pages_mail_mail__["a" /* MailPage */];
        this.languePage = __WEBPACK_IMPORTED_MODULE_5__langue_langue__["a" /* LanguePage */];
        this.smsPage = __WEBPACK_IMPORTED_MODULE_4__sms_sms__["a" /* SMSPage */];
        this.passwordPage = __WEBPACK_IMPORTED_MODULE_7__password_password__["a" /* PasswordPage */];
        this.userPage = __WEBPACK_IMPORTED_MODULE_8__user_user__["a" /* UserPage */];
    }
    ParamPage.prototype.ngOnInit = function () {
    };
    ParamPage.prototype.goMail = function (ev) {
        ev.preventDefault();
        this.navCtrl.push(this.mailPage);
    };
    ParamPage.prototype.goSms = function (ev) {
        ev.preventDefault();
        this.navCtrl.push(this.smsPage);
    };
    ParamPage.prototype.goLangue = function (ev) {
        ev.preventDefault();
        this.navCtrl.push(this.languePage);
    };
    ParamPage.prototype.goPassword = function (ev) {
        ev.preventDefault();
        this.navCtrl.push(this.passwordPage);
    };
    ParamPage.prototype.goUser = function (ev) {
        ev.preventDefault();
        this.navCtrl.push(this.userPage);
    };
    ParamPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-param',template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\guidedwalk\src\pages\param\param.html"*/'<!-- param page html template -->\n\n<ion-header no-border>\n\n  <headpage pageName="param"></headpage>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n  <ion-card text-center class="big_card">\n\n    <ion-grid class="heightstyle">\n\n      <ion-list>\n\n        <button ion-item class="btnscss" (mouseup)="goMail($event)" >\n\n          <ion-icon class="iconscss" name="at" item-start></ion-icon>{{param.datatext.mails}}<ion-icon class="iconscss"\n\n            name="at" item-end></ion-icon>\n\n        </button>\n\n        <!-- <button ion-item class="btnscss" (click)="goSms()"><ion-icon  class="iconscss" name="chatboxes" item-start></ion-icon>{{param.datatext.sms}}<ion-icon  class="iconscss" name="chatboxes" item-end></ion-icon></button> -->\n\n        <button ion-item class="btnscss" (mouseup)="goLangue($event)">\n\n          <ion-icon class="iconscss" name="globe" item-start></ion-icon>{{param.datatext.langage}}<ion-icon\n\n            class="iconscss" name="globe" item-end></ion-icon>\n\n        </button>\n\n        <button ion-item class="btnscss" (mouseup)="goPassword($event)">\n\n          <ion-icon class="iconscss" name="key" item-start></ion-icon>{{param.datatext.editpswd1}}<ion-icon\n\n            class="iconscss" name="key" item-end></ion-icon>\n\n        </button>\n\n        <button ion-item class="btnscss" (mouseup)="goUser($event)">\n\n          <ion-icon class="iconscss" name="person" item-start></ion-icon>{{param.datatext.users}}<ion-icon\n\n            class="iconscss" name="person" item-end></ion-icon>\n\n        </button>\n\n      </ion-list>\n\n    </ion-grid>\n\n  </ion-card>\n\n</ion-content>'/*ion-inline-end:"C:\Users\laele\Documents\KomApp\guidedwalk\src\pages\param\param.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__services_api_service__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_6__services_param_service__["a" /* ParamService */]])
    ], ParamPage);
    return ParamPage;
}());

//# sourceMappingURL=param.js.map

/***/ }),

/***/ 236:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MailPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_param_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_popup_service__ = __webpack_require__(24);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var MailPage = /** @class */ (function () {
    function MailPage(param, popup, toastCtrl) {
        this.param = param;
        this.popup = popup;
        this.toastCtrl = toastCtrl;
        this.inputmail = "";
    }
    MailPage.prototype.ngOnInit = function () {
        this.popup.onSomethingHappened2(this.addMail.bind(this));
    };
    MailPage.prototype.errorToast = function (m) {
        var toast = this.toastCtrl.create({
            message: m,
            duration: 3000,
            position: 'middle',
            cssClass: "toast"
        });
        toast.present();
    };
    MailPage.prototype.okToast = function (m) {
        var toast = this.toastCtrl.create({
            message: m,
            duration: 3000,
            position: 'middle',
            cssClass: "toastok"
        });
        toast.present();
    };
    MailPage.prototype.removeMail = function (ev, m) {
        ev.preventDefault();
        var index = this.param.maillist.indexOf(m);
        if (index !== -1) {
            this.param.maillist.splice(index, 1);
            this.param.deleteMail(m);
            this.okToast(this.param.datatext.mailRemove);
        }
    };
    MailPage.prototype.is_a_mail = function (m) {
        var mailformat = "[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$";
        return m.match(mailformat);
    };
    MailPage.prototype.addMail = function (ev) {
        var _this = this;
        ev.preventDefault();
        if (this.is_a_mail(this.inputmail)) {
            var index = this.param.maillist.indexOf(this.inputmail);
            if (index !== -1) {
                this.errorToast(this.param.datatext.mailexist);
            }
            else {
                this.param.maillist.push(this.inputmail);
                this.param.addMail(this.inputmail);
                this.okToast(this.param.datatext.mailadd);
                setTimeout(function () {
                    _this.content1.scrollToBottom();
                }, 300);
            }
        }
        else {
            this.errorToast(this.param.datatext.mailincorrect);
        }
    };
    MailPage.prototype.update_send_pic = function () {
        if (this.param.robot.send_pic == 1) {
            this.param.robot.send_pic = 0;
        }
        else {
            this.param.robot.send_pic = 1;
        }
        this.param.updateRobot();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("content1"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* Content */])
    ], MailPage.prototype, "content1", void 0);
    MailPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-mail',template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\guidedwalk\src\pages\mail\mail.html"*/'<!-- mail page html template -->\n\n<ion-header no-border>\n\n  <headpage pageName="mail"></headpage>\n\n</ion-header>\n\n\n\n\n\n<ion-content class="big-ion-content" padding >\n\n  <ion-card text-center class="big_card" >\n\n    <ion-grid class="heightstyle">\n\n      <ion-item >\n\n        <ion-label class="labelcss" stacked>{{param.datatext.receivepic}}</ion-label>\n\n        <ion-toggle\n\n              [checked]="this.param.robot.send_pic ==1 "\n\n              (ionChange)="update_send_pic()"\n\n              item-end\n\n            ></ion-toggle>\n\n            <ion-icon\n\n              class="iconscss"\n\n              name="camera"\n\n              item-start\n\n              color="primary"\n\n            ></ion-icon>\n\n      </ion-item>\n\n        <ion-item >\n\n          <ion-label class="txt" stacked>{{param.datatext.newmail}}</ion-label>\n\n          <ion-input inputmode="email" [(ngModel)]="inputmail" placeholder="kompai@gmail.com"></ion-input>\n\n          <button *ngIf="is_a_mail(inputmail)" class="btn-add" (mouseup)="popup.displayrgpd($event)" item-end>\n\n            <ion-icon  color="light" name="add"></ion-icon>\n\n          </button>\n\n          <button *ngIf="!(is_a_mail(inputmail))" class="mailnotvalid" (mouseup)="addMail($event)" item-end>\n\n            <ion-icon  color="light" name="add"></ion-icon>\n\n          </button>\n\n        </ion-item>\n\n       \n\n        <ion-content\n\n        #content1\n\n        style="height: 80%; width: 100%; background-color: transparent;"\n\n      >\n\n        \n\n          <ion-item *ngFor="let m of this.param.maillist">\n\n            <ion-icon class="iconscss" name="at" item-start color="primary"></ion-icon>{{m}}\n\n            <button class="btn-trash" item-end (mouseup)="removeMail($event,m)">\n\n              <ion-icon class="btn-trash" name="trash"></ion-icon>\n\n            </button>\n\n          </ion-item>\n\n        \n\n        </ion-content>\n\n    \n\n    </ion-grid> \n\n\n\n</ion-card>\n\n \n\n\n\n</ion-content>\n\n\n\n'/*ion-inline-end:"C:\Users\laele\Documents\KomApp\guidedwalk\src\pages\mail\mail.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_param_service__["a" /* ParamService */], __WEBPACK_IMPORTED_MODULE_3__services_popup_service__["a" /* PopupService */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* ToastController */]])
    ], MailPage);
    return MailPage;
}());

//# sourceMappingURL=mail.js.map

/***/ }),

/***/ 237:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SMSPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_param_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SMSPage = /** @class */ (function () {
    function SMSPage(param, toastCtrl) {
        this.param = param;
        this.toastCtrl = toastCtrl;
    }
    SMSPage.prototype.ngOnInit = function () {
    };
    SMSPage.prototype.errorToast = function (m) {
        var toast = this.toastCtrl.create({
            message: m,
            duration: 3000,
            position: 'middle',
            cssClass: "toast"
        });
        toast.present();
    };
    SMSPage.prototype.okToast = function (m) {
        var toast = this.toastCtrl.create({
            message: m,
            duration: 3000,
            position: 'middle',
            cssClass: "toastok"
        });
        toast.present();
    };
    SMSPage.prototype.removeNum = function (ev, m) {
        ev.preventDefault();
        var index = this.param.phonenumberlist.indexOf(m);
        if (index !== -1) {
            this.param.phonenumberlist.splice(index, 1);
            this.param.deletePhone(m);
            this.okToast(this.param.datatext.mailRemove);
        }
    };
    SMSPage.prototype.is_a_num = function (m) {
        var numformat = "^[0-9]{10}$";
        return m.match(numformat);
    };
    SMSPage.prototype.changeNum = function (m) {
        return "+33" + m.substring(1);
    };
    SMSPage.prototype.displayNum = function (m) {
        return "0" + m.substring(3);
    };
    SMSPage.prototype.addNum = function (ev, m) {
        ev.preventDefault();
        if (this.is_a_num(m)) {
            var index = this.param.phonenumberlist.indexOf(this.changeNum(m));
            if (index !== -1) {
                this.errorToast(this.param.datatext.numexist);
            }
            else if (this.param.phonenumberlist.length > 5) {
                this.errorToast(this.param.datatext.deletenum);
            }
            else {
                this.param.phonenumberlist.push(this.changeNum(m));
                this.param.addPhone(this.changeNum(m));
                this.okToast(this.param.datatext.numadd);
            }
        }
        else {
            this.errorToast(this.param.datatext.numincorrect);
        }
    };
    SMSPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-sms',template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\guidedwalk\src\pages\sms\sms.html"*/'<!-- mail page html template -->\n\n<ion-header no-border>\n\n  <headpage pageName="sms"></headpage>\n\n</ion-header>\n\n\n\n\n\n<ion-content padding >\n\n  <ion-card text-center class="big_card" >\n\n    <ion-grid class="heightstyle">\n\n     \n\n        <ion-item >\n\n          <ion-label class="txt" stacked>{{param.datatext.newnum}}</ion-label>\n\n          <ion-input #tel  inputmode="tel" placeholder="0630854706"></ion-input>\n\n          <button *ngIf="is_a_num(tel.value)" class="btn-add" (mouseup)="addNum($event,tel.value)" item-end>\n\n            <ion-icon  color="light" name="add"></ion-icon>\n\n          </button>\n\n          <button *ngIf="!(is_a_num(tel.value))" class="mailnotvalid" (mouseup)="addNum($event,tel.value)" item-end>\n\n            <ion-icon  color="light" name="add"></ion-icon>\n\n          </button>\n\n        </ion-item>\n\n       \n\n      \n\n        <ion-list >\n\n          <ion-item *ngFor="let m of this.param.phonenumberlist">\n\n            <ion-icon class="iconscss" name="chatboxes" item-start color="primary"></ion-icon>{{displayNum(m)}}\n\n            <button class="btn-trash" item-end (mouseup)="removeNum($event,m)">\n\n              <ion-icon class="btn-trash" name="trash"></ion-icon>\n\n            </button>\n\n          </ion-item>\n\n        </ion-list>\n\n    \n\n    </ion-grid> \n\n\n\n</ion-card>\n\n \n\n\n\n</ion-content>\n\n\n\n\n\n'/*ion-inline-end:"C:\Users\laele\Documents\KomApp\guidedwalk\src\pages\sms\sms.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_param_service__["a" /* ParamService */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* ToastController */]])
    ], SMSPage);
    return SMSPage;
}());

//# sourceMappingURL=sms.js.map

/***/ }),

/***/ 238:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LanguePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_param_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LanguePage = /** @class */ (function () {
    function LanguePage(param, loadingCtrl) {
        this.param = param;
        this.loadingCtrl = loadingCtrl;
        this.monitor = this.param.langage;
    }
    LanguePage.prototype.ngOnInit = function () {
    };
    LanguePage.prototype.monitorHandler = function () {
        this.loading = this.loadingCtrl.create({});
        this.loading.present(); //loading animation display
        this.param.updateRobot();
        setTimeout(function () {
            document.location.reload();
        }, 1000);
    };
    LanguePage.prototype.onSliderRelease = function (ev, id) {
        ev.preventDefault();
        id.open();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("select1"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* Select */])
    ], LanguePage.prototype, "select1", void 0);
    LanguePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-langue',template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\guidedwalk\src\pages\langue\langue.html"*/'<!-- langue page html template -->\n\n<ion-header no-border>\n\n  <headpage pageName="langue"></headpage>\n\n</ion-header>\n\n<ion-content padding >\n\n  <ion-grid class="main_ion_grid">\n\n    <div style="background-color: rgb(255, 255, 255); border-radius: 15px; height: 40%; width: 60%; ">\n\n      <ion-row style="height: 25%; color: rgb(0, 0, 0);justify-content: center!important; align-items: center!important; font-size: 4em;">\n\n        {{param.datatext.langage}}\n\n      </ion-row>\n\n      <ion-row style="height: 75%;vertical-align: middle;\n\n      justify-content: center;\n\n      display: flex!important;\n\n      align-items: center!important">\n\n       \n\n        <ion-select #select1 [(ngModel)]="param.robot.langage" (ionChange)="monitorHandler()" style="width: 70% !important; font-size: x-large;background-color:rgba(26, 156, 195, 0.199);" interface="popover" (mouseup)="onSliderRelease($event,select1)">\n\n          <!-- <ion-option value="de-DE">Deutsch</ion-option> -->\n\n          <ion-option value="en-GB">English</ion-option>\n\n          <ion-option value="el-GR">Ελληνικά</ion-option>\n\n          <ion-option value="fr-FR">Français</ion-option>\n\n          <ion-option value="es-ES">Spanish</ion-option>\n\n        </ion-select>\n\n     \n\n      </ion-row>\n\n    </div>\n\n   </ion-grid>\n\n\n\n</ion-content>\n\n\n\n'/*ion-inline-end:"C:\Users\laele\Documents\KomApp\guidedwalk\src\pages\langue\langue.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_param_service__["a" /* ParamService */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* LoadingController */]])
    ], LanguePage);
    return LanguePage;
}());

//# sourceMappingURL=langue.js.map

/***/ }),

/***/ 239:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PasswordPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_param_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_popup_service__ = __webpack_require__(24);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var PasswordPage = /** @class */ (function () {
    function PasswordPage(navCtrl, popup, param) {
        this.navCtrl = navCtrl;
        this.popup = popup;
        this.param = param;
        this.newpassword = "";
        this.warn = false;
    }
    PasswordPage.prototype.ngOnInit = function () {
    };
    PasswordPage.prototype.editpassword = function (ev) {
        ev.preventDefault();
        if (this.password === atob(this.param.robot.password)) {
            if (this.newpassword.length >= 2) {
                this.popup.showToast(this.param.datatext.pswdsaved, 3000, "middle");
                this.param.robot.password = btoa(this.newpassword);
                this.param.updateRobot();
                this.password = "";
                this.newpassword = "";
                this.navCtrl.pop();
            }
        }
        else {
            this.warn = true;
        }
    };
    PasswordPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-password',template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\guidedwalk\src\pages\password\password.html"*/'<!-- password page html template -->\n\n<ion-header no-border>\n\n    <headpage pageName="password"></headpage>\n\n  </ion-header>\n\n  \n\n  \n\n  <ion-content padding >\n\n    <ion-grid class="main_ion_grid">\n\n        <div style="background-color: rgb(255, 255, 255); border-radius: 15px; height: 60%; width: 60%; ">\n\n    \n\n        <ion-row style="height: 25%;justify-content: center!important; align-items: center!important;">\n\n          <ion-item style="width: 90%;">\n\n          <ion-label style="font-size: x-large; min-width: 30% !important;\n\n          max-width: 30% !important;">{{param.datatext.currentpswd}}</ion-label>\n\n          <ion-input minlength=2 maxlength=15 [(ngModel)]="password" style="font-size: xx-large; color: rgb(26, 156, 195);"  required></ion-input>\n\n       </ion-item>\n\n          \n\n        </ion-row>\n\n        \n\n    \n\n        <ion-row style="height: 25%;justify-content: center!important; align-items: center!important;">\n\n          <ion-item style="width: 90%;">\n\n          <ion-label style="font-size: x-large; min-width: 30% !important;\n\n          max-width: 30% !important;">{{param.datatext.newpswd}}</ion-label>\n\n          <ion-input [(ngModel)]="newpassword" minlength=2 maxlength=15 style="font-size: xx-large; color: rgb(26, 156, 195);" required></ion-input>\n\n          </ion-item>\n\n          \n\n        </ion-row>\n\n       <ion-row style="height: 20%; justify-content: center!important; ">\n\n          <p *ngIf="warn" style="color: red; font-size: large;">** {{param.datatext.wrongpass}} **</p>\n\n        </ion-row>\n\n        <ion-row style="height: 25%;justify-content: center!important; align-items: center!important;">\n\n          <button [disabled]="2>this.newpassword.length" ion-button solid large class="btn_login" (mouseup)="editpassword($event)"> \n\n            <!-- the text of the button depends on the page you are on -->\n\n            <font class="font_menu_size" >{{param.datatext.save}}</font>\n\n          </button>\n\n     \n\n        </ion-row>\n\n        </div>\n\n       </ion-grid>\n\n  \n\n  </ion-content>\n\n  '/*ion-inline-end:"C:\Users\laele\Documents\KomApp\guidedwalk\src\pages\password\password.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__services_popup_service__["a" /* PopupService */], __WEBPACK_IMPORTED_MODULE_1__services_param_service__["a" /* ParamService */]])
    ], PasswordPage);
    return PasswordPage;
}());

//# sourceMappingURL=password.js.map

/***/ }),

/***/ 24:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PopupService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__alert_service__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__api_service__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__param_service__ = __webpack_require__(10);
// to send mail and sms alert
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};






var PopupService = /** @class */ (function () {
    function PopupService(app, alert, param, api, alertCtrl, toastCtrl) {
        this.app = app;
        this.alert = alert;
        this.param = param;
        this.api = api;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
    }
    PopupService.prototype.onSomethingHappened1 = function (fn) {
        this.accessparam = fn;
    };
    PopupService.prototype.onSomethingHappened2 = function (fn) {
        this.addMail = fn;
    };
    PopupService.prototype.onSomethingHappened3 = function (fn) {
        this.accesswalklog = fn;
    };
    PopupService.prototype.ngOnInit = function () { };
    PopupService.prototype.startFailedAlert = function () {
        this.alert_blocked = this.alertCtrl.create({
            title: this.param.datatext.statusRedPresent_title,
            message: this.param.datatext.cantstart,
            cssClass: "alertstyle",
            enableBackdropDismiss: false,
            buttons: [
                {
                    text: this.param.datatext.btn_ok,
                    handler: function () {
                        console.log("Oui clicked");
                        window.close();
                    },
                },
            ],
        });
        this.alert_blocked.present();
    };
    PopupService.prototype.leaveDockingAlert = function () {
        // pop up if the robot is in the docking when you start the round
        var alert = this.alertCtrl.create({
            title: this.param.datatext.leaveDockingAlert_title,
            message: this.param.datatext.leaveDockingAlert_message,
            cssClass: "alertstyle",
            buttons: [
                {
                    text: this.param.datatext.btn_ok,
                    handler: function () {
                        console.log("OK clicked");
                    },
                },
            ],
        });
        alert.present();
    };
    PopupService.prototype.errorNavAlert = function () {
        this.alert_blocked = this.alertCtrl.create({
            title: this.param.datatext.errorNavAlert_title,
            message: this.param.datatext.errorNavAlert_message,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            buttons: [
                {
                    text: this.param.datatext.btn_ok,
                    handler: function () {
                        //console.log('Oui clicked');
                    },
                },
            ],
        });
        this.alert_blocked.present();
    };
    PopupService.prototype.blockedAlert = function () {
        this.alert_blocked = this.alertCtrl.create({
            title: this.param.datatext.blockedAlert_title,
            message: this.param.datatext.blockedAlert_message,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            buttons: [
                {
                    text: this.param.datatext.btn_ok,
                    handler: function () {
                        console.log("Oui clicked");
                    },
                },
            ],
        });
        this.alert_blocked.present();
    };
    PopupService.prototype.quitConfirm = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: this.param.datatext.quitConfirm_title,
            cssClass: "alertstyle",
            enableBackdropDismiss: false,
            buttons: [
                {
                    text: this.param.datatext.btn_no,
                    role: "cancel",
                    handler: function () {
                        //console.log('Non clicked');
                    },
                },
                {
                    text: this.param.datatext.btn_yes,
                    handler: function () {
                        //console.log('Oui clicked');
                        _this.api.close_app = true;
                        _this.alert.appClosed(_this.api.mailAddInformationBasic());
                        //stop the round and quit the app
                        _this.api.abortNavHttp();
                        setTimeout(function () {
                            window.close();
                        }, 1000);
                    },
                },
            ],
        });
        alert.present();
    };
    PopupService.prototype.lowBattery = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: this.param.datatext.lowBattery_title,
            message: this.param.datatext.lowBattery_message,
            cssClass: "alertstyle",
            enableBackdropDismiss: false,
            buttons: [
                {
                    text: this.param.datatext.btn_no,
                    role: "cancel",
                    handler: function () {
                        //console.log('Non clicked');
                        _this.api.towardDocking = false;
                    },
                },
                {
                    text: this.param.datatext.btn_yes,
                    handler: function () {
                        console.log("Oui clicked");
                        _this.api.towardDocking = true;
                        //console.log(this.api.towardDocking=true);
                        _this.api.reachHttp("docking");
                        _this.alert.askCharge(_this.api.mailAddInformationBasic());
                    },
                },
            ],
        });
        alert.present();
    };
    PopupService.prototype.errorBlocked = function () {
        var alert = this.alertCtrl.create({
            title: this.param.datatext.errorBlocked_title,
            message: this.param.datatext.errorBlocked_message,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            buttons: [
                {
                    text: this.param.datatext.btn_ok,
                    handler: function () {
                        //console.log('OK clicked');
                    },
                },
            ],
        });
        alert.present();
    };
    PopupService.prototype.lostAlert = function () {
        this.alert_blocked = this.alertCtrl.create({
            title: this.param.datatext.lostAlert_title,
            message: this.param.datatext.lostAlert_message,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            buttons: [
                {
                    text: this.param.datatext.btn_ok,
                    handler: function () {
                        //console.log('Oui clicked');
                    },
                },
            ],
        });
        this.alert_blocked.present();
    };
    PopupService.prototype.robotmuststayondocking = function () {
        // pop up if the robot is in the docking when you start the round
        this.alert_blocked = this.alertCtrl.create({
            title: this.param.datatext.robotmuststayondocking_title,
            message: this.param.datatext.robotmuststayondocking_message,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            buttons: [
                {
                    text: this.param.datatext.btn_ok,
                    handler: function () {
                        //console.log('Oui clicked');
                    },
                },
            ],
        });
        this.alert_blocked.present();
    };
    PopupService.prototype.goDockingConfirm = function () {
        var _this = this;
        // pop up to ask if the robot must go to the docking
        var alert = this.alertCtrl.create({
            title: this.param.datatext.goDockingConfirm_title +
                this.api.battery_status.remaining +
                "%",
            message: this.param.datatext.goDockingConfirm_message,
            cssClass: "alertstyle",
            enableBackdropDismiss: false,
            buttons: [
                {
                    text: this.param.datatext.btn_no,
                    role: "cancel",
                    handler: function () {
                        //console.log('Non clicked');
                        _this.api.walkercmdstartdetect();
                        _this.api.towardDocking = false;
                    },
                },
                {
                    text: this.param.datatext.btn_yes,
                    handler: function () {
                        //console.log('Oui clicked');
                        _this.api.towardDocking = true;
                        _this.api.reachHttp("docking");
                        _this.alert.askCharge(_this.api.mailAddInformationBasic());
                    },
                },
            ],
        });
        alert.present();
    };
    PopupService.prototype.statusRedPresent = function () {
        var alert = this.alertCtrl.create({
            title: this.param.datatext.statusRedPresent_title,
            message: this.param.datatext.statusRedPresent_message,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            buttons: [
                {
                    text: this.param.datatext.btn_ok,
                    handler: function () {
                        console.log("OK clicked");
                    },
                },
            ],
        });
        alert.present();
    };
    PopupService.prototype.statusGreenPresent = function () {
        var alert = this.alertCtrl.create({
            title: this.param.datatext.statusGreenPresent_title,
            message: this.param.datatext.statusGreenPresent_message,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            buttons: [
                {
                    text: this.param.datatext.btn_ok,
                    handler: function () {
                        console.log("OK clicked");
                    },
                },
            ],
        });
        alert.present();
    };
    PopupService.prototype.askHelp = function () {
        // pop up displayed when someone push the sos btn
        var alert = this.alertCtrl.create({
            title: this.param.datatext.askHelp_title,
            message: this.param.datatext.askHelp_message,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            buttons: [
                {
                    text: this.param.datatext.btn_ok,
                    handler: function () {
                        console.log("OK clicked");
                    },
                },
            ],
        });
        alert.present();
    };
    PopupService.prototype.leaveDockingConfirm = function () {
        var _this = this;
        // pop up to ask if the robot must leave the docking
        var alert = this.alertCtrl.create({
            title: this.param.datatext.leaveDockingConfirm_title +
                this.api.battery_status.remaining +
                "%",
            message: this.param.datatext.leaveDockingConfirm.mes,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            buttons: [
                {
                    text: this.param.datatext.btn_no,
                    role: "cancel",
                    handler: function () {
                        console.log("Non clicked");
                    },
                },
                {
                    text: this.param.datatext.btn_yes,
                    handler: function () {
                        console.log("Oui clicked");
                        _this.api.disconnectHttp();
                        _this.alert.leaveDocking(_this.api.mailAddInformationBasic());
                        setTimeout(function () {
                            _this.api.walkercmdstartdetect();
                        }, 8000);
                    },
                },
            ],
        });
        alert.present();
    };
    PopupService.prototype.askpswd = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: this.param.datatext.password,
            message: this.param.datatext.enterPassword,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            inputs: [
                {
                    name: "password",
                    placeholder: this.param.datatext.password,
                    type: "password",
                },
            ],
            buttons: [
                {
                    text: this.param.datatext.btn_cancel,
                    role: "cancel",
                    handler: function (data) {
                        console.log("Cancel clicked");
                    },
                },
                {
                    text: this.param.datatext.btn_save,
                    role: "backdrop",
                    handler: function (data) {
                        if (data.password === atob(_this.param.robot.password)) {
                            _this.accessparam();
                        }
                        else {
                            _this.wrongPassword();
                        }
                    },
                },
            ],
        });
        alert.present();
    };
    PopupService.prototype.wrongPassword = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: this.param.datatext.password,
            message: this.param.datatext.wrongPassword,
            cssClass: "alertstyle_wrongpass",
            enableBackdropDismiss: true,
            inputs: [
                {
                    name: "password",
                    placeholder: this.param.datatext.password,
                    type: "password",
                },
            ],
            buttons: [
                {
                    text: this.param.datatext.btn_cancel,
                    role: "cancel",
                    handler: function (data) {
                        console.log("Cancel clicked");
                    },
                },
                {
                    text: this.param.datatext.btn_save,
                    role: "backdrop",
                    handler: function (data) {
                        if (data.password === atob(_this.param.robot.password)) {
                            _this.accessparam();
                        }
                        else {
                            _this.wrongPassword();
                        }
                    },
                },
            ],
        });
        alert.present();
    };
    PopupService.prototype.askpswdWalk = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: this.param.datatext.password,
            message: this.param.datatext.enterPassword,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            inputs: [
                {
                    name: "password",
                    placeholder: this.param.datatext.password,
                    type: "password",
                },
            ],
            buttons: [
                {
                    text: this.param.datatext.btn_cancel,
                    role: "cancel",
                    handler: function (data) {
                        console.log("Cancel clicked");
                    },
                },
                {
                    text: this.param.datatext.btn_save,
                    role: "backdrop",
                    handler: function (data) {
                        if (data.password === atob(_this.param.robot.password)) {
                            _this.accesswalklog();
                        }
                        else {
                            _this.wrongPasswordWalk();
                        }
                    },
                },
            ],
        });
        alert.present();
    };
    PopupService.prototype.wrongPasswordWalk = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: this.param.datatext.password,
            message: this.param.datatext.wrongPassword,
            cssClass: "alertstyle_wrongpass",
            enableBackdropDismiss: true,
            inputs: [
                {
                    name: "password",
                    placeholder: this.param.datatext.password,
                    type: "password",
                },
            ],
            buttons: [
                {
                    text: this.param.datatext.btn_cancel,
                    role: "cancel",
                    handler: function (data) {
                        console.log("Cancel clicked");
                    },
                },
                {
                    text: this.param.datatext.btn_save,
                    role: "backdrop",
                    handler: function (data) {
                        if (data.password === atob(_this.param.robot.password)) {
                            _this.accesswalklog();
                        }
                        else {
                            _this.wrongPasswordWalk();
                        }
                    },
                },
            ],
        });
        alert.present();
    };
    PopupService.prototype.displayrgpd = function (ev) {
        var _this = this;
        ev.preventDefault();
        var alert = this.alertCtrl.create({
            title: "RGPD",
            message: this.param.datatext.rgpd_txt,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            buttons: [
                {
                    text: this.param.datatext.btn_cancel,
                    role: "cancel",
                    handler: function (data) {
                        console.log("Cancel clicked");
                    },
                },
                {
                    text: this.param.datatext.btn_save,
                    role: "backdrop",
                    handler: function (data) {
                        _this.addMail(ev);
                    }
                },
            ],
        });
        alert.present();
    };
    PopupService.prototype.relocAlert = function () {
        this.alert_blocked = this.alertCtrl.create({
            title: this.param.datatext.relocAlert_title,
            message: this.param.datatext.relocAlert_message,
            cssClass: "alertstyle",
            enableBackdropDismiss: false,
            buttons: [
                {
                    text: this.param.datatext.btn_ok,
                    handler: function () {
                        console.log("Oui clicked");
                        window.close();
                    },
                },
            ],
        });
        this.alert_blocked.present();
    };
    PopupService.prototype.showToast = function (msg, duration, position) {
        return __awaiter(this, void 0, void 0, function () {
            var toast;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastCtrl.create({
                            message: msg,
                            duration: duration,
                            position: position,
                            cssClass: "toastok"
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    PopupService.prototype.showToastRed = function (msg, duration, position) {
        return __awaiter(this, void 0, void 0, function () {
            var toast;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastCtrl.create({
                            message: msg,
                            duration: duration,
                            position: position,
                            cssClass: "toast"
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    PopupService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */],
            __WEBPACK_IMPORTED_MODULE_2__alert_service__["a" /* AlertService */],
            __WEBPACK_IMPORTED_MODULE_4__param_service__["a" /* ParamService */],
            __WEBPACK_IMPORTED_MODULE_3__api_service__["a" /* ApiService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ToastController */]])
    ], PopupService);
    return PopupService;
}());

//# sourceMappingURL=popup.service.js.map

/***/ }),

/***/ 240:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_param_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_popup_service__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_api_service__ = __webpack_require__(18);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var UserPage = /** @class */ (function () {
    function UserPage(param, api, popup, toastCtrl) {
        this.param = param;
        this.api = api;
        this.popup = popup;
        this.toastCtrl = toastCtrl;
    }
    UserPage.prototype.ngOnInit = function () {
    };
    UserPage.prototype.errorToast = function (m) {
        var toast = this.toastCtrl.create({
            message: m,
            duration: 3000,
            position: 'middle',
            cssClass: "toast"
        });
        toast.present();
    };
    UserPage.prototype.okToast = function (m) {
        var toast = this.toastCtrl.create({
            message: m,
            duration: 3000,
            position: 'middle',
            cssClass: "toastok"
        });
        toast.present();
    };
    UserPage.prototype.removeUser = function (ev, user) {
        var _this = this;
        ev.preventDefault();
        var index = this.param.users.indexOf(user);
        if (index !== -1) {
            this.param.users.splice(index, 1);
            this.param.deleteUser(user);
            this.okToast(this.param.datatext.mailRemove);
            this.remove_user_from_jetson(user);
            setTimeout(function () {
                _this.param.getWalkUser();
            }, 1000);
        }
    };
    UserPage.prototype.remove_user_from_jetson = function (user) {
        if (this.api.recoface) {
            console.log("delete user jetson");
            this.api.deleteUserJetson(user);
        }
    };
    UserPage.prototype.cleantext = function (text) {
        text = text.replace(new RegExp("[<>&$§µ£#*_|`¤~(){}°²%/]", "g"), '');
        text = text.replace(new RegExp("[\"]", "g"), '');
        text = text.replace(new RegExp(/\\/, "g"), '');
        text = text.replace(new RegExp(/[\[\]]/, "g"), '');
        return text;
    };
    UserPage.prototype.addUser = function (ev) {
        var _this = this;
        ev.preventDefault();
        this.inputuser = this.cleantext(this.inputuser);
        var index = this.param.users.indexOf(this.inputuser);
        if (index !== -1) {
            this.errorToast(this.param.datatext.userexist);
        }
        else {
            this.param.users.push(this.inputuser);
            this.param.addUser(this.inputuser, "", "");
            this.okToast(this.param.datatext.useradd);
            setTimeout(function () {
                _this.content1.scrollToBottom();
            }, 300);
            setTimeout(function () {
                _this.param.getWalkUser();
            }, 1000);
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("content1"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* Content */])
    ], UserPage.prototype, "content1", void 0);
    UserPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-user',template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\guidedwalk\src\pages\user\user.html"*/'<!-- mail page html template -->\n\n<ion-header no-border>\n\n    <headpage pageName="user"></headpage>\n\n  </ion-header>\n\n  \n\n  \n\n  <ion-content class="big-ion-content" padding >\n\n    <ion-card text-center class="big_card" >\n\n      <ion-grid class="heightstyle">\n\n          <ion-item *ngIf="!api.recoface">\n\n            <ion-label class="txt" stacked>{{param.datatext.newuser}}</ion-label>\n\n            <ion-input inputmode="text" [(ngModel)]="inputuser" placeholder="{{param.datatext.user}}"></ion-input>\n\n            <button [disabled]="!inputuser" class="btn-add" (mouseup)="addUser($event)" item-end>\n\n              <ion-icon  color="light" name="person-add"></ion-icon>\n\n            </button>\n\n          </ion-item>\n\n         \n\n          <ion-content\n\n          #content1\n\n          style="height: 80%; width: 100%; background-color: transparent;"\n\n        >\n\n          \n\n            <ion-item *ngFor="let user of this.param.users">\n\n              <ion-icon class="iconscss" name="contact" item-start color="primary"></ion-icon>{{user}}\n\n              <button class="btn-trash" item-end (mouseup)="removeUser($event,user)">\n\n                <ion-icon class="btn-trash" name="trash"></ion-icon>\n\n              </button>\n\n            </ion-item>\n\n          \n\n          </ion-content>\n\n      \n\n      </ion-grid> \n\n  \n\n  </ion-card>\n\n   \n\n  \n\n  </ion-content>'/*ion-inline-end:"C:\Users\laele\Documents\KomApp\guidedwalk\src\pages\user\user.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_param_service__["a" /* ParamService */], __WEBPACK_IMPORTED_MODULE_4__services_api_service__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_3__services_popup_service__["a" /* PopupService */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* ToastController */]])
    ], UserPage);
    return UserPage;
}());

//# sourceMappingURL=user.js.map

/***/ }),

/***/ 241:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WalkLogsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_param_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var WalkLogsPage = /** @class */ (function () {
    function WalkLogsPage(param, sanitizer) {
        this.param = param;
        this.sanitizer = sanitizer;
        this.selectOptionuser = {
            title: this.param.datatext.chooseuser,
        };
    }
    WalkLogsPage.prototype.ngOnInit = function () {
        var _this = this;
        this.param.getWalkLogs().subscribe(function (data) {
            _this.param.walklogs = data;
            _this.walklogstab = data;
            //console.log(data);
        }, function (err) {
            console.log(err);
        });
        // afficher/mettre à jour la liste
    };
    WalkLogsPage.prototype.getWalkLogsById = function (e) {
        var _this = this;
        console.log(e);
        this.param.getWalkLogsByUser(e).subscribe(function (data) {
            _this.walklogstab = data;
            console.log(data);
        }, function (err) {
            console.log(err);
        });
    };
    WalkLogsPage.prototype.onSliderRelease = function (ev, id) {
        ev.preventDefault();
        id.open();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("select1"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["j" /* Select */])
    ], WalkLogsPage.prototype, "select1", void 0);
    WalkLogsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-walklogs',template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\guidedwalk\src\pages\walklogs\walklogs.html"*/'<!-- home page html template -->\n\n<ion-header no-border>\n\n  <headpage pageName="walklogs"></headpage>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n\n\n  <div style="height:100%;">\n\n    <ion-grid style="height:100%;">\n\n      <ion-row style="height:100%;">\n\n\n\n\n\n        <ion-col>\n\n\n\n          <ion-row class="titrecol">\n\n            <ion-col>\n\n              <ion-select #select1 (ionChange)="getWalkLogsById($event)" [selectOptions]="selectOptionuser" placeholder="{{this.param.datatext.user}}" (mouseup)="onSliderRelease($event,select1)">\n\n                <ion-option value={{param.datatext.anonymous}}>{{param.datatext.anonymous}}</ion-option>\n\n                <ion-option *ngFor="let row of param.walkuser" value="{{row.username}}">{{row.username}}\n\n                </ion-option>\n\n              </ion-select>\n\n            </ion-col>\n\n            <ion-col>{{this.param.datatext.date}}</ion-col>\n\n            <ion-col>{{this.param.datatext.round}}</ion-col>\n\n            <ion-col>{{this.param.datatext.meters}}</ion-col>\n\n            <ion-col>{{this.param.datatext.duration}}</ion-col>\n\n            <ion-col>{{this.param.datatext.pause}}</ion-col>\n\n            <ion-col>{{this.param.datatext.speed}}</ion-col>\n\n          </ion-row>\n\n          <ion-scroll style="height:90%;width: 100%;" scrollY="true">\n\n\n\n            <ion-row class="sizecoltab" *ngFor="let row of walklogstab">\n\n              <ion-col>{{row.walkuser_id}}</ion-col>\n\n              <ion-col>{{row.walk_date}}</ion-col>\n\n              <ion-col>{{row.walk_round}}</ion-col>\n\n              <ion-col>{{row.walk_meters}} m</ion-col>\n\n              <ion-col>{{row.walk_time}} s</ion-col>\n\n              <ion-col>{{row.walk_timeofpauses}} s</ion-col>\n\n              <ion-col>{{row.walk_speed}} m/s</ion-col>\n\n            </ion-row>\n\n          </ion-scroll>\n\n\n\n\n\n        </ion-col>\n\n      </ion-row>\n\n    </ion-grid>\n\n  </div>\n\n\n\n\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\laele\Documents\KomApp\guidedwalk\src\pages\walklogs\walklogs.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_param_service__["a" /* ParamService */], __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__["c" /* DomSanitizer */]])
    ], WalkLogsPage);
    return WalkLogsPage;
}());

//# sourceMappingURL=walklogs.js.map

/***/ }),

/***/ 242:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(243);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(263);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 263:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(214);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(217);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_components_module__ = __webpack_require__(312);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_component__ = __webpack_require__(218);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_first_first__ = __webpack_require__(219);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_recoface_recoface__ = __webpack_require__(221);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_walk_walk__ = __webpack_require__(220);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__components_headpage_headpage__ = __webpack_require__(222);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_choose_assistance_choose_assistance__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__services_api_service__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__angular_common_http__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_tuto_tuto__ = __webpack_require__(223);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__services_popup_service__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__services_alert_service__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__services_param_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__services_speech_service__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_param_param__ = __webpack_require__(235);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_langue_langue__ = __webpack_require__(238);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_sms_sms__ = __webpack_require__(237);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_mail_mail__ = __webpack_require__(236);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_start_assistance_start_assistance__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__pages_password_password__ = __webpack_require__(239);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__pages_walklogs_walklogs__ = __webpack_require__(241);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__pages_user_user__ = __webpack_require__(240);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



























var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_9__pages_walk_walk__["a" /* WalkPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_first_first__["a" /* FirstPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_recoface_recoface__["a" /* RecofacePage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_choose_assistance_choose_assistance__["a" /* ChooseAssistancePage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_start_assistance_start_assistance__["a" /* StartAssistancePage */],
                __WEBPACK_IMPORTED_MODULE_24__pages_password_password__["a" /* PasswordPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_tuto_tuto__["a" /* TutoPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_langue_langue__["a" /* LanguePage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_mail_mail__["a" /* MailPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_sms_sms__["a" /* SMSPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_param_param__["a" /* ParamPage */],
                __WEBPACK_IMPORTED_MODULE_25__pages_walklogs_walklogs__["a" /* WalkLogsPage */],
                __WEBPACK_IMPORTED_MODULE_26__pages_user_user__["a" /* UserPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_5__components_components_module__["a" /* ComponentsModule */],
                __WEBPACK_IMPORTED_MODULE_13__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */], {}, {
                    links: []
                })
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_9__pages_walk_walk__["a" /* WalkPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_first_first__["a" /* FirstPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_recoface_recoface__["a" /* RecofacePage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_choose_assistance_choose_assistance__["a" /* ChooseAssistancePage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_start_assistance_start_assistance__["a" /* StartAssistancePage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_tuto_tuto__["a" /* TutoPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_param_param__["a" /* ParamPage */],
                __WEBPACK_IMPORTED_MODULE_24__pages_password_password__["a" /* PasswordPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_langue_langue__["a" /* LanguePage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_mail_mail__["a" /* MailPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_sms_sms__["a" /* SMSPage */],
                __WEBPACK_IMPORTED_MODULE_25__pages_walklogs_walklogs__["a" /* WalkLogsPage */],
                __WEBPACK_IMPORTED_MODULE_26__pages_user_user__["a" /* UserPage */],
                __WEBPACK_IMPORTED_MODULE_10__components_headpage_headpage__["a" /* HeadpageComponent */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_12__services_api_service__["a" /* ApiService */],
                __WEBPACK_IMPORTED_MODULE_15__services_popup_service__["a" /* PopupService */],
                __WEBPACK_IMPORTED_MODULE_16__services_alert_service__["a" /* AlertService */],
                __WEBPACK_IMPORTED_MODULE_17__services_param_service__["a" /* ParamService */],
                __WEBPACK_IMPORTED_MODULE_18__services_speech_service__["a" /* SpeechService */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicErrorHandler */] }
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 312:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComponentsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_component__ = __webpack_require__(218);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__headpage_headpage__ = __webpack_require__(222);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var ComponentsModule = /** @class */ (function () {
    function ComponentsModule() {
    }
    ComponentsModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [__WEBPACK_IMPORTED_MODULE_3__headpage_headpage__["a" /* HeadpageComponent */]],
            imports: [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_2__app_app_component__["a" /* MyApp */])],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_2__app_app_component__["a" /* MyApp */]
            ],
            exports: [__WEBPACK_IMPORTED_MODULE_3__headpage_headpage__["a" /* HeadpageComponent */]]
        })
    ], ComponentsModule);
    return ComponentsModule;
}());

//# sourceMappingURL=components.module.js.map

/***/ }),

/***/ 313:
/***/ (function(module, exports) {

module.exports = {"quit":"QUITTER","battery":"BATTERIE","return":"RETOUR","tutorial":"TUTORIEL","param":"PARAMÈTRES","receivesms":"RECEVOIR DES ALERTES SMS","receivemail":"RECEVOIR DES ALERTES PAR MAIL","changelangage":"CHANGER LA LANGUE","godocking":"EN ROUTE VERS LA STATION DE CHARGE","moving":"DÉPLACEMENT EN COURS VERS :","charging_remaining":"ROBOT EN CHARGE - BATTERIE: ","walkInProgress":"MARCHEZ A VOTRE RYTHME SANS POUSSER","notpush":"INSTALLEZ VOUS COMME SUR LA PHOTO ET APPUYEZ SUR «ALLONS-Y»","mails":"Mails","sms":"SMS","langage":"Langue","distance_covered":"Distance parcourue","goto":"Se rendre au :","inProgress":"EN COURS","btn_go":"LANCER","btn_help":"Aide","btn_walk":"Allons-y !","btn_stop":"STOP","btn_charge":"CHARGER","btn_cancel":"Annuler","btn_ok":"OK","btn_yes":"OUI","btn_no":"NON","numexist":"Ce numéro existe déjà !","deletenum":"Veuillez supprimer un numéro d'abord","numadd":"Numéro ajouté","numincorrect":"Numéro invalide","newnum":"Nouveau numéro","mailexist":"Cette adresse existe déjà ! ","userexist":"Cet utilisateur existe déjà !","deletemail":"Veuillez supprimer une adresse d'abord","mailadd":"Adresse mail ajoutée !","useradd":"Utilisateur ajouté !","mailincorrect":"Adresse mail invalide","newmail":"Nouvelle adresse","newuser":"Nouvel utilisateur","error":"Erreur","cantstart":"Impossible de démarrer l'application. Vérifiez que le robot est allumé correctement.","nointernet":"Pas d'internet","btn_save":"Sauver","choose_speed":"Choisir la vitesse","low":"Lent","mean":"Moyen","fast":"Rapide","speed":"Vitesse","receivepic":"Recevoir des photos","editpswd":"CHANGER LE MOT DE PASSE","currentpswd":"Mot de passe actuel :","newpswd":"Nouveau mot de passe :","save":"Sauvegarder","success":"C'est Fait !","pswdsaved":"Mot de passe sauvegardé","langage1":"Changer la langue","editpswd1":"Changer le mot de passe","wrongpass":"Mauvais mot de passe","gobehindrobot":"AVANCEZ VOUS DANS LA ZONE DE MARCHE","choosedestination":"CHOISIR LA DESTINATION ET APPUYER SUR «ALLONS-Y»","chooseround":"CHOISIR LA RONDE ET APPUYER SUR «ALLONS-Y»","gobackward":"VEUILLEZ RECULER, VOUS ETES TROP PRES DU ROBOT","wait":"ATTENDRE LA FIN DU DÉCOMPTE AVANT DE MARCHER","choosepoi":"Choisir un POI","chooseexercice":"Choisir une ronde","chooseuser":"Choisir un utilisateur","choosemode":"Choisir un mode","choose_walkzone":"Modifier la zone de marche","walkzone":"Zone de marche","round":"Ronde","destination":"Destination","map":"Carte","mode":"Mode","user":"Utilisateur","users":"Utilisateurs","btn_pause":"PAUSE","stepback":"Vous êtes trop près du robot.","closer":"Veuillez vous rapprochez, le robot ne vous détecte pas.","finishexo":"Bravo ! Vous avez terminé le parcours.","bravo":"Bravo !","atdestination":"Bravo ! vous êtes arrivé à destination.","gof":"Avancez tout droit","gol":"Tournez à gauche","gor":"Tournez à droite","timeofpauses":"Temps de pauses","timewalk":"Temps de marche","obstacle":"Arrêt sur obstacle","slowdown":"Ralentissez","startpoi":"Placez le robot sur le point de départ","smsSOS":"Quelqu'un a appuyé sur le bouton SOS du robot","mailSOS_suj":"SOS","mailSOS_body":"<br> Une personne a besoin d'aide","mailBattery_suj":"Batterie Faible","mailBattery_body":"<br> Le robot a besoin d'etre chargé. Veuillez le mettre sur sa station de charge.","mailBlocked_suj":"Robot bloqué","mailBlocked_body":"<br> Le robot est bloqué <br>","mailFall_body":"Le robot détecte une personne à terre <br>","mailFall_suj":"CHUTE détectée","mailPerson_suj":"Personne détectée","mailPerson_body":"Le robot a détecté une personne <br>","mailRemove":"Suppression réussie","presentAlert_title":"Robot en charge","presentAlert_message":"Attention je vais reculer !","AlertConnectedToI":"Le robot est connecté à internet","presentConfirm_title":"Ronde suspendue","presentConfirm_message":"Reprendre la ronde ?","FallConfirm_title":"Chute détectée !","FallConfirm_message":"Reprendre la patrouille ?","RemoteConfirm_title":"Ronde arrêtée à distance","RemoteConfirm_message":"Reprendre la ronde ?","blockedAlert_title":"Robot bloqué !","blockedAlert_message":"Veuillez enlever l'obstacle ou me déplacer","goDockingConfirm_title":"Batterie: ","goDockingConfirm_message":"Aller en station de charge ?","lowBattery_title":"Batterie faible","lowBattery_message":"Aller en station de charge ?","errorlaunchAlert_title":"Une erreur est survenue","errorlaunchAlert_message":"Veuillez rententer de lancer la ronde","robotmuststayondocking_title":"Batterie faible","robotmuststayondocking_message":"Le robot doit rester sur la docking","errorNavAlert_title":"Une erreur est survenue","errorNavAlert_message":"Veuillez appeler le support technique si le problème persiste","lostAlert_title":"Robot perdu !","lostAlert_message":"Veuillez appeler le support technique","quitConfirm_title":"Quitter l'application ?","errorBlocked_title":"Robot bloqué","errorBlocked_message":"Veuillez vérifier que le robot est bien dégagé","statusRedPresent_title":"Erreur","statusRedPresent_message":"Veuillez me redémarrer OU appeler mon fabricant si le problème persiste","statusGreenPresent_title":"Statut vert","statusGreenPresent_message":"Le robot est en bonne santé !","leaveDockingConfirm_title":"Batterie: ","leaveDockingConfirm_message":"Sortir de la station de charge ?","leaveDockingAlert_title":"Robot en charge","leaveDockingAlert_message":"Veuillez me retirer de la station de charge","askHelp_title":"Aide demandée","askHelp_message":"Quelqu'un va venir vous aider","relocAlert_title":"Robot perdu","relocAlert_message":"Veuillez relocaliser le robot.","walk":"Marchez !","walklog":"Données de marche","date":"Date","meters":"Mètres","duration":"Durée","pause":"Pause","password":"Mot de passe","enterPassword":"Nécessaire pour accéder aux paramètres.","wrongPassword":"Mot de passe incorrect. Veuillez réessayer.","chosenguidedmode":"Bonjour ! Vous avez choisi la marche guidée ! Choisissez votre vitesse, la taille de la zone de marche et le mode d'exercice. N'oubliez pas de retirez les obstacles du chemin.","letsstart":"Appuyez sur Allons-y quand vous êtes prêt à marcher !","wantstop":"Vous voulez faire une pause ? On continue plus tard !","anonymous":"anonyme","getclose":"Veuillez vous approcher de la camera, je vais vous identifier","hi":"Bonjour ","nicetosee":"Content de vous voir !","register":"Pour vous enregistrer veuillez entrer un nom et vos identifiants shapes","failscan":"Je n'ai pas réussi à scanner votre visage, veuillez réessayer","notfound":"Utilisateur introuvable, assurez vous que les informations saisies sont correctes","errorreco":"Utilisateur introuvable, veuillez réessayer","noreco":"Je ne reconnais pas votre visage, Veuillez vous inscrire","pswshapes":"Shapes Mdp","loginshapes":"Shapes Id","uname":"Nom","connection":"CONNEXION","registration":"INSCRIPTION","valide":"VALIDER","anonym":"Utilisateur Anonyme","auth":"AUTHENTIFICATION","URL_assistancemobility":"assets/pdf/tuto_FR.pdf","rgpd_txt":"Conformément à la loi du 6 janvier 1978 modifiée et au « Règlement Général de Protection des Données », vos données personnelles de contact sont collectées afin que vous puissiez recevoir les alertes du robot Kompai (personne détectée, robot bloqué, batterie faible..).  Vos données seront traitées par Korian et Kompai pendant la durée requise pour l’utilisation de la plateforme. Vous pouvez, notamment, vous opposez au traitement de vos données,  obtenir une copie de vos données, les rectifier ou les supprimer en écrivant au DPO et en justifiant votre identité à l’adresse : rgpd@kompai.com ou KOMPAI Robotics, Technopole d'Izarbel - 97 allée Théodore Monod 64210 Bidart - France."}

/***/ }),

/***/ 314:
/***/ (function(module, exports) {

module.exports = {"quit":"LEAVE","battery":"BATTERY","return":"RETURN","tutorial":"TUTORIAL","param":"SETTINGS","receivesms":"RECEIVE ALERTS BY SMS","receivemail":"RECEIVE ALERTS BY EMAIL","changelangage":"CHANGE THE LANGUAGE","godocking":"ON THE WAY TO THE CHARGING STATION","moving":"TRAVELING IN PROGRESS TO :","charging_remaining":"ROBOT IN CHARGE - BATTERY: ","walkInProgress":"WALK AT YOUR OWN PACE WITHOUT PUSHING","notpush":"INSTALL YOURSELF AS IN THE PHOTO AND PRESS «LET'S GO»","mails":"Emails","sms":"SMS","langage":"Language","distance_covered":"Distance travelled","goto":"Go to :","inProgress":"IN PROGRESS","post":"Position","btn_go":"GO","btn_help":"Help","btn_walk":"Let's go !","btn_stop":"STOP","btn_charge":"LOAD","btn_cancel":"Cancel","btn_ok":"OK","btn_yes":"YES","btn_no":"NO","numexist":"This phone number already exists !","deletenum":"Please delete a phone number first","numadd":"Phone number added","numincorrect":"Invalid phone number","newnum":"New phone number","mailexist":"This address already exists ! ","userexist":"This user already exists !","deletemail":"Please delete an address first","mailadd":"Email address added!","useradd":"User added !","mailincorrect":"Invalid email address","newmail":"New address","newuser":"New user","cantstart":"Application launching failed. Check that the robot is correctly switched on.","nointernet":"No internet access","btn_save":"Save","choose_speed":"Choose speed","low":"Slow","mean":"Average","fast":"Fast","speed":"Speed","receivepic":"Receive pictures","editpswd":"EDIT PASSWORD","currentpswd":"Current password :","newpswd":"New password :","save":"Save","success":"Success","pswdsaved":"Password saved","langage1":"Langage","editpswd1":"Edit password","wrongpass":"Wrong password","gobehindrobot":"MOVE INTO THE WALK AREA","choosedestination":"CHOOSE DESTINATION AND PRESS «LET'S GO»","chooseround":"CHOOSE THE ROUND AND PRESS «LET'S GO»","gobackward":"PLEASE BACK UP, YOU ARE TOO CLOSE TO THE ROBOT","wait":"WAIT FOR THE END OF THE COUNT BEFORE WALKING","choosepoi":"Choose a POI","chooseexercice":"Choose a round","chooseuser":"Choose a user","choosemode":"Choose a mode","choose_walkzone":"Edit walk size","walkzone":"Walk size","round":"Round","destination":"Destination","map":"Map","mode":"Mode","user":"User","users":"Users","btn_pause":"PAUSE","stepback":"Please step back slightly.","closer":"Please get closer to the robot.","finishexo":"Well done ! You have completed the course.","bravo":"Well done !","atdestination":"Well done ! you have arrived at your destination","gof":"Go forward","gol":"Turn left","gor":"Turn right","timeofpauses":"Time of pauses","timewalk":"Time of walk","obstacle":"Stop on obstacle","slowdown":"Slow down","startpoi":"Place the robot on the starting point","smsSOS":"Someone pressed the robot's SOS button","mailSOS_suj":"SOS","mailSOS_body":"<br> A person needs help","mailBattery_suj":"Low Battery","mailBattery_body":"<br> The robot needs to be charged. Please put it on its charging station.","mailBlocked_suj":"Robot blocked","mailBlocked_body":"<br> The robot is blocked <br>","mailFall_body":"The robot detects a person ashore <br>","mailFall_suj":"FALL detected","mailPerson_suj":"Person detected","mailPerson_body":"The robot detected a person <br>","mailRemove":"Deletion done","presentAlert_title":"Robot in charge","presentAlert_message":"I'm going to back up !","AlertConnectedToI":"The robot is connected to Internet","presentConfirm_title":"Round Suspended","presentConfirm_message":"Continue the round ?","FallConfirm_title":"Fall detected !","FallConfirm_message":"Continue the patrol ?","RemoteConfirm_title":"Remote stop round","RemoteConfirm_message":"Continue the round ?","blockedAlert_title":"Robot blocked !","blockedAlert_message":"Please remove the obstacle or move me","goDockingConfirm_title":"Battery: ","goDockingConfirm_message":"Go to the charging station ?","lowBattery_title":"Low Battery","lowBattery_message":"Go to the charging station ?","errorlaunchAlert_title":"A mistake has occured","errorlaunchAlert_message":"Please try again to start the round","robotmuststayondocking_title":"Low Battery","robotmuststayondocking_message":"The robot must stay on the docking","errorNavAlert_title":"A mistake has occured","errorNavAlert_message":"Please call technical support if the problem persists","lostAlert_title":"Robot lost !","lostAlert_message":"Please call technical support","quitConfirm_title":"Leave the application ?","errorBlocked_title":"Robot blocked","errorBlocked_message":"Please check that the robot is clear","statusRedPresent_title":"Error","statusRedPresent_message":"Please restart me OR call my manufacturer if the problem persists","statusGreenPresent_title":"Green status","statusGreenPresent_message":"The robot is healthy !","leaveDockingConfirm_title":"Battery: ","leaveDockingConfirm_message":"Get out of the charging station ?","leaveDockingAlert_title":"Robot in charge","leaveDockingAlert_message":"Please, remove me from the charging station","askHelp_title":"Help requested","askHelp_message":"Someone is going to come and help you","relocAlert_title":"Robot Lost","relocAlert_message":"Please relocate the robot","walk":"Walk !","walklog":"Walk Logs","date":"date","meters":"meters","duration":"Duration","pause":"Pause","password":"Password","enterPassword":"Required to access settings.","wrongPassword":"Wrong password. Please try again.","chosenguidedmode":"Great, you have chosen Guided mode. Please choose the speed, walking zone and mode for the exercise. Remember to remove all the obstacles around.","letsstart":"Let’s start today’s session! Press “Let’s go” when you are ready to start.","wantstop":"I see you want to stop the exercise. Ok, we can continue later!","anonymous":"anonymous","getclose":"Please get close to the camera, I will scan your face","hi":"Hi ","nicetosee":"Nice to see you!","register":"To register please type your username, shapes login and password","failscan":"I failed to scan your face, please try again","notfound":"User not found, make sure you enter the correct information","errorreco":"I couldn’t recognize you, please try again","noreco":"I don't recognize your face, please register","pswshapes":"Shapes Password","loginshapes":"Shapes Login","uname":"Username","connection":"CONNECTION","registration":"REGISTRATION","valide":"VALIDATE","anonym":"Anonymous user","auth":"AUTHENTICATION","URL_assistancemobility":"assets/pdf/tuto_EN.pdf","rgpd_txt":"In accordance with the amended law of 6 January 1978 and the «General Data Protection Regulations», your personal contact data are collected so that you can receive alerts from the Kompai robot (person detected, robot blocked, low battery, etc.). Your data will be processed by Korian and Kompai for the time required to use the platform. You can, in particular, oppose the processing of your data, obtain a copy of your data, rectify or delete them by writing to the DPO and justifying your identity at: rgpd@kompai.com or KOMPAI Robotics, Technopole d 'Izarbel - 97 allée Théodore Monod 64210 Bidart - France."}

/***/ }),

/***/ 315:
/***/ (function(module, exports) {

module.exports = {"quit":"SALIR","battery":"BATERÍA","return":"REGRESAR","tutorial":"TUTORIAL","param":"PARÁMETROS","receivesms":"RECIBIR ALERTAS POR SMS","receivemail":"RECIBIR ALERTAS POR CORREO ELECTRÓNICO","changelangage":"CAMBIAR EL IDIOMA","godocking":"EN CAMINO A LA ESTACÍON DE CARGA","patrol":"Patrulla","patrolInProgress":"PATRULLANDO","roundInProgress":"RECORRIDO DE ENTRETENIMIENTO EN CURSO","moving":"EN MOVIMIENTO PARA :","charging_remaining":"ROBOT EN CARGA - BATERÍA: ","walkInProgress":"CAMINE A SU PROPIO RITMO SIN EMPUJAR","notpush":"COLÓQUESE COMO EN LA FOTO Y PRESIONE «EMPEZAR»","mails":"Correos electrónicos","sms":"SMS","langage":"Idioma","distance_covered":"Distancia recorrida","goto":"Ir a :","inProgress":"EN CURSO","post":"Posición","sentinel":"Centinela","morningRound":"Ronda de la mañana","eveningRound":"Ronda de la noche","btn_go":"INICIAR","btn_help":"Ayuda","btn_walk":"Empezar","btn_stop":"ALTO","btn_charge":"CARGAR","btn_cancel":"Cancelar","btn_ok":"OK","btn_yes":"SÍ","btn_no":"NO","numexist":"¡ Este número ya existe !","deletenum":"Por favor, borre un número primero","numadd":"Número añadido","numincorrect":"Número inválido","newnum":"Nuevo número","mailexist":"¡ Esta dirección de correo electrónico ya existe ! ","userexist":"Este usuario ya existe !","deletemail":"Por favor, elimine primero una dirección de correo electrónico","mailadd":"¡ Dirección de correo electrónico añadida !","useradd":"Usuario añadido !","mailincorrect":"Dirección de correo electrónico inválida","newmail":"Nueva dirección de correo electrónico","newuser":"Nuevo usuario","error":"Error","cantstart":"No se puede iniciar la aplicación. Compruebe si el robot está bien encendido.","nointernet":"Sin acceso a internet","btn_save":"Guardar","choose_speed":"Elige la velocidad","low":"Lenta","mean":"Media","fast":"Rápida","speed":"Velocidad","receivepic":"Recibir fotos","editpswd":"EDITAR CONTRASEÑA","currentpswd":"Contraseña actual :","newpswd":"Nueva contraseña :","save":"Guardar","success":"Está hecho !","pswdsaved":"Contraseña guardada","langage1":"Idioma","editpswd1":"Editar contraseña","wrongpass":"Contraseña incorrecta","gobehindrobot":"COLOCA A LA ZONA DE PASEO","choosedestination":"ELIGE EL DESTINO Y HAZ CLICK EN «EMPEZAR»","chooseround":"ELIGE LA RONDA Y HAZ CLICK EN «EMPEZAR»","gobackward":"POR FAVOR, ATRÁS, ESTÁS DEMASIADO CERCA DEL ROBOT","wait":"ESPERAR EL FINAL DE LA CUENTA ANTES DE CAMINAR","choosepoi":"Elige un PoI","chooseexercice":"Elige una ronda","chooseuser":"Elige un usuario","choosemode":"Elige un modo","choose_walkzone":"Cambiar zona de paso","walkzone":"Zona de paseo","round":"Ronda","destination":"Destino","map":"Mapa","mode":"Modo","user":"Usuario","users":"Usuarios","btn_pause":"PAUSA","stepback":"Por favor, retroceda un poco.","closer":"Por favor, acércate al robot.","finishexo":"Bien hecho ! Has completado el curso.","bravo":"Bien hecho !","atdestination":"Bien hecho ! has llegado a tu destino","gof":"Avanzar","gol":"Girar a la izquierda","gor":"Girar a la derecha","timeofpauses":"Descansos","timewalk":"Tiempo de caminata","obstacle":"Detener en obstáculo","slowdown":"Ralentizar","startpoi":"Coloque el robot en el punto de partida","smsSOS":"Alguien ha pulsado el botón SOS del robot","mailSOS_suj":"SOS","mailSOS_body":"<br> Una persona necesita ayuda","mailBattery_suj":"Batería Baja","mailBattery_body":"<br> Hay que cargar al robot. Por favor, pongalo en su estación de carga.","mailBlocked_suj":"Robot bloqueado","mailBlocked_body":"<br> El robot está bloqueado <br>","mailFall_body":"El robot detecta a una persona en el suelo <br>","mailFall_suj":"CAÍDA detectada","mailPerson_suj":"Persona detectada","mailPerson_body":"El robot detectó a una persona <br>","mailRemove":"Supresión realizada","presentAlert_title":"Robot en carga","presentAlert_message":"¡ Voy a ir hacia atrás !","AlertConnectedToI":"El robot está conectado a internet","presentConfirm_title":"Ronda suspendida","presentConfirm_message":"¿ Volver a la ronda ?","FallConfirm_title":"¡ Caída detectada !","FallConfirm_message":"¿ Volver a la patrulla ?","RemoteConfirm_title":"Ronda parada a distancia","RemoteConfirm_message":"¿ Volver a la ronda ?","blockedAlert_title":"¡ Robot bloqueado !","blockedAlert_message":"Por favor, quite el obstáculo o muevame","goDockingConfirm_title":"Batería: ","goDockingConfirm_message":"¿ Ir a la estación de carga ?","lowBattery_title":"Batería Baja","lowBattery_message":"¿ Ir a la estación de carga ?","errorlaunchAlert_title":"Se ha producido un error","errorlaunchAlert_message":"Intente volver a empezar la ronda, por favor","robotmuststayondocking_title":"Batería Baja","robotmuststayondocking_message":"El robot debe quedarse en el atraque","errorNavAlert_title":"Se ha producido un error","errorNavAlert_message":"Por favor, llame al soporte técnico si el problema persiste","lostAlert_title":"¡ Robot perdido !","lostAlert_message":"Por favor, llame al soporte técnico","quitConfirm_title":"¿ Salir de la aplicación ?","errorBlocked_title":"Robot bloqueado","errorBlocked_message":"Compruebe si el robot está desbloqueado","statusRedPresent_title":"Error","statusRedPresent_message":"Por favor, reinicie me o llame a mi fabricante si el problema persiste","statusGreenPresent_title":"Situación verde","statusGreenPresent_message":"¡ El robot está sano !","leaveDockingConfirm_title":"Batería: ","leaveDockingConfirm_message":"¿ Salir de la estación de carga ?","leaveDockingAlert_title":"Robot cargando","leaveDockingAlert_message":"Por favor, retireme de la estación de carga","askHelp_title":"Ayuda solicitada","askHelp_message":"Alguien vendrá a ayudarte","relocAlert_title":"Robot perdido","relocAlert_message":"Reubique el robot","walk":"Ande","walklog":"Datos de caminata","date":"Fecha","meters":"Metros","duration":"Duración","pause":"Pausa","password":"Contraseña","enterPassword":"Requerido para acceder a la configuración.","wrongPassword":"Contraseña incorrecta. Por favor, intente de nuevo.","chosenguidedmode":"¡Genial! Ha elegido la modalidad guiada. Por favor, elija la velocidad, la zona de paso y el modo del ejercicio. Recuerde colocarse en el punto de salida y apartar todos los obstáculos que haya alrededor.","letsstart":"¡Vamos a empezar la sesión de hoy! Pulse “Empezar” cuando esté.","wantstop":"Veo que no quiere seguir. De acuerdo, podemos continuar más tarde","anonymous":"anónimo","getclose":"Por favor, acérquese a la cámara para que pueda reconocerle.","hi":"Hola ","nicetosee":"¡Me alegro de verle!","register":"Para registrarse por favor inserte su nombre, usuario y contraseña","failscan":"No pude escanear tu cara, por favor inténtalo de nuevo","notfound":"Usuario no encontrado, asegúrese de ingresar la información correcta","errorreco":"No le he podido reconocer, por favor inténtelo de nuevo","noreco":"No reconozco tu cara, por favor regístrate","pswshapes":"Shapes Contraseña","loginshapes":"Shapes ID","uname":"Nombre","connection":"CONEXION","registration":"REGISTRO","valide":"VALIDAR","anonym":"Usuario anónimo","auth":"AUTENTICACIÓN","URL_assistancemobility":"assets/pdf/tuto_EN.pdf","rgpd_txt":"De acuerdo con la ley modificada del 6 de enero de 1978 y el «Reglamento general de protección de datos», sus datos de contacto personales se recopilan para que pueda recibir alertas del robot Kompai (persona detectada, robot bloqueado, batería baja, etc.). Korian y Kompai procesarán sus datos durante el tiempo necesario para utilizar la plataforma. Puede, en particular, oponerse al tratamiento de sus datos, obtener una copia de sus datos, rectificarlos o suprimirlos escribiendo al DPO y justificando su identidad en: rgpd@kompai.com o KOMPAI Robotics, Technopole d 'Izarbel - 97 allée Théodore Monod 64210 Bidart - Francia."}

/***/ }),

/***/ 316:
/***/ (function(module, exports) {

module.exports = {"quit":"ΕΞΟΔΟΣ","battery":"ΜΠΑΤΑΡΙΑ","return":"ΕΠΙΣΤΡΟΦΗ","tutorial":"ΟΔΗΓΙΕΣ","param":"ΡΥΘΜΙΣΕΙΣ","receivesms":"ΛΗΨΗ ΕΙΔΟΠΟΙΗΣΕΩΝ ΜΕΣΩ ΜΗΝΥΜΑΤΟΣ","receivemail":"ΛΗΨΗ ΕΙΔΟΠΟΙΗΣΕΩΝ ΜΕΣΩ EMAIL","changelangage":"ΑΛΛΑΓΗ ΓΛΩΣΣΑΣ","godocking":"ΕΠΙΣΤΡΟΦΗ ΣΤΟ ΣΤΑΘΜΟ ΦΟΡΤΙΣΗΣ","moving":"ΣΕ ΔΙΑΔΙΚΑΣΊΑ ΜΕΤΑΚΙΝΗΣΗΣ ΠΡΟΣ:","charging_remaining":"ΡΟΜΠΟΤ ΣΕ ΦΟΡΤΙΣΗ - ΜΠΑΤΑΡΙΑ: ","walkInProgress":"ΠΕΡΠΑΤΗΣΤΕ ΣΤΟ ΔΙΚΟ ΣΑΣ ΡΥΘΜΟ ΧΩΡΙΣ ΝΑ ΣΠΡΩΧΝΕΤΕ","notpush":"ΠΑΡΤΕ ΘΕΣΗ ΟΠΩΣ ΔΕΙΧΝΕΙ Η ΦΩΤΟΓΡΑΦΙΑ ΚΑΙ ΠΑΤΗΣΤΕ «ΠΑΜΕ»","mails":"Emails","sms":"ΜΗΝΥΜΑΤΑ","langage":"Γλώσσα","distance_covered":"Απόσταση που διανύθηκε","goto":"Πάμε σε :","inProgress":"ΣΕ ΕΞΕΛΙΞΗ","post":"Θέση","btn_go":"ΠΑΜΕ","btn_help":"Βοήθεια","btn_walk":"ΠΑΜΕ","btn_stop":"ΠΑΥΣΗ","btn_charge":"ΦΟΡΤΩΣΗ","btn_cancel":"Ακύρωση","btn_ok":"OK","btn_yes":"ΝΑΙ","btn_no":"ΟΧΙ","numexist":"Αυτός ο αριθμός τηλεφώνου υπάρχει ήδη !","deletenum":"Παρακαλώ διαγράψτε έναν αριθμό τηλεφώνου πρώτα","numadd":"Ο αριθμός τηλεφώνου προστέθηκε","numincorrect":"Μη έγκυρος αριθμός τηλεφώνου","newnum":"Νέος αριθμός τηλεφώνου","mailexist":"Αυτή η διεύθυνση email υπάρχει ήδη ! ","userexist":"Αυτός ο χρήστης υπάρχει ήδη !","deletemail":"Παρακαλούμε διαγράψτε μία διεύθυνση email πρώτα","mailadd":"Η διεύθυνση email προστέθηκε !","useradd":"Ο χρήστης προστέθηκε !","mailincorrect":"Μη έγκυρη διεύθυνση email","newmail":"Νέα διεύθυνση email","newuser":"Νέος χρήστης","cantstart":"Η φόρτωση της εφαρμογής απέτυχε. Ελέγξτε ότι το ρομπότ έχει ενεργοποιηθεί σωστά.","nointernet":"Δεν υπάρχει σύνδεση στο διαδίκτυο","btn_save":"Αποθήκευση","choose_speed":"Επιλογή ταχύτητας","speed":"Ταχύτητα","receivepic":"Λήψη φωτογραφιών","editpswd":"ΕΠΕΞΕΡΓΑΣΙΑ ΚΩΔΙΚΟΥ ΠΡΟΣΒΑΣΗΣ","currentpswd":"Τρέχων κωδικός πρόσβασης :","newpswd":"Νεός κωδικός πρόσβασης :","save":"Αποθήκευση","success":"Επιτυχία","pswdsaved":"Ο κωδικός πρόσβασης αποθηκεύτηκε","langage1":"Γλώσσα","editpswd1":"Επεξεργασία κωδικού πρόσβασης","wrongpass":"Λάθος κωδικός πρόσβασης","gobehindrobot":"ΠΡΟΧΩΡΗΣΤΕ ΣΤΗΝ ΚΑΤΑΛΛΗΛΗ ΠΕΡΙΟΧΗ ΒΑΔΙΣΗΣ","choosedestination":"ΕΠΙΛΕΞΤΕ ΠΡΟΟΡΙΣΜΟ ΚΑΙ ΠΑΤΗΣΤΕ «ΠΑΜΕ»","chooseround":"ΕΠΙΛΕΞΤΕ ΤΗΝ ΑΣΚΗΣΗ ΚΑΙ ΠΑΤΗΣΤΕ «ΠΑΜΕ»","gobackward":"ΠΑΡΑΚΑΛΩ ΜΕΤΑΚΙΝΗΘΕΙΤΕ ΠΙΣΩ, ΕΙΣΤΕ ΠΟΛΥ ΚΟΝΤΑ ΣΤΟ ΡΟΜΠΟΤ","wait":"ΠΕΡΙΜΕΝΕΤΕ ΜΕΧΡΙ ΤΟ ΤΕΛΟΣ ΤΗΣ ΑΝΤΙΣΤΡΟΦΗΣ ΜΕΤΡΗΣΗΣ ΠΡΙΝ ΞΕΚΙΝΗΣΕΤΕ ΤΟ ΠΕΡΠΑΤΗΜΑ","choosepoi":"Επιλέξτε ένα σημείο","chooseexercice":"Επιλέξτε μια διαδρομή","chooseuser":"Επιλέξτε ένα χρήστη","choosemode":"Επιλέξτε μία λειτουργία","choose_walkzone":"Επεξεργαστείτε την απόσταση της διαδρομής","walkzone":"Απόσταση διαδρομής","round":"Διαδρομή","destination":"Προορισμός","map":"Χάρτης","mode":"Λειτουργία","user":"Χρήστης","users":"Χρήστες","btn_pause":"ΠΑΥΣΗ","stepback":"Παρακαλώ μετακινηθείτε λίγο πίσω.","closer":"Παρακαλώ πλησιάστε το ρομπότ","finishexo":"Μπράβο! Ολοκληρώσατε την άσκηση!","bravo":"Μπράβο !","atdestination":"Μπράβο ! Φτάσατε στον προορισμό σας!","gof":"Προχωρήστε μπροστά","gol":"Στρίψτε αριστερά","gor":"Στρίψτε δεξιά","timeofpauses":"Διάρκεια παύσεων","timewalk":"Διάρκεια βάδισης","obstacle":"Σταματήστε στο εμπόδιο","slowdown":"Πηγαίνετε πιο αργά","startpoi":"Τοποθετήστε το ρομπότ στο σημείο εκκίνησης","smsSOS":"Κάποιος πάτησε το κουμπί κινδύνου του ρομπότ","mailSOS_suj":"ΚΙΝΔΥΝΟΣ","mailSOS_body":"<br> Κάποιος χρειάζεται βοήθεια","mailBattery_suj":"Χαμηλή μπαταρία","mailBattery_body":"<br> Το ρομπότ χρειάζεται φόρτιση. Παρακαλώ τοποθετήστε το στο σταθμό φόρτισης.","mailBlocked_suj":"Μπλοκαρισμένο ρομπότ","mailBlocked_body":"<br> Το ρομπότ έχει μπλοκαριστεί <br>","mailFall_body":"Το ρομπότ ανιχνεύει κάποιο άτομο στην πορεία <br>","mailFall_suj":"Ανιχνεύθηκε ΠΤΩΣΗ","mailPerson_suj":"Ανιχνεύθηκε άτομο","mailPerson_body":"Το ρομπότ ανίχνευσε ένα άτομο <br>","mailRemove":"Η διαγραφή έγινε","presentAlert_title":"Το ρομπότ είναι σε κατάσταση φόρτισης","presentAlert_message":"Θα κρατήσω αντίγραφα ασφαλείας !","AlertConnectedToI":"Το ρομπότ είναι συνδεδεμένο στο διαδίκτυο","presentConfirm_title":"Διακοπή διαδρομής","presentConfirm_message":"Θέλετε να συνεχίσετε τη διαδρομή ;","FallConfirm_title":"Ανιχνεύθηκε πτώση!","FallConfirm_message":"Θέλετε να συνεχίσετε τη διαδρομή ;","RemoteConfirm_title":"Απομακρυσμένη διακοπή διαδρομής","RemoteConfirm_message":"Θέλετε να συνεχίσετε τη διαδρομή ;","blockedAlert_title":"Το ρομπότ έχει μπλοκαριστεί !","blockedAlert_message":"Παρακαλώ αφαιρέστε το εμπόδιο ή μετακίνήστε με","goDockingConfirm_title":"Μπαταρία: ","goDockingConfirm_message":"Μετάβαση στο σταθμό φόρτισης ;","lowBattery_title":"Χαμηλή μπαταρία","lowBattery_message":"Μετάβαση στο σταθμό φόρτισης ;","errorlaunchAlert_title":"Προέκυψε κάποιο σφάλμα","errorlaunchAlert_message":"Παρακαλώ προσπαθήστε ξανά για να ξεκινήσετε τη διαδρομή","robotmuststayondocking_title":"Χαμηλή μπαταρία","robotmuststayondocking_message":"Το ρομπότ πρέπει να μείνει στο σταθμό φόρτισης","errorNavAlert_title":"Προέκυψε κάποιο σφάλμα","errorNavAlert_message":" Παρακαλώ καλέστε την τεχνική υποστήριξη σε περίπτωση που προκύψει κάποιο πρόβλημα","lostAlert_title":"Το ρομπότ χάθηκε !","lostAlert_message":"Παρακαλώ καλέστε την τεχνική υποστήριξη","quitConfirm_title":"Θέλετε να βγείτε από την εφαρμογή ;","errorBlocked_title":"Το ρομπότ έχει μπλοκαριστεί","errorBlocked_message":"Παρακαλούμε ελέγξτε εάν εμποδίζει κάτι το ρομπότ","statusRedPresent_title":"Σφάλμα","statusRedPresent_message":"Παρακαλώ επανεκκινήστε το ρομπότ ή καλέστε τον κατασκευαστή αν το πρόβλημα παραμένει","statusGreenPresent_title":"Κατάσταση ενεργή","statusGreenPresent_message":"Το ρομπότ είναι υγιές!","leaveDockingConfirm_title":"Μπαταρία: ","leaveDockingConfirm_message":"Αποσύνδεση από το σταθμό φόρτισης ;","leaveDockingAlert_title":"Το ρομπότ είναι σε κατάσταση φόρτισης","leaveDockingAlert_message":"Παρακαλώ, απομακρύνετέ με από το σταθμό φόρτισης","askHelp_title":"Ζητήθηκε βοήθεια","askHelp_message":"Κάποιος θα έρθει να σας βοηθήσει","relocAlert_title":"Το ρομπότ χάθηκε","relocAlert_message":"Παρακαλώ μετακινήστε το ρομπότ ","walk":"Περπατήστε !","walklog":"Αρχεία Βάδισης","date":"Ημερομηνία","meters":"Μέτρα","duration":"Διάρκεια","pause":"Παύση","password":"Κωδικός","enterPassword":"Απαιτείται για να αποκτήσετε πρόσβαση στις ρυθμίσεις.","wrongPassword":"Λάθος κωδικός πρόσβασης. Παρακαλώ προσπαθήστε ξανά.","chosenguidedmode":"Ωραία έχετε επιλέξει τη λειτουργία καθοδήγησης. Παρακαλώ επιλέξτε την ταχύτητα, την περιοχή βάδισης και τη λειτουργία για την άσκηση. Θυμηθείτε να απομακρύνετε ή να αφαιρέσετε όλα τα εμπόδια στο χώρο.","letsstart":"Ας ξεκινήσουμε τη σημερινή συνεδρία! Επιλέξτε “Πάμε” όταν είστε έτοιμοι να ξεκινήσετε.","wantstop":"Φαίνεται ότι θέλετε να σταματήσετε την άσκηση. ΟΚ μπορούμε να συνεχίσουμε αργότερα!","anonymous":"άγνωστος","getclose":"Παρακαλώ πλησιάστε την κάμερα, θα σκανάρω το πρόσωπό σας","hi":"Γεια ","nicetosee":"Χαίρομαι που σας βλέπω!","register":"Για εγγραφή πληκτρολογήστε το όνομα χρήστη και τον κωδικό πρόσβασής σας.","failscan":"Δεν κατάφερα να σκανάρω το πρόσωπό σας, παρακαλώ προσπαθήστε πάλι","notfound":"Ο χρήστης δεν βρέθηκε, βεβαιωθείτε ότι εισάγετε τα σωστά στοιχεία","errorreco":"Δεν κατάφερα να σας αναγνωρίσω, παρακαλώ προσπαθήστε πάλι","noreco":"Δεν αναγνωρίζω το πρόσωπό σας, παρακαλώ κάνετε εγγραφή","pswshapes":"Κωδικός πρόσβασης","loginshapes":"Σύνδεση","uname":"Όνομα χρήστη","connection":"ΣΥΝΔΕΣΗ","registration":"ΕΓΓΡΑΦΗ","valide":"ΕΠΙΒΕΒΑΙΩΣΗ","anonym":"Ανώνυμος χρήστης","auth":"ΑΥΘΕΝΤΙΚΟΠΟΙΗΣΗ","URL_assistancemobility":"assets/pdf/tuto_EN.pdf","rgpd_txt":"Σύμφωνα με το νόμο που καθορίστηκε στις 6 Ιανουαρίου 1978 και τον «Γενικό Κανονισμό για την Προστασία των Δεδομένων», τα προσωπικά σας δεδομένα συλλέγονται για να μπορείτε να λάβετε ειδοποιήσεις από το ρομπότ Kompai (ανίχνευση ατόμου, μπλοκάρισμα ρομπότ, χαμηλή μπαταρία κ.λπ.). Τα δεδομένα σας θα επεξεργαστούν από την Korian και την Kompai για το χρόνο που απαιτείται για τη χρήση της πλατφόρμας. Συγκεκριμένα, μπορείτε να ανακαλέσετε την επεξεργασία των δεδομένων σας, να λάβετε ένα αντίγραφο των δεδομένων σας, να αποσύρετε ή να διαγράψετε τα δεδομένα σας, αφού απενθυθείτε στον Υπεύθυνο Προστασίας Δεδομένων και ορίσετε την ταυτότητά σας στο rgpd@kompai.com or KOMPAI Robotics, Technopole d 'Izarbel - 97 allée Théodore Monod 64210 Bidart - France."}

/***/ }),

/***/ 328:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 330:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 350:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 351:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 352:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 36:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SpeechService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__param_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SpeechService = /** @class */ (function () {
    function SpeechService(param) {
        this.param = param;
        this.volumeState = 2;
        this.msg = new SpeechSynthesisUtterance();
        this.msg.onerror = function (event) {
            console.error("An error has occurred with the speech synthesis: " + event.error);
        };
        this.msg.volume = parseFloat("1");
        this.msg.rate = parseFloat("1");
        this.msg.pitch = parseFloat("1");
        this.synth = window.speechSynthesis;
        //this.msg.voice = speechSynthesis.getVoices().filter(function(voice) { return voice.name ; })[10];
    }
    SpeechService.prototype.getVoice = function () {
        var _this = this;
        var voices = speechSynthesis
            .getVoices()
            .filter(function (voice) {
            return voice.localService == true && voice.lang == _this.param.langage;
        });
        if (voices.length > 1) {
            this.msg.voice = voices[1];
        }
        else {
            this.msg.voice = voices[0];
        }
    };
    // Create a new utterance for the specified text and add it to
    // the queue.
    SpeechService.prototype.speak = function (text) {
        if (this.param.allowspeech == 1) {
            this.synth.cancel();
            this.msg.lang = this.param.langage;
            // Create a new instance of SpeechSynthesisUtterance.
            // Set the text.
            this.getVoice();
            this.msg.text = text;
            //this.msg.voice = speechSynthesis.getVoices().filter(function(voice) { return voice.name ; })[10];
            //console.log(msg);
            // Queue this utterance.
            this.synth.speak(this.msg);
        }
        console.log(this.msg);
    };
    SpeechService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__param_service__["a" /* ParamService */]])
    ], SpeechService);
    return SpeechService;
}());

//# sourceMappingURL=speech.service.js.map

/***/ }),

/***/ 367:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 62:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChooseAssistancePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_api_service__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_alert_service__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_popup_service__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_param_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_speech_service__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__start_assistance_start_assistance__ = __webpack_require__(115);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var ChooseAssistancePage = /** @class */ (function () {
    function ChooseAssistancePage(speech, popup, alert, param, navCtrl, api, renderer) {
        var _this = this;
        this.speech = speech;
        this.popup = popup;
        this.alert = alert;
        this.param = param;
        this.navCtrl = navCtrl;
        this.api = api;
        this.renderer = renderer;
        this.videoWidth = 0;
        this.videoHeight = 0;
        this.start = {
            x: 0,
            y: 0,
        };
        this.clickpose = {
            x: 0,
            y: 0,
        };
        this.startPage = __WEBPACK_IMPORTED_MODULE_7__start_assistance_start_assistance__["a" /* StartAssistancePage */];
        this.map_url = "http://" + this.param.localhost + "/api/maps/current/image";
        this.constraints = {
            //audio: false,
            //video: {deviceId: "f9860b260e18d7fd1687d8567e21b49c4bb26f87606020741717e11f6179f409"},
            video: true,
            facingMode: "environment",
            width: { ideal: 1920 },
            height: { ideal: 1080 },
        };
        this.api.metercovered = 0;
        this.updateMeter = setInterval(function () { return _this.getUpdate(); }, 500); // update meter and walker detection every 1/2 secondes
        this.cptduration = setInterval(function () { return _this.mail(); }, 120000); // toutes les 2 min actualise time
        this.api.inUse = false;
        this.cpt_locked = 0;
        this.is_blocked = false;
        this.speech.getVoice();
        // declecnche la popup de permission camera si besoin
        //this.setupconstraint();
        this.selectOptionspeed = {
            title: this.param.datatext.choose_speed,
        };
        this.selectOptionpoi = {
            title: this.param.datatext.choosepoi,
        };
        this.selectOptionround = {
            title: this.param.datatext.chooseexercice,
        };
        this.selectOptionuser = {
            title: this.param.datatext.chooseuser,
        };
        this.selectOptionmode = {
            title: this.param.datatext.choosemode,
        };
        this.selectOptionsWalkZone = {
            title: this.param.datatext.choose_walkzone,
        };
        this.api.getCurrentLocations();
        this.api.walkercmdstartdetect();
    }
    ChooseAssistancePage.prototype.mail = function () {
        this.param.cptDuration(); //update duration
        if (this.param.durationNS.length > 1) {
            if (this.api.wifiok) {
                this.alert.duration(this.param.durationNS[0], this.api.mailAddInformationBasic());
            }
        }
    };
    ChooseAssistancePage.prototype.ngOnInit = function () {
        var _this = this;
        this._CANVAS = this.canvasEl.nativeElement;
        this._CONTEXT = this._CANVAS.getContext("2d");
        this._CANVAS.style.width = "100%";
        this._CANVAS.style.height = "100%";
        this.heightcol =
            document.getElementById("body").clientHeight -
                document.getElementById("firstrow").clientHeight * 2;
        this.widthcol = document.getElementById("testcol").clientWidth;
        this.param.tablewalklogs.walkuser_id = this.api.selectUser;
        setTimeout(function () {
            _this.speech.speak(_this.param.datatext.chosenguidedmode);
        }, 1000);
        setTimeout(function () {
            if (!_this.api.inUse) {
                _this.speech.speak(_this.param.datatext.letsstart);
            }
        }, 20000);
    };
    ChooseAssistancePage.prototype.updatemap = function () {
        if (this.api.differential_status.CurrentAngularSpeed > 0.005 || this.api.differential_status.CurrentLinearSpeed > 0.01 || this._CANVAS.width == 0) {
            this.createImageFromBlob();
        }
    };
    ChooseAssistancePage.prototype.startAssistance_page = function (ev) {
        ev.preventDefault();
        this.speech.synth.cancel();
        if (this.api.selectMode === "round") {
            var x1 = this.api.round_selected.Locations[0].Location.Pose.X;
            var y1 = this.api.round_selected.Locations[0].Location.Pose.Y;
            var x2 = this.api.localization_status.positionx;
            var y2 = this.api.localization_status.positiony;
            if (this.getDistance(x1, y1, x2, y2) < 1) {
                this.onRefreshMeter();
                this.api.resume = false;
                this.navCtrl.push(this.startPage);
                this.api.walkercmdstartdetect();
            }
            else {
                //console.log("Placez le robot sur le point de départ");
                this.speech.speak(this.param.datatext.startpoi);
                this.popup.showToast(this.param.datatext.startpoi, 3000, "middle");
            }
        }
        else {
            this.onRefreshMeter();
            this.api.resume = false;
            this.navCtrl.push(this.startPage);
            this.api.walkercmdstartdetect();
        }
    };
    ChooseAssistancePage.prototype.ionViewWillLeave = function () {
        clearInterval(this.intervmap);
        //clearInterval(this.updateMeter);
        //clearInterval(this.cptduration);
    };
    ChooseAssistancePage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.api.abortNavHttp();
        this.getImageFromService();
        this.intervmap = setInterval(function () { return _this.updatemap(); }, 400);
        this.api.putStopDetecthttp();
        this.api.cptpoi = 1;
    };
    ChooseAssistancePage.prototype.getUpdate = function () {
        //console.log(this.api.selectSpeed);
        if (this.api.close_app) {
            clearInterval(this.updateMeter);
            clearInterval(this.cptduration);
        }
        //this.updateTrajectory();
        //this.watchIfLocked();
        //this.getDetect();
        //this.getMeter();
        //this.updatedirection();
    };
    ChooseAssistancePage.prototype.watchIfLocked = function () {
        if (this.api.towardDocking) {
            if (this.api.docking_status.status === 2 ||
                this.api.docking_status.status === 3 ||
                (this.api.anticollision_status.forward < 2 &&
                    this.api.anticollision_status.right < 2 &&
                    this.api.anticollision_status.left < 2)) {
                this.cpt_locked = 0;
                if (this.is_blocked) {
                    this.is_blocked = false;
                    this.popup.alert_blocked.dismiss();
                }
            }
            else if (this.api.anticollision_status.forward === 2 ||
                this.api.anticollision_status.right === 2 ||
                this.api.anticollision_status.left === 2) {
                this.cpt_locked += 1;
            }
            if (this.cpt_locked > 40 && !this.is_blocked) {
                this.popup.blockedAlert();
                this.is_blocked = true;
                this.alert.blockingdocking(this.api.mailAddInformationBasic());
            }
            else if (this.cpt_locked === 100 && this.is_blocked) {
                this.captureBlocked();
            }
        }
    };
    ChooseAssistancePage.prototype.getDetect = function () {
        // if smart bouton is pressed or the bouton A of the gamepad is pressed
        if (!this.api.walker_states.detected ||
            this.api.btnPush ||
            !this.api.is_connected) {
            if (this.api.inUse) {
                //start the detection and stop the enslavement of the legs
                this.api.putStopDetecthttp();
                this.api.abortNavHttp();
                // change the color and logo of the box
                this.api.inUse = false;
            }
            if (this.api.towardDocking && this.api.btnPush) {
                this.api.abortNavHttp();
                this.api.towardDocking = false;
                this.api.putStopDetecthttp();
            }
            this.api.btnPush = false;
        }
    };
    ChooseAssistancePage.prototype.updateTrajectory = function () {
        // listen the round status to allow the robot to go to the next
        //// go docking or go poi in progress
        if (this.api.towardDocking) {
            this.api.appStart = false;
            if (this.api.differential_status.status === 2) {
                // if current is hight
                this.api.towardDocking = false;
                this.api.abortNavHttp();
                this.popup.errorBlocked();
                this.alert.errorblocked(this.api.mailAddInformationBasic());
                this.captureBlocked();
            }
            else if (this.api.navigation_status.status === 5) {
                // nav error
                this.api.towardDocking = false;
                this.api.abortNavHttp();
                this.popup.errorNavAlert();
                this.alert.naverror(this.api.mailAddInformationBasic());
            }
            else if (this.api.docking_status.status === 3) {
                this.api.walkercmdstartdetect();
                this.api.towardDocking = false;
                this.alert.charging(this.api.mailAddInformationBasic());
                this.api.close_app = true; //close application
                setTimeout(function () {
                    window.close();
                }, 1000);
            }
            else if (this.api.towardDocking &&
                this.api.navigation_status.status === 0 &&
                this.api.docking_status.detected) {
                //connect to the docking when he detect it
                this.api.connectHttp();
            }
        }
    };
    ChooseAssistancePage.prototype.onRefreshMeter = function () {
        this.api.traveldebut = this.api.statistics_status.totalDistance;
        this.api.metercovered = 0;
    };
    ChooseAssistancePage.prototype.on_mouse_down = function (evt) {
        if (evt.button == 0) {
            evt.preventDefault();
            this.moving = true;
            this.start.x = evt.x;
            this.start.y = evt.y;
            var x = (evt.offsetX * this._CANVAS.width) / this._CANVAS.clientWidth;
            var y = (evt.offsetY * this._CANVAS.height) / this._CANVAS.clientHeight;
            this.clickpose.x =
                (x - this.offsetX - this._CANVAS.width / 2) * this.resomap;
            this.clickpose.y =
                -(y - this.offsetY - this._CANVAS.height / 2) * this.resomap;
        }
        this.createImageFromBlob();
    };
    ChooseAssistancePage.prototype.on_mouse_up = function (evt) {
        if (this.moving) {
            var offset = {
                x: this.offsetX,
                y: this.offsetY,
                t: 0,
            };
            offset.x += evt.x - this.start.x;
            offset.y += evt.y - this.start.y;
            this.start.x = evt.x;
            this.start.y = evt.y;
            this.offsetX = offset.x;
            this.offsetY = offset.y;
        }
        this.moving = false;
        this.createImageFromBlob();
    };
    ChooseAssistancePage.prototype.on_mouse_move = function (evt) {
        if (this.moving) {
            evt.preventDefault();
            var offset = {
                x: this.offsetX,
                y: this.offsetY,
            };
            offset.x += evt.x - this.start.x;
            offset.y += evt.y - this.start.y;
            this.start.x = evt.x;
            this.start.y = evt.y;
            this.offsetX = offset.x;
            this.offsetY = offset.y;
        }
        this.createImageFromBlob();
    };
    ChooseAssistancePage.prototype.on_wheel = function (evt) {
        evt.preventDefault();
        if (evt.deltaY > 0) {
            if (!(this.resomap >= this.initialzoom)) {
                this.resomap = this.resomap + 150 / 100000;
            }
            else {
                this.offsetX = 0;
                this.offsetY = 0;
            }
        }
        else {
            if (this.resomap > this.initialzoom / 2) {
                this.resomap = this.resomap - 150 / 100000;
            }
        }
        this.createImageFromBlob();
    };
    ChooseAssistancePage.prototype.on_zoom_out = function (ev) {
        ev.preventDefault();
        //zoom -
        if (!(this.resomap >= this.initialzoom)) {
            this.resomap = this.resomap + 150 / 100000;
        }
        else {
            this.offsetX = 0;
            this.offsetY = 0;
        }
        this.createImageFromBlob();
    };
    ChooseAssistancePage.prototype.on_zoom_in = function (ev) {
        ev.preventDefault();
        //zoom +
        if (this.resomap > this.initialzoom / 2) {
            this.resomap = this.resomap - 150 / 100000;
        }
        this.createImageFromBlob();
    };
    ChooseAssistancePage.prototype.createImageFromBlob = function () {
        var _this = this;
        if (this._CANVAS.getContext) {
            //console.log(i);
            this._CANVAS.width = this.imge.width;
            this._CANVAS.height = this.imge.height;
            if (this._CANVAS.width == 0) {
                console.log("width 0");
            }
            var w = this._CANVAS.width;
            var h = this._CANVAS.height;
            var r = this.resomap;
            var x = this.offsetX;
            var y = this.offsetY;
            var xm = this.api.mapdata.Offset.X;
            var ym = this.api.mapdata.Offset.Y;
            var rm = this.api.mapdata.Resolution;
            var wm = this.api.mapdata.Width;
            var hm = this.api.mapdata.Height;
            var dx = xm / r + x;
            var dy = h - (ym + hm * rm) * (1 / r) + y;
            var dw = (wm * rm) / r;
            var dh = (hm * rm) / r;
            if (this.api.id_current_map) {
                this._CONTEXT.drawImage(this.imge, dx, dy, dw, dh);
            }
            //console.log('dessin image');
            if (this.api.all_locations != undefined) {
                if (this.api.inUse) {
                    console.log("marche en cours");
                }
                else {
                    if (this.api.selectMode === "destination") {
                        this.api.all_locations.forEach(function (evenement) {
                            if (evenement.Label !== "qr") {
                                _this._CONTEXT.beginPath();
                                _this._CONTEXT.font =
                                    "" + Math.round(_this._CANVAS.width * 0.025) + "px Arial";
                                //console.log(evenement);
                                _this._CONTEXT.fillStyle = "rgba(26, 156, 195,0.7)";
                                _this._CONTEXT.fillText(evenement.Id, // + "- " + evenement.Name,
                                evenement.Pose.X / _this.resomap + _this.offsetX - 20, _this._CANVAS.height -
                                    (evenement.Pose.Y / _this.resomap - _this.offsetY) -
                                    10);
                                _this._CONTEXT.beginPath();
                                //console.log(evenement.Pose)
                                _this._CONTEXT.arc(evenement.Pose.X / _this.resomap + _this.offsetX, _this._CANVAS.height -
                                    (evenement.Pose.Y / _this.resomap - _this.offsetY), _this._CANVAS.width * 0.005, 0, 2 * Math.PI);
                                _this._CONTEXT.lineWidth = 1;
                                _this._CONTEXT.fillStyle = "rgb(26, 156, 195)";
                                _this._CONTEXT.fill();
                                _this._CONTEXT.beginPath();
                                _this._CONTEXT.save();
                                _this._CONTEXT.translate(evenement.Pose.X / _this.resomap + _this.offsetX, _this._CANVAS.height -
                                    (evenement.Pose.Y / _this.resomap - _this.offsetY));
                                _this._CONTEXT.rotate(-evenement.Pose.T);
                                _this._CONTEXT.moveTo(0, 0);
                                _this._CONTEXT.lineWidth = 2;
                                _this._CONTEXT.lineTo(2 * _this._CANVAS.width * 0.009, 0);
                                _this._CONTEXT.strokeStyle = "rgb(26, 156, 195)";
                                _this._CONTEXT.stroke();
                                _this._CONTEXT.restore();
                            }
                        });
                    }
                    else if (this.api.round_selected) {
                        this.api.round_selected.Locations.forEach(function (evenement) {
                            _this._CONTEXT.arc(evenement.Location.Pose.X / _this.resomap + _this.offsetX, _this._CANVAS.height -
                                (evenement.Location.Pose.Y / _this.resomap - _this.offsetY), _this._CANVAS.width * 0.01, 0, 2 * Math.PI);
                            if (_this.api.round_selected.Locations[0].Location.Id == evenement.Location.Id) {
                                _this._CONTEXT.fillStyle = "rgb(145,221,145)";
                                _this._CONTEXT.fill();
                                _this._CONTEXT.fillText("Start", evenement.Location.Pose.X / _this.resomap + _this.offsetX - 20, _this._CANVAS.height -
                                    (evenement.Location.Pose.Y / _this.resomap - _this.offsetY) -
                                    10);
                            }
                            _this._CONTEXT.beginPath();
                            _this._CONTEXT.save();
                            _this._CONTEXT.restore();
                        });
                        for (var i = 0; i < this.api.round_selected.Locations.length - 1; i++) {
                            this._CONTEXT.beginPath();
                            if (this.api.round_selected.Locations[i + 1].Speed > 0) {
                                this._CONTEXT.setLineDash([]);
                            }
                            else {
                                this._CONTEXT.setLineDash([5]);
                            }
                            this._CONTEXT.moveTo(this.api.round_selected.Locations[i].Location.Pose.X / this.resomap + this.offsetX, this._CANVAS.height -
                                (this.api.round_selected.Locations[i].Location.Pose.Y / this.resomap - this.offsetY));
                            this._CONTEXT.lineTo(this.api.round_selected.Locations[i + 1].Location.Pose.X / this.resomap + this.offsetX, this._CANVAS.height -
                                (this.api.round_selected.Locations[i + 1].Location.Pose.Y / this.resomap - this.offsetY));
                            this._CONTEXT.strokeStyle = "rgb(145,221,145)";
                            this._CONTEXT.stroke();
                            this._CONTEXT.restore();
                        }
                    }
                }
            }
            // draw robot
            this._CONTEXT.beginPath();
            //this._CONTEXT.save();
            this._CONTEXT.translate(this.api.localization_status.positionx / this.resomap + this.offsetX, this._CANVAS.height -
                this.api.localization_status.positiony / this.resomap +
                this.offsetY);
            this._CONTEXT.rotate(-this.api.localization_status.positiont);
            this._CONTEXT.moveTo(0, -15);
            this._CONTEXT.lineTo(15, 0);
            this._CONTEXT.lineTo(0, 15);
            this._CONTEXT.lineTo(-10, 15);
            this._CONTEXT.lineTo(-10, -15);
            this._CONTEXT.lineTo(0, -15);
            this._CONTEXT.fillStyle = "#32CD32";
            this._CONTEXT.fill();
            this._CONTEXT.closePath();
            this._CONTEXT.lineWidth = 2;
            this._CONTEXT.strokeStyle = "rgb(0,0,0)";
            this._CONTEXT.stroke();
            this._CONTEXT.restore();
            if (this.api.navigation_status.status === 1) {
                console.log("trajectoire reach");
                var traj = this.api.navigation_status.trajectory;
                traj.forEach(function (evenement) {
                    _this._CONTEXT.beginPath();
                    _this._CONTEXT.moveTo(evenement.Start.X / _this.resomap + _this.offsetX, _this._CANVAS.height -
                        (evenement.Start.Y / _this.resomap - _this.offsetY));
                    _this._CONTEXT.lineTo(evenement.End.X / _this.resomap + _this.offsetX, _this._CANVAS.height -
                        (evenement.End.Y / _this.resomap - _this.offsetY));
                    _this._CONTEXT.strokeStyle = "#32CD32";
                    _this._CONTEXT.stroke();
                });
            }
        }
    };
    ;
    ChooseAssistancePage.prototype.getImageFromService = function () {
        var _this = this;
        this.imge = document.getElementById("imgmap");
        if (this.api.id_current_map != undefined) {
            this.offsetX = this.api.mapdata.Offset.X;
            this.offsetY = this.api.mapdata.Offset.Y;
            this.resomap = this.api.mapdata.Resolution;
            this.initialzoom = this.resomap;
        }
        //console.log("GetImageFromService");
        this.api.getImgMapHttp().subscribe(function (data) {
            //console.log(data);
            _this.api.imgMap = data;
            _this.createImageFromBlob();
            console.log("EndcreateImageFromBlob");
        }, function (error) {
            console.log(error);
        });
    };
    // config la variable constraints
    ChooseAssistancePage.prototype.setupconstraint = function () {
        var _this = this;
        // demande de permission camera
        navigator.mediaDevices.getUserMedia(this.constraints);
        // verification des permissions video et de la disponibilité de devices
        if (!!(navigator.mediaDevices && navigator.mediaDevices.getUserMedia)) {
            // enumeration des appareils connectes pour filtrer
            navigator.mediaDevices.enumerateDevices().then(function (result) {
                var constraints;
                console.log(result);
                result.forEach(function (device) {
                    // filtrer pour garder les flux video
                    if (device.kind.includes("videoinput")) {
                        // filtrer le label de la camera
                        if (device.label.includes("USB")) {
                            constraints = {
                                audio: false,
                                //video: {deviceId: "f9860b260e18d7fd1687d8567e21b49c4bb26f87606020741717e11f6179f409"},
                                // affecter le deviceid filtré
                                video: { deviceId: device.deviceId },
                                facingMode: "environment",
                                width: { ideal: 1920 },
                                height: { ideal: 1080 },
                            };
                        }
                    }
                });
                // on lance la connexion de la vidéo
                _this.startCamera(constraints);
            });
        }
        else {
            alert("Sorry, camera not available.");
        }
    };
    // function launch connection camera
    ChooseAssistancePage.prototype.startCamera = function (cs) {
        if (!!(navigator.mediaDevices && navigator.mediaDevices.getUserMedia)) {
            // recupération du flux avec les constraints configurés puis on attache le flux à la balise video
            navigator.mediaDevices
                .getUserMedia(cs)
                .then(this.attachVideo.bind(this))
                .catch(this.handleError);
        }
        else {
            alert("Sorry, camera not available.");
        }
    };
    ChooseAssistancePage.prototype.handleError = function (error) {
        console.log("Error: ", error);
    };
    // function qui attache le flux video à la balise video
    ChooseAssistancePage.prototype.attachVideo = function (stream) {
        var _this = this;
        this.renderer.setProperty(this.videoElement.nativeElement, "srcObject", stream);
        this.renderer.listen(this.videoElement.nativeElement, "play", function (event) {
            _this.videoHeight = _this.videoElement.nativeElement.videoHeight;
            _this.videoWidth = _this.videoElement.nativeElement.videoWidth;
        });
    };
    // function take snapshot and send mail
    ChooseAssistancePage.prototype.captureBlocked = function () {
        this.setupconstraint();
        this.renderer.setProperty(this.canvas.nativeElement, "width", this.videoWidth);
        this.renderer.setProperty(this.canvas.nativeElement, "height", this.videoHeight);
        this.canvas.nativeElement
            .getContext("2d")
            .drawImage(this.videoElement.nativeElement, 0, 0);
        var url = this.canvas.nativeElement.toDataURL();
        console.log(url);
        this.alert.Blocked_c(url);
    };
    ChooseAssistancePage.prototype.onRoundChange = function () {
        var _this = this;
        this.api.cptpoi = 1;
        console.log(this.api.selectRound);
        this.api.round_selected = this.api.rounds_current_map.filter(function (round) { return round.Id == _this.api.selectRound; })[0];
        console.log(this.api.round_selected);
        this.param.tablewalklogs.walk_round = this.api.round_selected.Name;
        console.log(this.api.round_selected.Locations.length);
        this.createImageFromBlob();
    };
    ChooseAssistancePage.prototype.onUserChange = function (event) {
        this.param.tablewalklogs.walkuser_id = event;
        //console.log(this.api.username);
    };
    ChooseAssistancePage.prototype.getDistance = function (x1, y1, x2, y2) {
        var y = x2 - x1;
        var x = y2 - y1;
        var res = Math.sqrt(x * x + y * y);
        console.log(res);
        return res;
    };
    ChooseAssistancePage.prototype.onSliderRelease = function (ev, id) {
        ev.preventDefault();
        id.open();
        this.createImageFromBlob();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("canvas"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], ChooseAssistancePage.prototype, "canvasEl", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("imgmap"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], ChooseAssistancePage.prototype, "imgmapElement", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("video"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], ChooseAssistancePage.prototype, "videoElement", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("canvas"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], ChooseAssistancePage.prototype, "canvas", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("select1"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Select */])
    ], ChooseAssistancePage.prototype, "select1", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("select2"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Select */])
    ], ChooseAssistancePage.prototype, "select2", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("select3"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Select */])
    ], ChooseAssistancePage.prototype, "select3", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("select4"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Select */])
    ], ChooseAssistancePage.prototype, "select4", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("select5"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Select */])
    ], ChooseAssistancePage.prototype, "select5", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("select6"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Select */])
    ], ChooseAssistancePage.prototype, "select6", void 0);
    ChooseAssistancePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: "page-chooseassistance",template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\guidedwalk\src\pages\choose_assistance\choose_assistance.html"*/'<!-- mobility support html template -->\n\n<ion-header no-border>\n\n  <headpage pageName="choose_assistance"></headpage>\n\n</ion-header>\n\n\n\n<ion-content padding id="body">\n\n  <ion-card text-center class="big_card">\n\n    <ion-grid no-padding class="heightstyle">\n\n      <ion-row class="heightstyle" id="firstrow">\n\n\n\n        <ion-col col-3>\n\n          <ion-item class="itemselectuser">\n\n            <ion-label fixed style=" color:white; margin-left: 10px;">{{this.param.datatext.user}}</ion-label>\n\n            <ion-select disabled={{api.recoface}} #select1 okText={{this.param.datatext.btn_ok}} cancelText={{this.param.datatext.btn_cancel}}\n\n              class="myCustomSelect" [selectOptions]="selectOptionuser" [(ngModel)]="api.selectUser"\n\n              placeholder={{this.param.datatext.chooseuser}} (ionChange)="onUserChange($event)" (mouseup)="onSliderRelease($event,select1)">\n\n              <ion-option value={{param.datatext.anonymous}}>{{param.datatext.anonymous}}</ion-option>\n\n              <ion-option *ngFor="let m of this.param.walkuser" value="{{m.username}}">{{m.username}}</ion-option>\n\n            </ion-select>\n\n          </ion-item>\n\n          <ion-item class="itemselectspeed">\n\n            <ion-label fixed style=" color:white; margin-left: 10px;">{{param.datatext.speed}}</ion-label>\n\n            <ion-select #select2 okText={{this.param.datatext.btn_ok}} cancelText={{this.param.datatext.btn_cancel}}\n\n              class="myCustomSelectPOI" [selectOptions]="selectOptionspeed" [(ngModel)]="api.selectSpeed"\n\n              placeholder={{param.datatext.choose_speed}} (mouseup)="onSliderRelease($event,select2)">\n\n              <ion-option [value]=0.1>10 cm/s</ion-option>\n\n              <ion-option [value]=0.2>20 cm/s</ion-option>\n\n              <ion-option [value]=0.3>30 cm/s</ion-option>\n\n              <ion-option [value]=0.4>40 cm/s</ion-option>\n\n              <ion-option [value]=0.5>50 cm/s</ion-option>\n\n              <ion-option [value]=0.6>60 cm/s</ion-option>\n\n              <ion-option [value]=0.7>70 cm/s</ion-option>\n\n            </ion-select>\n\n          </ion-item>\n\n\n\n          <ion-item class="itemselectspeed">\n\n            <ion-label fixed style=" color:white; margin-left: 10px;">{{param.datatext.walkzone}}</ion-label>\n\n            <ion-select #select3 okText={{this.param.datatext.btn_ok}} cancelText={{this.param.datatext.btn_cancel}}\n\n              class="myCustomSelectPOI" [selectOptions]="selectOptionsWalkZone" [(ngModel)]="api.selectWalkZone"\n\n              placeholder={{param.datatext.choose_walkzone}} (mouseup)="onSliderRelease($event,select3)">\n\n              <ion-option [value]=0.4>40 cm</ion-option>\n\n              <ion-option [value]=0.45>45 cm</ion-option>\n\n              <ion-option [value]=0.5>50 cm</ion-option>\n\n              <ion-option [value]=0.55>55 cm</ion-option>\n\n              <ion-option [value]=0.6>60 cm</ion-option>\n\n              <ion-option [value]=0.65>65 cm</ion-option>\n\n              <ion-option [value]=0.70>70 cm</ion-option>\n\n              <ion-option [value]=0.75>75 cm</ion-option>\n\n              <ion-option [value]=0.80>80 cm</ion-option>\n\n            </ion-select>\n\n          </ion-item>\n\n          <ion-item class="itemselectmode">\n\n            <ion-label fixed style=" color:white; margin-left: 10px;">{{this.param.datatext.mode}}</ion-label>\n\n            <ion-select #select4 okText={{this.param.datatext.btn_ok}} cancelText={{this.param.datatext.btn_cancel}}\n\n              class="myCustomSelectPOI" [selectOptions]="selectOptionmode" [(ngModel)]="api.selectMode"\n\n              placeholder={{this.param.datatext.choosemode}} (mouseup)="onSliderRelease($event,select4)">\n\n              <ion-option value="round">{{this.param.datatext.round}}</ion-option>\n\n              <ion-option value="destination">{{this.param.datatext.destination}}</ion-option>\n\n\n\n\n\n            </ion-select>\n\n          </ion-item>\n\n          <ion-item class="itemselectpoi" *ngIf="api.selectMode===\'destination\'">\n\n            <ion-label fixed style=" color:white; margin-left: 10px;">PoI</ion-label>\n\n            <ion-select #select5 okText={{this.param.datatext.btn_ok}} cancelText={{this.param.datatext.btn_cancel}}\n\n              class="myCustomSelectPOI" [selectOptions]="selectOptionpoi" [(ngModel)]="api.selectDestination"\n\n              placeholder={{this.param.datatext.choosepoi}} (mouseup)="onSliderRelease($event,select5)">\n\n              <ion-option *ngFor="let m of this.api.all_locations" value="{{m.Name}}">{{m.Id}}. {{m.Name}}</ion-option>\n\n\n\n            </ion-select>\n\n          </ion-item>\n\n          <ion-item class="itemselectround" *ngIf="api.selectMode===\'round\'">\n\n            <ion-label fixed style=" color:white; margin-left: 10px;">{{this.param.datatext.round}}</ion-label>\n\n            <ion-select #select6 okText={{this.param.datatext.btn_ok}} cancelText={{this.param.datatext.btn_cancel}}\n\n              class="myCustomSelectPOI" [selectOptions]="selectOptionround" [(ngModel)]="api.selectRound"\n\n              placeholder={{this.param.datatext.chooseexercice}} (ionChange)="onRoundChange()" (mouseup)="onSliderRelease($event,select6)">\n\n              <ion-option *ngFor="let m of this.api.rounds_current_map" value="{{m.Id}}">{{m.Name}}</ion-option>\n\n\n\n            </ion-select>\n\n          </ion-item>\n\n          <!-- the Go button is disabled if the robot is not connected or if it detects no one -->\n\n          <button *ngIf="api.selectMode===\'destination\' && !api.towardDocking" (mouseup)="startAssistance_page($event)"\n\n            [disabled]=" !api.is_connected || !api.selectDestination" class="btnwalk">\n\n            <ion-icon color="light" name="walk" class="icon_style">\n\n              <p class="legende">\n\n                {{this.param.datatext.btn_walk}}\n\n              </p>\n\n            </ion-icon>\n\n          </button>\n\n          <button *ngIf="api.selectMode===\'round\' && !api.towardDocking" (mouseup)="startAssistance_page($event)"\n\n            [disabled]="!api.is_connected || !api.selectRound" class="btnwalk">\n\n            <ion-icon color="light" name="walk" class="icon_style">\n\n              <p class="legende">\n\n                {{this.param.datatext.btn_walk}}\n\n              </p>\n\n            </ion-icon>\n\n          </button>\n\n\n\n\n\n        </ion-col>\n\n\n\n        <ion-col no-padding class="colmeter">\n\n          <ion-row class="heightstyle90">\n\n            <ion-col col-12 id="testcol" class="heightstyle">\n\n                <canvas #canvas (wheel)="on_wheel($event);"\n\n                (mouseup)="on_mouse_up($event);" (mousedown)="on_mouse_down($event);" (mousemove)="on_mouse_move($event);"></canvas>\n\n                <img id="imgmap" #imgmap src={{map_url}} crossorigin="anonymous" class="video" style="display:none;" />\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row class="heightstyle10">\n\n            <ion-col col-1>\n\n              <button class="btnmap" (mouseup)="on_zoom_out($event)">\n\n                <ion-icon name="search"></ion-icon> -\n\n              </button>\n\n            </ion-col>\n\n            <ion-col col-1>\n\n              <button (mouseup)="on_zoom_in($event)" class="btnmap">\n\n                <ion-icon name="search"></ion-icon> +\n\n              </button>\n\n            </ion-col>\n\n            <ion-col col-4>\n\n              <button class="btnmapinfo">\n\n                {{this.param.datatext.map}} : {{api.mapdata.Name}}\n\n              </button>\n\n\n\n            </ion-col>\n\n\n\n          </ion-row>\n\n        </ion-col>\n\n\n\n      </ion-row>\n\n\n\n    </ion-grid>\n\n  </ion-card>\n\n</ion-content>'/*ion-inline-end:"C:\Users\laele\Documents\KomApp\guidedwalk\src\pages\choose_assistance\choose_assistance.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_6__services_speech_service__["a" /* SpeechService */],
            __WEBPACK_IMPORTED_MODULE_4__services_popup_service__["a" /* PopupService */],
            __WEBPACK_IMPORTED_MODULE_3__services_alert_service__["a" /* AlertService */],
            __WEBPACK_IMPORTED_MODULE_5__services_param_service__["a" /* ParamService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__services_api_service__["a" /* ApiService */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["W" /* Renderer2 */]])
    ], ChooseAssistancePage);
    return ChooseAssistancePage;
}());

//# sourceMappingURL=choose_assistance.js.map

/***/ })

},[242]);
//# sourceMappingURL=main.js.map