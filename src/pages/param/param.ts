import { Component, OnInit } from '@angular/core';
import { NavController} from 'ionic-angular';
import { ApiService } from '../../services/api.service';
import { MailPage } from '../../pages/mail/mail';
import { SMSPage } from '../sms/sms';
import { LanguePage } from '../langue/langue';
import { ParamService } from '../../services/param.service';
import { PasswordPage } from '../password/password';
import { UserPage } from '../user/user';


@Component({
  selector: 'page-param',
  templateUrl: 'param.html'
})

export class ParamPage implements OnInit {

  mailPage = MailPage;
  languePage=LanguePage;
  smsPage=SMSPage;
  passwordPage=PasswordPage;
  userPage=UserPage;
  ngOnInit() {

  }



  constructor(public navCtrl: NavController, public api:ApiService, public param:ParamService ) {
  

  }

goMail(ev){
  ev.preventDefault();
  this.navCtrl.push(this.mailPage);
}
goSms(ev){
  ev.preventDefault();
  this.navCtrl.push(this.smsPage);
}
goLangue(ev){
  ev.preventDefault();
  this.navCtrl.push(this.languePage);
}
goPassword(ev){
  ev.preventDefault();
  this.navCtrl.push(this.passwordPage);
}
goUser(ev){
  ev.preventDefault();
  this.navCtrl.push(this.userPage);
}

}
