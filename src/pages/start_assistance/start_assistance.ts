import { Component, OnInit } from "@angular/core";
import { NavController } from "ionic-angular";
import { ApiService } from "../../services/api.service";
import { AlertService } from "../../services/alert.service";
import { PopupService } from "../../services/popup.service";
import { ParamService } from "../../services/param.service";
import { SpeechService } from "../../services/speech.service";
import { WalkPage } from '../walk/walk';

@Component({
  selector: "page-startassistance",
  templateUrl: "start_assistance.html",
})
export class StartAssistancePage implements OnInit {
 
  intervalcountdown: any;
  timecountdown: number; /// in sec

  firstcount: boolean;
  videoWidth = 0;
  videoHeight = 0;

  updateinterv: any;
  speechinterv:any;

  walkPage = WalkPage;


  constructor(
    public speech: SpeechService,
    public popup: PopupService,
    public alert: AlertService,
    public param: ParamService,
    public navCtrl: NavController,
    public api: ApiService,

  ) {
    this.api.metercovered = 0;
    this.updateinterv = setInterval(() => this.getUpdate(), 500);
    this.speechinterv = setInterval(() => this.sayorder(), 5000);
    this.api.inUse = true;
    this.timecountdown = 3;
    this.speech.getVoice();
    // declecnche la popup de permission camera si besoin
  
    this.firstcount = false;

  }


  ngOnInit() {
   
  
  }

  ionViewWillLeave() {
    clearInterval(this.updateinterv);
    clearInterval(this.speechinterv);
  }

  ionViewDidEnter() {
    this.timecountdown = 3;
  }

  sayorder(){
    if(this.api.position_state <2){
      this.speech.speak(this.param.datatext.closer);
    }
   
  }

  getUpdate() {
  
    if(!this.api.walker_states.detected){
      this.api.position_state=0;
      if (this.firstcount) {
        clearInterval(this.intervalcountdown);
        this.firstcount = false;
      }
      if(this.api.walker_states.status!=1){
        //console.log("arreter marche******");
        this.api.walkercmdstartdetect();
      } 
    }
    else{
      if(this.api.walker_states.person["X"]>this.api.selectWalkZone){
        this.api.position_state=1;
        if (this.firstcount) {
          clearInterval(this.intervalcountdown);
          this.firstcount = false;
        }
      }
      else{
        this.api.position_state=2;
        if (!this.firstcount) {
          this.timecountdown = 3;
          this.firstcount = true;
          this.intervalcountdown = setInterval(
            () => this.updateCountdown(),
            1300
          );
          
        }
      }
      
    }
    
  }

  
  updateCountdown() {

    if (this.api.position_state===2) { // goodpose
      if (this.timecountdown > 0) {
        this.speech.speak(this.timecountdown.toString());
        this.api.okToast(this.timecountdown.toString(), 1300);
      } else if (this.timecountdown == 0) {
        this.api.okToast(this.param.datatext.btn_walk, 2000);
        this.speech.speak(this.param.datatext.walk);
      } else {
        clearInterval(this.intervalcountdown);
        if(this.api.selectMode==="destination"){ 
          this.api.reachHttp(this.api.selectDestination);
          this.api.walkercmdstart(this.api.selectSpeed);
         
        }else{
          //console.log("moderonde");
          var x2=this.api.round_selected.Locations[this.api.cptpoi].Location.Pose.X;
          var y2=this.api.round_selected.Locations[this.api.cptpoi].Location.Pose.Y;
          var x1=this.api.localization_status.positionx;
          var y1=this.api.localization_status.positiony;
          this.api.puttrajectory(x1,y1,x2,y2);
          this.api.walkercmdstart(this.api.selectSpeed);
        }
        if(!this.api.resume){
          this.Walk_page();
          this.param.tablewalklogs.walk_time = 0;
          this.param.tablewalklogs.walk_timeofpauses = 0;
          if(this.api.selectMode==="round"){
            console.log("addwalkdata");
            this.param.addwalklog(this.api.name_current_map,this.api.selectSpeed);
          }

        }
        else{
          this.navCtrl.pop();
        }
      }
    }//badpose
    if (this.timecountdown < -1) {
      clearInterval(this.intervalcountdown);
    }
    this.timecountdown--;
  }


  onclickstop(ev){
    ev.preventDefault();
    this.api.inUse=false;
    clearInterval(this.intervalcountdown);
    this.api.walkercmdstartdetect();
    this.navCtrl.pop();
  }


  
  Walk_page(){
    this.navCtrl.push(this.walkPage);
  }


}
