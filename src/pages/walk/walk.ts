import { Component, OnInit, ViewChild} from "@angular/core";
import { NavController } from "ionic-angular";
import { ApiService } from "../../services/api.service";
import { AlertService } from "../../services/alert.service";
import { PopupService } from "../../services/popup.service";
import { ParamService } from "../../services/param.service";
import { SpeechService } from "../../services/speech.service";
import { StartAssistancePage } from '../start_assistance/start_assistance';
import { Select } from "ionic-angular";


@Component({
  selector: "page-walk",
  templateUrl: "walk.html",
})
export class WalkPage implements OnInit {
  arrowinfo:string="f";
  direction:string="f";
  speechcpt:number=0;
  detectobstacle:boolean=false;
  updateinterv: any;
  updatetimes: any;
  speechinterv:any;
  walkPage = WalkPage;
  selectOptionspeed:any;
  startPage = StartAssistancePage;
  finish:boolean;
  is_sendmail:boolean;
  selectOptionsWalkZone:any;
  cptdetected:number=0;
  onpause:boolean;
  acknowledge:boolean=false;
  donttalk:boolean=false;

  @ViewChild("select1") select1: Select;
  @ViewChild("select2") select2: Select;

  constructor(
    public speech: SpeechService,
    public popup: PopupService,
    public alert: AlertService,
    public param: ParamService,
    public navCtrl: NavController,
    public api: ApiService,


  ) {
    this.api.metercovered = 0;
    this.updateinterv = setInterval(() => this.getUpdate(), 500);
    this.updatetimes=setInterval(() => this.getUpdatetimes(), 1000);
    this.speechinterv = setInterval(() => this.sayorder(), 500);
    this.api.inUse = true;
    this.speech.getVoice();
    // declecnche la popup de permission camera si besoin
    this.finish=false;

    this.selectOptionspeed = {
      title: this.param.datatext.choose_speed,
    };
    this.selectOptionsWalkZone = {
      title: this.param.datatext.choose_walkzone,
    };
    this.is_sendmail = false;

  }


  ngOnInit() {

  }

  ionViewWillLeave() {
  }

  ionViewDidEnter() {
   this.onpause=false;
   this.donttalk=false;
  }

  onlyClick_Pause(ev) {
    // Call preventDefault() to prevent any further handling
    ev.preventDefault();
    this.btnpause();
    this.speech.speak(this.param.datatext.wantstop);
  }

  onlyClick_Stop(ev) {
    // Call preventDefault() to prevent any further handling
    ev.preventDefault();
    this.btnstop();

  }

  getDistance(x1, y1, x2, y2){
    let y = x2 - x1;
    let x = y2 - y1;
    let res = Math.sqrt(x * x + y * y);
    //console.log(res);
    return res;
  }

  sayorder(){
    if(this.api.walker_states.status===3){
      this.speechcpt=this.speechcpt+1;
      if(this.api.anticollision_status.forward===0){
        this.detectobstacle=false;
      }

      if(this.api.selectMode==="round"){
      var x1=this.api.round_selected.Locations[this.api.cptpoi].Location.Pose.X;
      var y1=this.api.round_selected.Locations[this.api.cptpoi].Location.Pose.Y;
      var x2=this.api.localization_status.positionx;
      var y2=this.api.localization_status.positiony;
      var dist = this.getDistance(x1,y1,x2,y2);

      if(this.api.position_state===2 && dist<1.4 && dist>1.1 && !this.donttalk){
        this.speech.speak(this.param.datatext.slowdown);
        this.donttalk=true;
      }

      }
      if(this.api.anticollision_status.forward===2 && !this.detectobstacle){
        this.detectobstacle=true;
        this.speechcpt=0;
        this.speech.speak(this.param.datatext.obstacle);
        this.api.okToast(this.param.datatext.obstacle, 4000);
      }
      else if(this.api.anticollision_status.forward===2 && this.speechcpt>20){
        this.detectobstacle=false;
      }
      else if(this.arrowinfo==="f" && this.direction!=="f"){
        this.speechcpt=0;
        this.direction="f";
        this.speech.speak(this.param.datatext.gof);
        this.donttalk=false;

      }
      else if(this.arrowinfo==="l" && this.direction!=="l"){
        this.speechcpt=0;
        this.direction="l";
        this.speech.speak(this.param.datatext.gol);
        this.donttalk=false;
      }
      else if(this.arrowinfo==="r" && this.direction!=="r"){
        this.speechcpt=0;
        this.direction="r";
        this.speech.speak(this.param.datatext.gor);
        this.donttalk=false;
      }

    }

  }

  getUpdatetimes(){
    //console.log(this.param.tablewalklogs.walk_time);
    if(!this.finish){
      this.update_walktimeofpauses();
      this.update_walktime();
    }

  }

  sendHelp(ev) {
    ev.preventDefault();
    this.popup.askHelp();
    this.btnpause();
    // change the color and logo of the box
    if (!this.is_sendmail) {
      this.is_sendmail = true;
      // for (let num of this.param.phonenumberlist) {
      //   this.alert.sendsmsperso(this.param.datatext.smsSOS, num);
      // }

      // capture l'image et l'envoie via mail
      this.alert.SOS_c();
      this.alert.mobAssistSOS(this.api.mailAddInformationWalk());

      setTimeout(() => {
        this.is_sendmail = false;
      }, 15000);
    }
  }

  getUpdate() {
   this.getDetect();
   this.getMeter()
  }


  btnpause(){
    this.onpause=true;
    if(this.api.selectMode==="destination"){
      this.api.walkercmdstartdetect();
      console.log("pause abortnav");
      this.api.abortNavHttp();
    }
    else{
      this.api.walkercmdstartdetect();
      //this.api.abortNavHttp();
      this.param.tablewalklogs.walk_speed=this.api.selectSpeed;
      this.param.tablewalklogs.walk_meters=this.api.metercovered;
      this.param.updatewalklog();
     
    }

    if(!this.finish){
        this.param.tablewalklogs.walk_numberofpauses++;
      }

  }

  btnresume(ev){
    ev.preventDefault();
    this.api.inUse=true;
    this.api.resume=true;
    this.navCtrl.push(this.startPage);
    this.api.walkercmdstartdetect();
  }

  btnstop(){
    this.api.cptpoi=1;
    this.api.resume=false;
    this.api.inUse=false;
    this.api.walkercmdstartdetect();
    console.log("stop abortnav");
    this.api.abortNavHttp();
    this.navCtrl.popToRoot();
    //this.param.senddatawalking();
    clearInterval(this.updateinterv);
    clearInterval(this.updatetimes);
  }

  update_walktime(){
    if(this.api.walker_states.status===3){
      this.param.tablewalklogs.walk_time++;

      if(this.param.tablewalklogs.walk_time >=15 && this.param.tablewalklogs.walk_time%15===0){
        console.log("add emotion");
        this.param.addEmotion(this.api.emotion_status.sad,this.api.emotion_status.happy, this.api.emotion_status.anger, this.api.emotion_status.surprise, this.api.emotion_status.neutral);
      }
    }

  }

  update_walktimeofpauses(){
    if(this.api.walker_states.status!=3){
      this.onpause=true;
      this.param.tablewalklogs.walk_timeofpauses++;
    }
  }

  getMeter() {
    if (this.api.traveldebut > 0) {
      this.api.metercovered =
        this.api.statistics_status.totalDistance - this.api.traveldebut;
    } else {
      this.api.traveldebut = this.api.statistics_status.totalDistance;
    }
  }


  getDetect() {

    if(this.api.walker_states.status===3){
      if(this.api.walker_states.person["X"] > this.api.walker_states.activationDistance || !this.api.walker_states.detected){
        this.api.position_state=1;//bad
      }
      else{
        this.api.position_state=2;//in walkzone
      }

      if(!this.api.walker_states.detected){
        this.cptdetected=this.cptdetected+1;
      }
      else{
        this.cptdetected=0;
      }

      if(this.api.trajectory_status.Status===3){
        this.acknowledge=false;
      }
    }

    if(this.api.walker_states.targetAngularSpeed<0.28 && this.api.walker_states.targetAngularSpeed>-0.28){
      this.arrowinfo="f";
    }
    else if(this.api.walker_states.targetAngularSpeed>=0.28){
      this.arrowinfo="l";
    }
    else{
      this.arrowinfo="r";
    }


    // if smart bouton is pressed or the bouton A of the gamepad is pressed
    if (
      this.cptdetected>6 ||
      this.api.btnPush ||
      !this.api.is_connected
    ) {
      if (this.api.walker_states.status===3 && this.param.tablewalklogs.walk_time>2) {
        console.log("pause à cause du bouton malin");
        this.btnpause();
        this.cptdetected=0;

      }

      this.api.btnPush = false;
    }


    if((this.api.selectMode==="destination" && this.api.navigation_status.status === 5)){
      this.btnstop();
      this.popup.errorNavAlert();
      this.alert.naverror(this.api.mailAddInformationBasic());
    }
    else if(this.api.selectMode==="round" && (this.api.round_selected.Locations.length>(this.api.cptpoi+1)) && !this.onpause && (this.api.trajectory_status.Status<2) && !this.acknowledge){
     //go next poi
     console.log("next poi");
     console.log(this.api.cptpoi);
     this.acknowledge=true;
     this.api.cptpoi=this.api.cptpoi+1;

     var x2=this.api.round_selected.Locations[this.api.cptpoi].Location.Pose.X;
     var y2=this.api.round_selected.Locations[this.api.cptpoi].Location.Pose.Y;
     var x1=this.api.localization_status.positionx;
     var y1=this.api.localization_status.positiony;
     this.api.puttrajectory(x1,y1,x2,y2);

    }
    else if(this.api.selectMode==="round" && (this.api.trajectory_status.Status<2) && (this.api.round_selected.Locations.length-1) === this.api.cptpoi && !this.finish){
      this.finish=true
      this.api.okToast(this.param.datatext.bravo, 3000);
      this.speech.speak(this.param.datatext.finishexo);

      this.btnpause();
    }
    else if(this.api.selectMode==="destination" && this.api.navigation_status.status === 2 && this.param.tablewalklogs.walk_time>3 && !this.finish){
      this.finish=true
      this.api.okToast(this.param.datatext.bravo, 3000);
      this.speech.speak(this.param.datatext.atdestination);
      this.btnpause();
    }
  }

  onSliderRelease(ev, id: Select){
    ev.preventDefault();
    id.open();
  }
}
