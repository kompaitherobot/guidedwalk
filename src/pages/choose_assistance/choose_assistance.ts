import { Component, ViewChild, ElementRef, OnInit, Renderer2 } from "@angular/core";
import { NavController } from "ionic-angular";
import { ApiService } from "../../services/api.service";
import { AlertService } from "../../services/alert.service";
import { PopupService } from "../../services/popup.service";
import { ParamService } from "../../services/param.service";
import { SpeechService } from "../../services/speech.service";
import { StartAssistancePage } from '../start_assistance/start_assistance';
import { Select } from "ionic-angular";

@Component({
  selector: "page-chooseassistance",
  templateUrl: "choose_assistance.html",
})
export class ChooseAssistancePage implements OnInit {
  @ViewChild("canvas") canvasEl: ElementRef;
  @ViewChild("imgmap") imgmapElement: ElementRef;
  updateMeter: any;
  cpt_locked: number; // cpt to know if kompai is blocked
  is_blocked: boolean;
  cptduration: any;
  is_sendmail: boolean;
  @ViewChild("video") videoElement: ElementRef;
  @ViewChild("canvas") canvas: ElementRef;
  videoWidth = 0;
  videoHeight = 0;
  selectOptionspeed: any;
  selectOptionpoi: any;
  selectOptionmode: any;
  selectOptionuser: any;
  selectOptionround: any;
  selectOptionsWalkZone:any;
  monitor: any;

  private _CANVAS: any;
  initialzoom: any;
  private heightcol: any;
  private widthcol: any;
  private _CONTEXT: any;

  imageToShow: any;
  previousimage: any;
  isImageLoading: boolean;
  rawImageToShow: any;
  loc: any;
  intervmap: any;
  intervgodocking: any;
  offsetX: any;
  offsetY: any;
  resomap: any;

  moving: any;
  start: any = {
    x: 0,
    y: 0,
  };

  clickpose: any = {
    x: 0,
    y: 0,
  };

  xtest: number;
  ytest: number;
  poi: any;
  startPage = StartAssistancePage;

  map_url="http://" + this.param.localhost + "/api/maps/current/image";
  imge:any;
  

  @ViewChild("select1") select1: Select;
  @ViewChild("select2") select2: Select;
  @ViewChild("select3") select3: Select;
  @ViewChild("select4") select4: Select;
  @ViewChild("select5") select5: Select;
  @ViewChild("select6") select6: Select;

  constructor(
    public speech: SpeechService,
    public popup: PopupService,
    public alert: AlertService,
    public param: ParamService,
    public navCtrl: NavController,
    public api: ApiService,
    private renderer: Renderer2

  ) {
    this.api.metercovered = 0;
    this.updateMeter = setInterval(() => this.getUpdate(), 500); // update meter and walker detection every 1/2 secondes
    this.cptduration = setInterval(() => this.mail(), 120000); // toutes les 2 min actualise time
    this.api.inUse = false;
    this.cpt_locked = 0;
    this.is_blocked = false;

    this.speech.getVoice();
    // declecnche la popup de permission camera si besoin
    //this.setupconstraint();

    this.selectOptionspeed = {
      title: this.param.datatext.choose_speed,
    };
    this.selectOptionpoi = {
      title: this.param.datatext.choosepoi,
    };
    this.selectOptionround = {
      title: this.param.datatext.chooseexercice,
    };
    this.selectOptionuser = {
      title: this.param.datatext.chooseuser,
    };
    this.selectOptionmode = {
      title: this.param.datatext.choosemode,
    };
    this.selectOptionsWalkZone = {
      title: this.param.datatext.choose_walkzone,
    };
    this.api.getCurrentLocations();
    this.api.walkercmdstartdetect();
  }

  mail(){
    this.param.cptDuration(); //update duration
    if(this.param.durationNS.length > 1){
      if(this.api.wifiok){
        this.alert.duration(this.param.durationNS[0],this.api.mailAddInformationBasic());
      }
    }
  }

  ngOnInit() {
    this._CANVAS = this.canvasEl.nativeElement;
    this._CONTEXT = this._CANVAS.getContext("2d");
    this._CANVAS.style.width = "100%";
    this._CANVAS.style.height = "100%";
    this.heightcol =
      document.getElementById("body").clientHeight -
      document.getElementById("firstrow").clientHeight * 2;
    this.widthcol = document.getElementById("testcol").clientWidth;
    this.param.tablewalklogs.walkuser_id = this.api.selectUser;

    setTimeout(() => { 
      this.speech.speak(this.param.datatext.chosenguidedmode);
    }, 1000);
    setTimeout(() => { 
      if(!this.api.inUse){
        this.speech.speak(this.param.datatext.letsstart);
      }
    }, 20000);
  }

  updatemap() {

    if (this.api.differential_status.CurrentAngularSpeed > 0.005 || this.api.differential_status.CurrentLinearSpeed > 0.01 || this._CANVAS.width==0) {
        this.createImageFromBlob();
    }

}

  startAssistance_page(ev) {
    ev.preventDefault();
    this.speech.synth.cancel();
    if(this.api.selectMode==="round"){
      var x1=this.api.round_selected.Locations[0].Location.Pose.X;
      var y1=this.api.round_selected.Locations[0].Location.Pose.Y;
      var x2=this.api.localization_status.positionx;
      var y2=this.api.localization_status.positiony;
      if(this.getDistance(x1,y1,x2,y2)<1){
        this.onRefreshMeter();
        this.api.resume = false;
        this.navCtrl.push(this.startPage);
        this.api.walkercmdstartdetect();
      }else{
        //console.log("Placez le robot sur le point de départ");
        this.speech.speak(this.param.datatext.startpoi);
        this.popup.showToast(this.param.datatext.startpoi,3000,"middle");
      }
    }
    else{
      this.onRefreshMeter();
      this.api.resume = false;
      this.navCtrl.push(this.startPage);
      this.api.walkercmdstartdetect();
    } 
  }

  ionViewWillLeave() {
    clearInterval(this.intervmap);
    //clearInterval(this.updateMeter);
    //clearInterval(this.cptduration);
  }

  ionViewDidEnter() {
    this.api.abortNavHttp();
    this.getImageFromService();
    this.intervmap = setInterval(() => this.updatemap(), 400);
    this.api.putStopDetecthttp();
    this.api.cptpoi=1;
  }


  getUpdate() {
    //console.log(this.api.selectSpeed);
    if (this.api.close_app) {
      clearInterval(this.updateMeter);
      clearInterval(this.cptduration);
    }
    //this.updateTrajectory();
    //this.watchIfLocked();
    //this.getDetect();
    //this.getMeter();

    //this.updatedirection();
  }



  watchIfLocked() {
    if (this.api.towardDocking) {
      if (
        this.api.docking_status.status === 2 ||
        this.api.docking_status.status === 3 ||
        (this.api.anticollision_status.forward < 2 &&
          this.api.anticollision_status.right < 2 &&
          this.api.anticollision_status.left < 2)
      ) {
        this.cpt_locked = 0;
        if (this.is_blocked) {
          this.is_blocked = false;
          this.popup.alert_blocked.dismiss();
        }
      } else if (
        this.api.anticollision_status.forward === 2 ||
        this.api.anticollision_status.right === 2 ||
        this.api.anticollision_status.left === 2
      ) {
        this.cpt_locked += 1;
      }
      if (this.cpt_locked > 40 && !this.is_blocked) {
        this.popup.blockedAlert();
        this.is_blocked = true;
        this.alert.blockingdocking(this.api.mailAddInformationBasic());
      } else if (this.cpt_locked === 100 && this.is_blocked) {
        this.captureBlocked();
      }
    }
  }


  getDetect() {
    // if smart bouton is pressed or the bouton A of the gamepad is pressed
    if (
      !this.api.walker_states.detected ||
      this.api.btnPush ||
      !this.api.is_connected
    ) {
      if (this.api.inUse) {
        //start the detection and stop the enslavement of the legs
        this.api.putStopDetecthttp();
        this.api.abortNavHttp();
        // change the color and logo of the box
        this.api.inUse = false;
      }
      if (this.api.towardDocking && this.api.btnPush) {
        this.api.abortNavHttp();
        this.api.towardDocking = false;
        this.api.putStopDetecthttp();
      }
      this.api.btnPush = false;
    }
  }



  updateTrajectory() {

    // listen the round status to allow the robot to go to the next
    //// go docking or go poi in progress
    if (this.api.towardDocking) {
      this.api.appStart = false;
      if (this.api.differential_status.status === 2) {
        // if current is hight
        this.api.towardDocking = false;
        this.api.abortNavHttp();
        this.popup.errorBlocked();
        this.alert.errorblocked(this.api.mailAddInformationBasic());
        this.captureBlocked();
      } else if (this.api.navigation_status.status === 5) {
        // nav error

        this.api.towardDocking = false;
        this.api.abortNavHttp();
        this.popup.errorNavAlert();
        this.alert.naverror(this.api.mailAddInformationBasic());
      } else if (this.api.docking_status.status === 3) {
        this.api.walkercmdstartdetect();
        this.api.towardDocking = false;
        this.alert.charging(this.api.mailAddInformationBasic());
        this.api.close_app = true; //close application
        setTimeout(() => {
          window.close();
        }, 1000);
      } else if (
        this.api.towardDocking &&
        this.api.navigation_status.status === 0 &&
        this.api.docking_status.detected
      ) {
        //connect to the docking when he detect it
        this.api.connectHttp();
      }
    }

  }

  onRefreshMeter() {
    this.api.traveldebut = this.api.statistics_status.totalDistance;
    this.api.metercovered = 0;
  }





  on_mouse_down(evt) {
    if (evt.button == 0) {
      evt.preventDefault();
      this.moving = true;
      this.start.x = evt.x;
      this.start.y = evt.y;

      var x = (evt.offsetX * this._CANVAS.width) / this._CANVAS.clientWidth;

      var y = (evt.offsetY * this._CANVAS.height) / this._CANVAS.clientHeight;
      this.clickpose.x =
        (x - this.offsetX - this._CANVAS.width / 2) * this.resomap;
      this.clickpose.y =
        -(y - this.offsetY - this._CANVAS.height / 2) * this.resomap;


    }
    this.createImageFromBlob();
  }

  on_mouse_up(evt) {
    if (this.moving) {
      var offset = {
        x: this.offsetX,
        y: this.offsetY,
        t: 0,
      };

      offset.x += evt.x - this.start.x;
      offset.y += evt.y - this.start.y;

      this.start.x = evt.x;
      this.start.y = evt.y;

      this.offsetX = offset.x;
      this.offsetY = offset.y;
    }
    this.moving = false;
    this.createImageFromBlob();
  }

  on_mouse_move(evt) {
    if (this.moving) {
      evt.preventDefault();

      var offset = {
        x: this.offsetX,
        y: this.offsetY,
      };

      offset.x += evt.x - this.start.x;
      offset.y += evt.y - this.start.y;

      this.start.x = evt.x;
      this.start.y = evt.y;

      this.offsetX = offset.x;
      this.offsetY = offset.y;
    }
    this.createImageFromBlob();
  }

  on_wheel(evt) {
    evt.preventDefault();


    if (evt.deltaY > 0) {
      if (!(this.resomap >= this.initialzoom)) {
        this.resomap = this.resomap + 150 / 100000;
      } else {
        this.offsetX = 0;
        this.offsetY = 0;
      }
    } else {
      if (this.resomap > this.initialzoom / 2) {
        this.resomap = this.resomap - 150 / 100000;
      }
    }
    this.createImageFromBlob();
  }

  on_zoom_out(ev) {
    ev.preventDefault();
    //zoom -
    if (!(this.resomap >= this.initialzoom)) {
      this.resomap = this.resomap + 150 / 100000;
    } else {
      this.offsetX = 0;
      this.offsetY = 0;
    }
    this.createImageFromBlob();
  }

  on_zoom_in(ev) {
    ev.preventDefault();
    //zoom +
    if (this.resomap > this.initialzoom / 2) {
      this.resomap = this.resomap - 150 / 100000;
    }
    this.createImageFromBlob();
  }


  createImageFromBlob() {
   
      if (this._CANVAS.getContext) {
       
        //console.log(i);

        this._CANVAS.width = this.imge.width;
        this._CANVAS.height = this.imge.height;

        
        if(this._CANVAS.width==0){
          console.log("width 0")
         
       }
       

        var w = this._CANVAS.width;
        var h = this._CANVAS.height;
        var r = this.resomap;
        var x = this.offsetX;
        var y = this.offsetY;

        var xm = this.api.mapdata.Offset.X;
        var ym = this.api.mapdata.Offset.Y;
        var rm = this.api.mapdata.Resolution;
        var wm = this.api.mapdata.Width;
        var hm = this.api.mapdata.Height;

        var dx = xm / r + x;
        var dy = h - (ym + hm * rm) * (1 / r) + y;

        var dw = (wm * rm) / r;
        var dh = (hm * rm) / r;

        if (this.api.id_current_map) {
          this._CONTEXT.drawImage(this.imge, dx, dy, dw, dh);
          
      }
        //console.log('dessin image');

        if (this.api.all_locations != undefined) {

          if (this.api.inUse) { // si l'aide à la marche est active
            console.log("marche en cours");
          } else {
            if (this.api.selectMode === "destination")//mode destination
            {
              this.api.all_locations.forEach((evenement) => {
                if (evenement.Label !== "qr") {
                  this._CONTEXT.beginPath();
                  this._CONTEXT.font =
                    "" + Math.round(this._CANVAS.width * 0.025) + "px Arial";
                  //console.log(evenement);
                  this._CONTEXT.fillStyle = "rgba(26, 156, 195,0.7)";
                  this._CONTEXT.fillText(
                    evenement.Id, // + "- " + evenement.Name,
                    evenement.Pose.X / this.resomap + this.offsetX - 20,
                    this._CANVAS.height -
                    (evenement.Pose.Y / this.resomap - this.offsetY) -
                    10
                  );

                  this._CONTEXT.beginPath();
                  //console.log(evenement.Pose)
                  this._CONTEXT.arc(
                    evenement.Pose.X / this.resomap + this.offsetX,
                    this._CANVAS.height -
                    (evenement.Pose.Y / this.resomap - this.offsetY),
                    this._CANVAS.width * 0.005,
                    0,
                    2 * Math.PI
                  );
                  this._CONTEXT.lineWidth = 1;
                  this._CONTEXT.fillStyle = "rgb(26, 156, 195)";
                  this._CONTEXT.fill();
                  this._CONTEXT.beginPath();
                  this._CONTEXT.save();

                  this._CONTEXT.translate(
                    evenement.Pose.X / this.resomap + this.offsetX,
                    this._CANVAS.height -
                    (evenement.Pose.Y / this.resomap - this.offsetY)
                  );
                  this._CONTEXT.rotate(-evenement.Pose.T);
                  this._CONTEXT.moveTo(0, 0);
                  this._CONTEXT.lineWidth = 2;
                  this._CONTEXT.lineTo(2 * this._CANVAS.width * 0.009, 0);
                  this._CONTEXT.strokeStyle = "rgb(26, 156, 195)";
                  this._CONTEXT.stroke();

                  this._CONTEXT.restore();
                }
              });
            }
            else if (this.api.round_selected) {   //mode ronde
              this.api.round_selected.Locations.forEach((evenement) => {


                this._CONTEXT.arc(
                  evenement.Location.Pose.X / this.resomap + this.offsetX,
                  this._CANVAS.height -
                  (evenement.Location.Pose.Y / this.resomap - this.offsetY),
                  this._CANVAS.width * 0.01,
                  0,
                  2 * Math.PI
                );

                if (this.api.round_selected.Locations[0].Location.Id == evenement.Location.Id) {
                  this._CONTEXT.fillStyle = "rgb(145,221,145)";
                  this._CONTEXT.fill();

                  this._CONTEXT.fillText(
                    "Start",
                    evenement.Location.Pose.X / this.resomap + this.offsetX - 20,
                    this._CANVAS.height -
                      (evenement.Location.Pose.Y / this.resomap - this.offsetY) -
                      10
                  );
                }

                this._CONTEXT.beginPath();
                this._CONTEXT.save();
                this._CONTEXT.restore();

              });
              for (let i = 0; i < this.api.round_selected.Locations.length - 1; i++) { //draw line
                this._CONTEXT.beginPath();
                if (this.api.round_selected.Locations[i + 1].Speed > 0) {
                  this._CONTEXT.setLineDash([]);
                } else {
                  this._CONTEXT.setLineDash([5]);
                }

                this._CONTEXT.moveTo(
                  this.api.round_selected.Locations[i].Location.Pose.X / this.resomap + this.offsetX,
                  this._CANVAS.height -
                  (this.api.round_selected.Locations[i].Location.Pose.Y / this.resomap - this.offsetY)
                );

                this._CONTEXT.lineTo(
                  this.api.round_selected.Locations[i + 1].Location.Pose.X / this.resomap + this.offsetX,
                  this._CANVAS.height -
                  (this.api.round_selected.Locations[i + 1].Location.Pose.Y / this.resomap - this.offsetY)
                );
                this._CONTEXT.strokeStyle = "rgb(145,221,145)";
                this._CONTEXT.stroke();

                this._CONTEXT.restore();

              }
            }
          }

        }
        // draw robot
       
          this._CONTEXT.beginPath();
          
          
          //this._CONTEXT.save();

          this._CONTEXT.translate(
            this.api.localization_status.positionx / this.resomap + this.offsetX,
            this._CANVAS.height -
            this.api.localization_status.positiony / this.resomap +
            this.offsetY
          );
          this._CONTEXT.rotate(-this.api.localization_status.positiont);
          this._CONTEXT.moveTo(0, -15);
          this._CONTEXT.lineTo(15, 0);
          this._CONTEXT.lineTo(0, 15);
          this._CONTEXT.lineTo(-10, 15);
          this._CONTEXT.lineTo(-10, -15);
          this._CONTEXT.lineTo(0, -15);
          this._CONTEXT.fillStyle = "#32CD32";
          this._CONTEXT.fill();
          this._CONTEXT.closePath();
          
          this._CONTEXT.lineWidth = 2;
          this._CONTEXT.strokeStyle = "rgb(0,0,0)";
          this._CONTEXT.stroke();

          this._CONTEXT.restore();
        
        if (this.api.navigation_status.status === 1) { //reach line (navigation)
          console.log("trajectoire reach");
          var traj = this.api.navigation_status.trajectory;
          traj.forEach((evenement) => {
            this._CONTEXT.beginPath();
            this._CONTEXT.moveTo(
              evenement.Start.X / this.resomap + this.offsetX,
              this._CANVAS.height -
              (evenement.Start.Y / this.resomap - this.offsetY)
            );
            this._CONTEXT.lineTo(
              evenement.End.X / this.resomap + this.offsetX,
              this._CANVAS.height -
              (evenement.End.Y / this.resomap - this.offsetY)
            );
            this._CONTEXT.strokeStyle = "#32CD32";
            this._CONTEXT.stroke();
          });
        }

      }
    };
   
  

  getImageFromService() {
    this.imge = document.getElementById("imgmap") as HTMLImageElement;
    if (this.api.id_current_map != undefined) {
      this.offsetX = this.api.mapdata.Offset.X;
      this.offsetY = this.api.mapdata.Offset.Y;
      this.resomap = this.api.mapdata.Resolution;
      this.initialzoom = this.resomap;
    }
    //console.log("GetImageFromService");
    this.api.getImgMapHttp().subscribe(
      (data) => {
        //console.log(data);
        this.api.imgMap = data;
        this.createImageFromBlob();
        console.log("EndcreateImageFromBlob");
      },
      (error) => {
        console.log(error);
      }
    );
  }

  constraints = {
    //audio: false,
    //video: {deviceId: "f9860b260e18d7fd1687d8567e21b49c4bb26f87606020741717e11f6179f409"},
    video: true,

    facingMode: "environment",
    width: { ideal: 1920 },
    height: { ideal: 1080 },
  };

  // config la variable constraints
  setupconstraint() {
    // demande de permission camera
    navigator.mediaDevices.getUserMedia(this.constraints);

    // verification des permissions video et de la disponibilité de devices
    if (!!(navigator.mediaDevices && navigator.mediaDevices.getUserMedia)) {
      // enumeration des appareils connectes pour filtrer
      navigator.mediaDevices.enumerateDevices().then((result) => {
        var constraints;
        console.log(result);

        result.forEach(function (device) {
          // filtrer pour garder les flux video
          if (device.kind.includes("videoinput")) {
            // filtrer le label de la camera
            if (device.label.includes("USB")) {
              constraints = {
                audio: false,
                //video: {deviceId: "f9860b260e18d7fd1687d8567e21b49c4bb26f87606020741717e11f6179f409"},
                // affecter le deviceid filtré
                video: { deviceId: device.deviceId },

                facingMode: "environment",
                width: { ideal: 1920 },
                height: { ideal: 1080 },
              };
            }
          }
        });
        // on lance la connexion de la vidéo
        this.startCamera(constraints);
      });
    } else {
      alert("Sorry, camera not available.");
    }
  }

  // function launch connection camera
  startCamera(cs) {
    if (!!(navigator.mediaDevices && navigator.mediaDevices.getUserMedia)) {
      // recupération du flux avec les constraints configurés puis on attache le flux à la balise video
      navigator.mediaDevices
        .getUserMedia(cs)
        .then(this.attachVideo.bind(this))
        .catch(this.handleError);
    } else {
      alert("Sorry, camera not available.");
    }
  }

  handleError(error) {
    console.log("Error: ", error);
  }

  // function qui attache le flux video à la balise video
  attachVideo(stream) {
    this.renderer.setProperty(
      this.videoElement.nativeElement,
      "srcObject",
      stream
    );
    this.renderer.listen(this.videoElement.nativeElement, "play", (event) => {
      this.videoHeight = this.videoElement.nativeElement.videoHeight;
      this.videoWidth = this.videoElement.nativeElement.videoWidth;
    });
  }

  // function take snapshot and send mail

  captureBlocked() {
    this.setupconstraint();
    this.renderer.setProperty(
      this.canvas.nativeElement,
      "width",
      this.videoWidth
    );
    this.renderer.setProperty(
      this.canvas.nativeElement,
      "height",
      this.videoHeight
    );
    this.canvas.nativeElement
      .getContext("2d")
      .drawImage(this.videoElement.nativeElement, 0, 0);
    var url = this.canvas.nativeElement.toDataURL();
    console.log(url);
    this.alert.Blocked_c(url);
  }

  onRoundChange() {
    this.api.cptpoi=1;
    console.log(this.api.selectRound);
    this.api.round_selected = this.api.rounds_current_map.filter(
      (round) => round.Id == this.api.selectRound
    )[0];
    console.log(this.api.round_selected);
    this.param.tablewalklogs.walk_round = this.api.round_selected.Name

    console.log(this.api.round_selected.Locations.length);
    this.createImageFromBlob();
  }

  onUserChange(event) {
    this.param.tablewalklogs.walkuser_id = event;
    //console.log(this.api.username);

  }

  getDistance(x1, y1, x2, y2){
    let y = x2 - x1;
    let x = y2 - y1;
    let res = Math.sqrt(x * x + y * y);
    console.log(res);
    return res;
  }

  onSliderRelease(ev, id: Select){
    ev.preventDefault();
    id.open();
    this.createImageFromBlob();
  }

}
