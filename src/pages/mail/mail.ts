import { Component, OnInit, ViewChild } from '@angular/core';
import { ParamService } from '../../services/param.service';
import {ToastController, Content} from 'ionic-angular';
import { PopupService } from '../../services/popup.service';

@Component({
  selector: 'page-mail',
  templateUrl: 'mail.html'
})

export class MailPage implements OnInit {

 
  ngOnInit() {
    this.popup.onSomethingHappened2(this.addMail.bind(this));
  }

  inputmail:string;
  @ViewChild("content1") content1: Content;

  constructor(public param:ParamService, public popup: PopupService,public toastCtrl: ToastController ) {

    this.inputmail="";

  } 

  errorToast(m: string){
    const toast=this.toastCtrl.create({
      message:m,
      duration:3000,
      position: 'middle',
      cssClass:"toast"
    });
    toast.present();
  }

  okToast(m: string){
    const toast=this.toastCtrl.create({
      message:m,
      duration:3000,
      position: 'middle',
      cssClass:"toastok"
    });
    toast.present();
  }


  removeMail(ev,m:string){
    ev.preventDefault();
    const index: number = this.param.maillist.indexOf(m);
    if (index !== -1) {
      this.param.maillist.splice(index, 1);
      this.param.deleteMail(m);
      this.okToast(this.param.datatext.mailRemove);
}
  }

 is_a_mail(m:string){
  var mailformat="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$";
  return m.match(mailformat);
 }

  
  addMail(ev)
  {
    ev.preventDefault();
    if(this.is_a_mail(this.inputmail)){
      const index: number = this.param.maillist.indexOf(this.inputmail);
      if (index !== -1) {
        this.errorToast(this.param.datatext.mailexist);}
        else{
          this.param.maillist.push(this.inputmail);
          this.param.addMail(this.inputmail);
          this.okToast(this.param.datatext.mailadd);
          setTimeout(() => {
            this.content1.scrollToBottom();
          },300
          );
        }
    }else{
        this.errorToast(this.param.datatext.mailincorrect);
    }
  }
  update_send_pic(){
    if(this.param.robot.send_pic==1){
      this.param.robot.send_pic=0;
    }else{
      this.param.robot.send_pic=1;
    }
    this.param.updateRobot();
  }

}
