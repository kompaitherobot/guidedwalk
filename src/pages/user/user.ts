import { Component, OnInit, ViewChild } from '@angular/core';
import { ParamService } from '../../services/param.service';
import {ToastController, Content} from 'ionic-angular';
import { PopupService } from '../../services/popup.service';
import { ApiService } from '../../services/api.service';

@Component({
  selector: 'page-user',
  templateUrl: 'user.html'
})

export class UserPage implements OnInit {

    inputuser: string;
    @ViewChild("content1") content1: Content;
 
  ngOnInit() {

  }

  inputmail:string;

  constructor(public param:ParamService, public api: ApiService, public popup: PopupService,public toastCtrl: ToastController ) {

  }

  errorToast(m: string){
    const toast=this.toastCtrl.create({
      message:m,
      duration:3000,
      position: 'middle',
      cssClass:"toast"
    });
    toast.present();
  }

  okToast(m: string){
    const toast=this.toastCtrl.create({
      message:m,
      duration:3000,
      position: 'middle',
      cssClass:"toastok"
    });
    toast.present();
  }

  removeUser(ev,user:string){
    ev.preventDefault();
    const index: number = this.param.users.indexOf(user);
    if (index !== -1) {
      this.param.users.splice(index, 1);
      this.param.deleteUser(user);
      this.okToast(this.param.datatext.mailRemove);
      this.remove_user_from_jetson(user);
      setTimeout(() => {
        this.param.getWalkUser();
      },1000
      );
    }
  }

  remove_user_from_jetson(user:string){
    if(this.api.recoface){
      console.log("delete user jetson");
      this.api.deleteUserJetson(user);
    }
    
  }

  cleantext(text:string){
    text=text.replace(new RegExp("[<>&$§µ£#*_|`¤~(){}°²%/]", "g"),'');
    text=text.replace(new RegExp("[\"]", "g"),'');
    text=text.replace(new RegExp(/\\/, "g"),'');
    text=text.replace(new RegExp(/[\[\]]/, "g"),'');
    return text
  }

  addUser(ev)
  {
    ev.preventDefault();
    this.inputuser=this.cleantext(this.inputuser);
   
    const index: number = this.param.users.indexOf(this.inputuser);
    if (index !== -1) {
      this.errorToast(this.param.datatext.userexist);}
      else{
        this.param.users.push(this.inputuser);
        this.param.addUser(this.inputuser,"","");
        this.okToast(this.param.datatext.useradd);
        setTimeout(() => {
          this.content1.scrollToBottom();
        },300
        );
        setTimeout(() => {
          this.param.getWalkUser();
        },1000
        );
      }
    }

  }