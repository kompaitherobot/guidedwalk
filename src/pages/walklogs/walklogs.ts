import { Component, OnInit, ViewChild } from '@angular/core';
import { ParamService } from '../../services/param.service';
import { DomSanitizer } from '@angular/platform-browser';
import { Select } from 'ionic-angular';

@Component({
  selector: 'page-walklogs',
  templateUrl: 'walklogs.html'
})

export class WalkLogsPage implements OnInit {

  walklogstab : any;
  selectOptionuser:any;
  @ViewChild("select1") select1: Select;

  constructor(public param: ParamService, public sanitizer: DomSanitizer) {

    this.selectOptionuser = {
      title: this.param.datatext.chooseuser,
    };
  }

  ngOnInit(){
    this.param.getWalkLogs().subscribe(
      (data) => {


        this.param.walklogs=data;
        this.walklogstab = data;
        //console.log(data);
        }
      ,
      (err) => {
        console.log(err);
      }
    );




    // afficher/mettre à jour la liste


  }


  getWalkLogsById(e){

    console.log(e);

    this.param.getWalkLogsByUser(e).subscribe(
      (data) => {

        this.walklogstab=data;
        console.log(data);
        }
      ,
      (err) => {
        console.log(err);
      }
    );

  }

  onSliderRelease(ev, id: Select){
    ev.preventDefault();
    id.open();
  }
}
