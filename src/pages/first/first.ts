import { Component, OnInit } from "@angular/core";
import { NavController } from "ionic-angular";
import { ParamService } from "../../services/param.service";
import { ApiService } from "../../services/api.service";
import { AlertService } from "../../services/alert.service";
import { ChooseAssistancePage } from "../choose_assistance/choose_assistance";
import { RecofacePage } from "../recoface/recoface";
import { LoadingController } from "ionic-angular";
import { PopupService } from "../../services/popup.service";
import { SpeechService } from "../../services/speech.service";

@Component({
  selector: "page-first",
  templateUrl: "first.html",
})
export class FirstPage implements OnInit {
  
  choosePage = ChooseAssistancePage;
  recoPage=RecofacePage;
  appopen: any;
  cpt_open: number;
  loading: any;

  constructor(
    public loadingCtrl: LoadingController,
    public speech: SpeechService,
    public popup: PopupService,
    public param: ParamService,
    public alert: AlertService,
    public api: ApiService,
    public navCtrl: NavController
  ) {
    this.cpt_open = 0;
    this.loading = this.loadingCtrl.create({});
    this.loading.present(); //loading animation display
  }

  ngOnInit() {
    this.appopen = setInterval(() => this.appOpen(), 900);
    this.speech.getVoice();
  }

  appOpen() {
    if (this.param.localhost != undefined) {
      this.api.checkrobot();
    }
    this.api.checkInternet();
    this.alert.checkwifi(this.api.wifiok);
    this.cpt_open += 1;
    if (this.cpt_open === 15 && !this.api.robotok) {
      this.popup.startFailedAlert();
      this.speech.speak(this.param.datatext.cantstart);
      
    } else if (this.cpt_open === 15 && this.api.robotok) {
      this.popup.relocAlert();
      this.speech.speak(this.param.datatext.relocAlert_title + ". " + this.param.datatext.relocAlert_message);
    }
    if (!this.api.appOpened) {
      this.param.getDataRobot();
      this.param.getWalkUser();
      this.param.getMail();
      this.param.getPhone();
      this.param.getDurationNS();
      this.param.getDuration();
      this.param.getBattery();
      if (
        this.param.robot &&
        this.param.duration &&
        this.param.maillist &&
        this.param.phonenumberlist
      ) {
        this.param.fillData();
        if (this.param.robot.httpskomnav == 0) {
          this.api.httpskomnav = "http://";
          this.api.wsskomnav = "ws://";
        } else {
          this.api.httpskomnav = "https://";
          this.api.wsskomnav = "wss://";
        }
        if (this.param.robot.httpsros == 0) {
          this.api.wssros = "ws://";
        } else {
          this.api.wssros = "wss://";
        }
      }
      if (this.api.robotok && this.param.langage && this.param.maillist) {
        this.api.getCurrentMap();
        if (!this.api.socketok)
        {
          this.api.instanciate();
        }
        if (this.api.mapdata) {
          this.api.appOpened = true;
          this.api.getRoundList();
        }
      }
    } else if (this.api.appOpened && this.cpt_open>6) {
      this.api.abortNavHttp();
      this.alert.appOpen(this.api.mailAddInformationBasic());
      if(this.api.recoface){
        this.api.launchreco(true);
        this.navCtrl.setRoot(this.recoPage);
      }else{
        
        this.navCtrl.setRoot(this.choosePage);
      }
      
      this.loading.dismiss(); // destroy loading animation
      clearInterval(this.appopen);
      if (this.popup.alert_blocked) {
        this.popup.alert_blocked.dismiss();
      }
      console.log(this.cpt_open);
    }
  }
}
