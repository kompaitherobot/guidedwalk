import { Component, OnInit } from "@angular/core";
import { NavController } from "ionic-angular";
import { ParamService } from "../../services/param.service";
import { ApiService } from "../../services/api.service";
import { AlertService } from "../../services/alert.service";
import { ChooseAssistancePage } from "../choose_assistance/choose_assistance";
import { LoadingController } from "ionic-angular";
import { PopupService } from "../../services/popup.service";
import { SpeechService } from "../../services/speech.service";
import { HttpClient } from "@angular/common/http";


@Component({
  selector: "page-recoface",
  templateUrl: "recoface.html",
})
export class RecofacePage implements OnInit {

  choosePage = ChooseAssistancePage;
  authentify:any;
  username:string;
  login:string;
  password:string;
  recointerv:any;
  imgRos : any;
  loading: any;
  hidel = false;
  hidep = false;
  cptnoreco=0;
  token : any;

  constructor(
    public loadingCtrl: LoadingController,
    public speech: SpeechService,
    public popup: PopupService,
    public param: ParamService,
    public alert: AlertService,
    public api: ApiService,
    private httpClient: HttpClient,
    public navCtrl: NavController
  ) {
    this.speech.getVoice();
    
    this.api.listener_camera.subscribe((message) => {

      console.log('Received message on ' + this.api.listener_camera.name + ': ');
      this.imgRos = document.getElementById('imgcompressed');
      //console.log(message);

      var bufferdata = this.convertDataURIToBinary2(message.data);

      var blob = new Blob([bufferdata], {type: 'image/jpeg'});
      var blobUrl = URL.createObjectURL(blob);

      this.imgRos.src = blobUrl;
    });

  }

  convertDataURIToBinary2(dataURI) {
    var raw = window.atob(dataURI);
    var rawLength = raw.length;
    var array = new Uint8Array(new ArrayBuffer(rawLength));


    for(var i = 0; i < rawLength; i++) {
      array[i] = raw.charCodeAt(i);
    }
    return array;
  }

  ngOnInit() {
    this.recointerv = setInterval(()=> this.scanface(),500);
    this.authentify = "connect";
    setTimeout(() => { 
      this.speech.speak(this.param.datatext.getclose);
    }, 1000); 
  }

  scanface(){
    if(this.cptnoreco>25){
      this.cptnoreco=0;
      this.popup.showToastRed(this.param.datatext.noreco, 5000,"bottom");
      this.speech.speak(this.param.datatext.noreco);
    }
    //console.log("reco de visage en cours");
    const errors = new Set(["-1", "-2", "-3", "-4"]);
    var name =this.api.preco;
    if (this.authentify=="connect"){
      
      if(!errors.has(name)){
        var index: number = this.param.users.indexOf(name);
        if(index !== -1){//username dans bdd tablet
          this.api.selectUser=name;
          this.api.launchreco(false);
          clearInterval(this.recointerv);
          this.loading = this.loadingCtrl.create({});
          this.loading.present();
          this.popup.showToast(this.param.datatext.hi + name + ". "+ this.param.datatext.nicetosee, 5000,"bottom");
          this.speech.speak(this.param.datatext.hi + name );
          this.cptnoreco=0;
          setTimeout(() => {
            this.loading.dismiss();
            this.navCtrl.setRoot(this.choosePage);
            
          },3000
          );
          
        }else{
          this.cptnoreco+=1;
          console.log("name not in bdd tablet");
          
        }
      }else{
        console.log(name);
        if(name=="-1"){
          this.cptnoreco+=1;
        }
        
      }
    
    }else{
      console.log("scan reco register");
    }
  
    
  }

  cleantext(text:string){
    //text=text.replace(new RegExp("[<>&$§µ£#*_|`¤~(){}°²%/]", "g"),'');
    text=text.replace(new RegExp("[\"]", "g"),'');
    text=text.replace(new RegExp(/\\/, "g"),'');
    text=text.replace(new RegExp(/[\[\]]/, "g"),'');
    return text
  }

  addUser(ev)
  {
    ev.preventDefault();
    console.log(this.param.users)
    var index: number = this.param.users.indexOf(this.username);
    if (index !== -1) {
      this.popup.showToastRed(this.param.datatext.userexist,3000,"bottom");}
      else{
        this.loading = this.loadingCtrl.create({});
        this.loading.present();
        this.addfacejetson();
        
        
        
      }
  }

  handleChangeSeg(){
    console.log(this.authentify);
    if(this.authentify=="connect"){
      this.recointerv= setInterval(()=> this.scanface(),500);
      this.speech.speak(this.param.datatext.getclose);
    }else{
      clearInterval(this.recointerv);
      this.speech.speak(this.param.datatext.register);
    }
  }

  anonymous(ev){
    ev.preventDefault();
    clearInterval(this.recointerv);
    this.api.launchreco(false);
    this.navCtrl.setRoot(this.choosePage);
    
    
  }

  addfacejetson(){
    this.speech.speak(this.param.datatext.getclose);
    //console.log("add face jetson")
    setTimeout(() => {
      this.api.addUserJetson(this.username);
    },3000
    );
    setTimeout(() => {
      if(this.api.useradded){
       
        this.param.users.push(this.username);
        this.param.addUser(this.username,this.login,this.password);
        this.popup.showToast(this.param.datatext.useradd,3000,"bottom");
      }else{
        
        this.popup.showToastRed(this.param.datatext.failscan,3000,"bottom");
        this.speech.speak(this.param.datatext.failscan);
      }
    },5000
    );
    setTimeout(() => {
      this.param.getWalkUser();
      this.loading.dismiss();
      if( this.api.useradded){
        this.api.useradded=false;
        this.authentify="connect";
      }
    },6000
    );

  
  
  }


  clicValidate(ev){
    ev.preventDefault();
    this.username=this.cleantext(this.username);
    this.login=this.cleantext(this.login);
    this.password=this.cleantext(this.password);
    if(this.authentify=="connect"){
      const index: number = this.param.users.indexOf(this.username); 
      console.log(index);
      if(index !== -1){
       
        const headers = { 'X-Shapes-Key':'7Msbb3w^SjVG%j'};
        console.log("testapiloginstart");

        const body = {"email":this.login,"password":this.password};
        this.httpClient.post("https://kubernetes.pasiphae.eu/shapes/asapa/auth/login",body,{headers}).subscribe( (data) =>{
          console.log(data);
          //enregistrer token
          //console.log(data[items][0]);
          //this.token = data.items[0].token;
          this.popup.showToast(this.param.datatext.hi + name + ". "+ this.param.datatext.nicetosee, 3000,"bottom");
          clearInterval(this.recointerv);
          this.api.selectUser=this.username;
          this.param.tablewalklogs.walkuser_id=this.username;
          console.log(this.param.tablewalklogs.walkuser_id);
          this.navCtrl.setRoot(this.choosePage);
          this.api.launchreco(false);
          // text connexion
          
        },(error)=>{
          console.log('error : ', error);
          this.speech.speak(this.param.datatext.errorreco);
        this.popup.showToastRed(this.param.datatext.notfound,5000,"bottom");
        });

        
      }else{
        this.speech.speak(this.param.datatext.errorreco);
        this.popup.showToastRed(this.param.datatext.notfound,5000,"bottom");
      }
    }else{
      const headers = { 'X-Shapes-Key':'7Msbb3w^SjVG%j'};
      console.log("testapiloginstart");

      const body = {"email":this.login,"password":this.password};
      this.httpClient.post("https://kubernetes.pasiphae.eu/shapes/asapa/auth/login",body,{headers}).subscribe( (data) =>{
        console.log(data);
        //this.token = data.items[0].token;
        //mofid text enregistrement
        //this.popup.showToast(this.param.datatext.useradd,3000,"middle");
        this.addUser(ev);
      },(error)=>{
        console.log('error : ', error);
        this.speech.speak(this.param.datatext.errorreco);
        this.popup.showToastRed(this.param.datatext.notfound,5000,"bottom");
      });
    
    }
  }

}
