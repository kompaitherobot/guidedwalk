import { Component, OnInit } from '@angular/core';
import { ParamService } from '../../services/param.service';
import {ToastController} from 'ionic-angular';


@Component({
  selector: 'page-sms',
  templateUrl: 'sms.html'
})

export class SMSPage implements OnInit {


  ngOnInit() {

  }

  constructor(public param:ParamService, public toastCtrl: ToastController ) {

  } 

  errorToast(m:string){
    const toast=this.toastCtrl.create({
      message:m,
      duration:3000,
      position: 'middle',
      cssClass:"toast"
    });
    toast.present();
  }
  
  okToast(m: string){
    const toast=this.toastCtrl.create({
      message:m,
      duration:3000,
      position: 'middle',
      cssClass:"toastok"
    });
    toast.present();
  }

  removeNum(ev,m:string){
    ev.preventDefault();
    const index: number = this.param.phonenumberlist.indexOf(m);
    if (index !== -1) {
      this.param.phonenumberlist.splice(index, 1);
      this.param.deletePhone(m);
      this.okToast(this.param.datatext.mailRemove);
}
  }

 is_a_num(m:string){
  var numformat="^[0-9]{10}$";
  return m.match(numformat);
 }

 changeNum(m:string){
   return "+33"+m.substring(1);
 }

 displayNum(m:string){
  return "0"+m.substring(3);
}

addNum(ev,m:string)
  {
    ev.preventDefault();
    if(this.is_a_num(m)){
      const index: number = this.param.phonenumberlist.indexOf(this.changeNum(m));
      if (index !== -1) {
        this.errorToast(this.param.datatext.numexist);
        }
      else if(this.param.phonenumberlist.length>5){
        this.errorToast(this.param.datatext.deletenum);
      }
      else{
        this.param.phonenumberlist.push(this.changeNum(m));
        this.param.addPhone(this.changeNum(m));
        this.okToast(this.param.datatext.numadd);
      }
        
    }else{
        this.errorToast(this.param.datatext.numincorrect);
    }
  }


}
