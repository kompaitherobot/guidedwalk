import { Component, Input, OnInit } from '@angular/core';
import { ChooseAssistancePage } from '../../pages/choose_assistance/choose_assistance';
import { NavController} from 'ionic-angular';
import { ApiService } from '../../services/api.service';
import { TutoPage } from '../../pages/tuto/tuto';
import { PopupService } from '../../services/popup.service';
import { AlertService } from '../../services/alert.service';
import { ParamService } from '../../services/param.service';
import { ParamPage } from '../../pages/param/param';
import { WalkLogsPage } from '../../pages/walklogs/walklogs';
import {ToastController} from 'ionic-angular';
import { DomSanitizer } from '@angular/platform-browser';
import { SpeechService } from "../../services/speech.service";

@Component({
  selector: 'headpage',
  templateUrl: 'headpage.html'
})

export class HeadpageComponent implements OnInit{
  choosePage = ChooseAssistancePage;
  tutoPage = TutoPage;
  paramPage = ParamPage;
  walklogPage = WalkLogsPage;
  lowbattery:any;
  microon: boolean;
  cpt_battery:number;
  cpt_open:number;
  update: any;
  updateB:any;
  textHeadBand:string;
  source : any;
  
  textBoutonHeader: string; // bouton 's text (Return / Exit), to the left of the header, depends on the page you are on
  @Input() pageName: string;


  constructor(public toastCtrl: ToastController, public alert: AlertService,public param:ParamService, public popup : PopupService, public navCtrl: NavController,  public speech: SpeechService,public api:ApiService, public sanitizer: DomSanitizer) {
    this.api.appStart=false;
    this.microon=false;
    this.cpt_battery=0;
    this.cpt_open=0;

    if (this.param.allowspeech == 1) {
      this.api.volumeState = 1;
    } else {
      this.api.volumeState = 0;
    }
  }

  ionViewWillLeave(){
    clearInterval(this.update);
    clearInterval(this.lowbattery);
    clearInterval(this.source);
  }


  ngOnDestroy(): void {
    clearInterval(this.update);
    clearInterval(this.lowbattery);
    clearInterval(this.source);
  }

  onTouchHelp(ev){
    ev.preventDefault();
    if(!this.api.towardDocking && !this.api.inUse){
      this.alert.displayTuto(this.api.mailAddInformationBasic());
      this.api.walkercmdstartdetect();
      this.navCtrl.push(this.tutoPage);
    }
  }

  onTouchVolume(ev) {
    ev.preventDefault();
    this.speech.synth.cancel();
    if (this.api.volumeState === 0) {
      this.api.volumeState = 1;
      this.param.allowspeech = 1;

    } else {
      this.api.volumeState = 0;
      this.param.allowspeech = 0;
    }
  }

  updateBattery(){
    if(this.api.statusRobot===2){
      // battery logo will not apppear
      this.api.batteryState=10;
    }
    if(this.api.battery_status.status < 2){
      //charging
      this.api.batteryState=4;

    }else if (
      this.api.battery_status.status === 3 ||
      this.api.battery_status.remaining <= this.param.battery.critical
    ) {
      //critical
      this.api.batteryState = 0;
    } else if (this.api.battery_status.remaining > this.param.battery.high) {
      //hight
      this.api.batteryState = 3;
    } else if (this.api.battery_status.remaining > this.param.battery.low) {
      //mean
      this.api.batteryState = 2;
    } else if (this.api.battery_status.remaining <= this.param.battery.low) {
      //low
      this.api.batteryState = 1;
    } else {
      // battery logo will not apppear
      this.api.batteryState = 10;
    }

  }


  onTouchStatus(ev){
    ev.preventDefault();
    
    if(this.api.statusRobot===2)// if status red
    {      
      this.popup.statusRedPresent();
    }else {
      this.okToast(this.param.datatext.statusGreenPresent_message);
    }
    
  }


accessparam(){
  this.navCtrl.push(this.paramPage);
}

accesswalklog(){
  this.navCtrl.push(this.walklogPage);
}

onTouchParam(ev){
  ev.preventDefault();
  //this.speech.speak("Bonjour les amis. Comment ça va aujourd'hui ! ");
    if(!this.api.towardDocking && !this.api.inUse){//robot is not moving
      if(this.pageName==="param" || this.pageName==="sms" || this.pageName==="mail" || this.pageName==="langue" ){
        console.log("already");
      }else{
        //this.navCtrl.push(this.paramPage);
        this.popup.askpswd();
      }
  }
}

onTouchWalkLogs(ev){
  ev.preventDefault();
  //this.navCtrl.push(this.walklogPage);
    if(!this.api.towardDocking && !this.api.inUse){//robot is not moving
      if(this.pageName==="param" || this.pageName==="sms" || this.pageName==="mail" || this.pageName==="langue"){
        console.log("already");
      }else{
        //this.navCtrl.push(this.paramPage);
        this.popup.askpswdWalk();
      }
  }
}

  onTouchBattery(ev){
     ev.preventDefault();
    this.okToast(this.param.datatext.battery+" : "+this.api.battery_status.remaining+"%");
//     if(!this.api.towardDocking && !this.api.inUse){

//       if(this.pageName==="free_assistance" )
//       {
//         if(this.api.docking_status.status !=3){ // if not on docking
//           this.api.walkercmdstopdetect();
//           if(this.api.batteryState===0){
//             this.popup.lowBattery();
//           }
//           else{
//             this.popup.goDockingConfirm();
//           }
//         }
//         if(this.api.docking_status.status ===3){ //if on docking
//           this.api.walkercmdstopdetect();
//           this.popup.leaveDockingConfirm();
//         }
//       }
//       else{
//         this.clicOnMenu();
//       }
//     }else{
//       this.okToast(this.param.datatext.battery+" : "+this.api.battery_status.remaining+"%")
//   }
  }

onTouchWifi(ev){
  ev.preventDefault();
  if(!this.api.wifiok){
    this.okToast(this.param.datatext.nointernet);
  }else{
    this.okToast(this.param.datatext.AlertConnectedToI);
    }
}

  okToast(m: string){
    const toast=this.toastCtrl.create({
      message:m,
      duration:3000,
      position: 'middle',
      cssClass:"toastok"
    });
    toast.present();
  }

  monitoringBattery(){

    if (this.api.batteryState===0){

      this.popup.lowBattery();
      this.alert.lowBattery(this.api.mailAddInformationBasic());
      this.alert.Battery_c();
      if(!this.api.towardDocking){
        this.api.walkercmdstopdetect();
      }
      clearInterval(this.lowbattery);
    }
  }

  ngOnInit() {
    this.updateB = setInterval(()=> this.updateBand(),500);
    this.popup.onSomethingHappened1(this.accessparam.bind(this));
    this.popup.onSomethingHappened3(this.accesswalklog.bind(this));
    // bouton 's text (Return / Exit), to the left of the header, depends on the page you are on
    if(this.pageName==="recoface"){
      this.update = setInterval(()=> this.getUpdate(),500);//update hour, battery every 1/2 secondes
      this.lowbattery = setInterval(()=> this.monitoringBattery(),60000);
      this.source = setInterval(()=> this.api.jetsonOK(),2000);
      this.textBoutonHeader=this.param.datatext.quit;
      this.textHeadBand=this.param.datatext.auth;

    }else if(this.pageName==="choose_assistance" && !this.api.recoface){
      this.update = setInterval(()=> this.getUpdate(),500);//update hour, battery every 1/2 secondes
      this.lowbattery = setInterval(()=> this.monitoringBattery(),60000);
      this.source = setInterval(()=> this.api.jetsonOK(),2000);
      this.textBoutonHeader=this.param.datatext.quit;
    }else if(this.pageName==="choose_assistance"){
      this.textBoutonHeader=this.param.datatext.quit;
    }else if(this.pageName==="param"){
      this.textBoutonHeader=this.param.datatext.return;
      this.textHeadBand=this.param.datatext.param;
    }else if(this.pageName==="sms"){
      this.textBoutonHeader=this.param.datatext.return;
      this.textHeadBand=this.param.datatext.receivesms;
    }else if(this.pageName==="mail"){
      this.textBoutonHeader=this.param.datatext.return;
      this.textHeadBand= this.param.datatext.receivemail;
    }else if(this.pageName==="langue"){
      this.textBoutonHeader=this.param.datatext.return;
      this.textHeadBand=this.param.datatext.changelangage;
    }else if(this.pageName==="password"){
      this.textBoutonHeader=this.param.datatext.return;
      this.textHeadBand=this.param.datatext.editpswd;
    }else if(this.pageName==="start_assistance"){
        this.textBoutonHeader=this.param.datatext.return;
        this.textHeadBand=this.param.datatext.gobehindrobot;
    }else if(this.pageName==="walk"){
      this.textBoutonHeader=this.param.datatext.return;
      this.textHeadBand=this.param.datatext.walkInProgress;
    }else if(this.pageName==="walklogs"){
      this.textBoutonHeader=this.param.datatext.return;
      this.textHeadBand=this.param.datatext.walklog.toUpperCase();
    }else if(this.pageName==="user"){
      this.textBoutonHeader=this.param.datatext.return;
      this.textHeadBand=this.param.datatext.users.toUpperCase();
    }
    else{
      this.textBoutonHeader=this.param.datatext.return;
      this.textHeadBand=this.param.datatext.tutorial;
    }
  }


  updateBand(){

      if(this.api.towardDocking){
        this.textHeadBand=this.param.datatext.godocking;
      }

      if(this.pageName==="choose_assistance"){
      if(this.api.inUse){
        this.textHeadBand=this.param.datatext.walkInProgress;
      }else{
        if(this.api.selectMode=="destination"){
          this.textHeadBand=this.param.datatext.choosedestination;
        }else{
          this.textHeadBand=this.param.datatext.chooseround;
        }

      }

    }else if(this.pageName==="start_assistance"){
      if(this.api.position_state!==2){
        this.textHeadBand=this.param.datatext.gobehindrobot;
      }
      else if(this.api.position_state===2){
        this.textHeadBand=this.param.datatext.wait;
      }
    }
  }

  getUpdate(){ //update hour, battery and the walker detection
    if(!this.api.towardDocking)
    {

      if(this.api.walker_states.status===1)
      {
        this.api.appStart=true;
      }

    if(this.pageName==="choose_assistance" && this.api.walker_socket.readyState===1 && this.api.walker_states.status===0 && !this.api.appStart){
      //initialize the detection
      //this.api.walkercmdstartdetect();

      this.api.appStart=true;

    }
    }

    if(this.api.close_app){
      clearInterval(this.update);
    }

    this.updateBattery();
    var now = new Date().toLocaleString('fr-FR',{hour:'numeric',minute:'numeric'});
    this.api.hour = now;

    if (this.api.statusRobot === 0 && this.api.batteryState === 10){ // importante to know if the application has run correctly
      this.cpt_battery+=1;
    }
    else {
      this.cpt_battery=0;
    }

    if (this.cpt_battery > 10){
      this.api.statusRobot=2;
    }
  }

  clicOnMenu(ev){
    ev.preventDefault();
    // the action depends on the page you are on
    if (this.pageName==='tuto'||this.pageName==='param'|| this.pageName==='walklogs'){
      //this.api.walkercmdstartdetect();
      this.navCtrl.popToRoot()
    }
    else if(this.pageName==='sms'||this.pageName==='mail'||this.pageName==='langue'||this.pageName==='password'||this.pageName==='user'){
      this.navCtrl.pop();
    }
    else if(this.pageName==='start_assistance'|| this.pageName==='walk'){
      //this.api.walkercmdstopdetect();
      //this.navCtrl.popToRoot();
    }
    else{
      if(this.api.towardDocking){
        this.api.abortNavHttp();
        this.api.towardDocking=false;
        this.popup.quitConfirm();
      }else{
        this.api.close_app=true;
        this.alert.appClosed(this.api.mailAddInformationBasic());
        //stop the enslavement of the legs and quit the app
        this.api.walkercmdstopdetect();
        setTimeout(
          () => {
            window.close();
          }, 500
        );
      }
    }
  }
}
