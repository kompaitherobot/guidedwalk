import { ParamService } from "./param.service";
import { Injectable } from "@angular/core";

@Injectable()
export class SpeechService {
  synth:any;
  msg: any;
  volumeState:number=2;
  constructor(public param: ParamService) {
    this.msg = new SpeechSynthesisUtterance();
    this.msg.onerror = (event) => {
      console.error(`An error has occurred with the speech synthesis: ${event.error}`);
    }
    this.msg.volume = parseFloat("1");
    this.msg.rate = parseFloat("1");
    this.msg.pitch = parseFloat("1");
    this.synth=window.speechSynthesis;
    //this.msg.voice = speechSynthesis.getVoices().filter(function(voice) { return voice.name ; })[10];
  }

  getVoice() {
    var voices = speechSynthesis
    .getVoices()
    .filter(
      (voice) =>
        voice.localService == true && voice.lang == this.param.langage
    );

    if(voices.length > 1)
    {
      this.msg.voice = voices[1];
    }
    else{
      this.msg.voice = voices[0];
    }

  }

  // Create a new utterance for the specified text and add it to
  // the queue.
 
    speak(text: string) {
      if (this.param.allowspeech==1){
      this.synth.cancel();
      this.msg.lang = this.param.langage;
      // Create a new instance of SpeechSynthesisUtterance.
      // Set the text.
      this.getVoice();
      this.msg.text = text;
      //this.msg.voice = speechSynthesis.getVoices().filter(function(voice) { return voice.name ; })[10];
      //console.log(msg);
      // Queue this utterance.
      this.synth.speak(this.msg);
     
      }
      console.log(this.msg);
    }
    

 
}
