// the parameter script
import frtext from "../langage/fr-FR.json";
import entext from "../langage/en-GB.json";
import estext from "../langage/es-ES.json";
import grtext from "../langage/el-GR.json";
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { DomSanitizer } from "@angular/platform-browser";

@Injectable()
export class ParamService {
  localhost: string;
  robotmail: string;
  datamail: string;
  langage: string;
  phonenumberlist: string[];
  maillist: string[];
  datamaillist: string[];
  serialnumber: string;
  mailrobotpassw: string;
  maildatapassw: string;
  allowspeech: number;
  name: string;
  datatext: any;
  robot: any;
  duration: any;
  tableduration: any = {};
  tablerobot: any = {};
  mail: any = {};
  emotion: any = {};
  phone: any = {};
  date: any;
  currentduration: any = {};
  cpt: number;
  olddata: any = {};
  battery:any;
  sendduration: boolean;
  source: any;
  walkuser:any={};
  walklogs : any ;
  tablewalklogs:any={};
  users: string[]=[];
  tablewalkuser:any={};
  durationNS:any={}; //NS for not send
  tabledurationNS: any = {};
  tokenAsapa : string;



  constructor(private httpClient: HttpClient, public sanitizer: DomSanitizer) {
    this.maillist = [];
    this.phonenumberlist = [];
    this.cpt = 0;
    this.sendduration = false;

  }

  getDataRobot() {
    this.httpClient.get("http://localhost/ionicDB/getrobot.php").subscribe(
      (data) => {
        //console.log("robot:"+data);
        this.robot = data[0];
        this.langage = this.robot.langage;
        if (this.langage === "fr-FR") {
          this.datatext = frtext;
        } else if (this.langage === "en-GB") {
          this.datatext = entext;
        }else if(this.langage === "el-GR"){
          this.datatext = grtext;
        } else if (this.langage === "es-ES") {
          this.datatext = estext;
        }


        if(this.robot.tokenASAPA == null)
        {
          const headers = { 'X-Shapes-Key':'7Msbb3w^SjVG%j'};
          console.log("testapiloginstart");

          const body = {"email":this.robot.loginASAPA,"password":this.robot.passASAPA};
          this.httpClient.post("https://kubernetes.pasiphae.eu/shapes/asapa/auth/login",body,{headers}).subscribe( (data) =>{
            console.log(data["items"]);
            //console.log(data.items);
            console.log(data["items"][0].token);
            this.tokenAsapa = data["items"][0].token;

            this.savetok();

            //enregistrer token
            //console.log(data[items][0]);
          },(error)=>{
            console.log('error : ', error);
          });
        }
        else{
          this.tokenAsapa = this.robot.tokenASAPA;
        }

      },
      (err) => {
        console.log(err);
      }
    );
  }

  updateRobot() {
    this.tablerobot.action = "update";
    this.tablerobot.langage = this.robot.langage;
    this.tablerobot.serialnumber = this.serialnumber;
    this.tablerobot.name = this.name;
    this.tablerobot.allowspeech = this.robot.allowspeech;
    this.tablerobot.send_pic = this.robot.send_pic;
    this.tablerobot.password = this.robot.password;
    this.httpClient
      .post(
        "http://localhost/ionicDB/updaterobotpassword.php",
        JSON.stringify(this.tablerobot)
      )
      .subscribe(
        (data) => {
          console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
  }

  getPhone() {
    this.httpClient.get("http://localhost/ionicDB/getphone.php").subscribe(
      (data) => {
        if (this.phonenumberlist.length === 0) {
          for (let value in data) {
            this.phonenumberlist.push(data[value].phonenumber);
          }
        }
      },
      (err) => {
        console.log(err);
      }
    );
  }

  addMail(m: string) {
    this.mail.action = "insert";
    this.mail.mail = m;
    this.mail.serialnumber = this.serialnumber;
    this.httpClient
      .post("http://localhost/ionicDB/addmail.php", JSON.stringify(this.mail))
      .subscribe(
        (data) => {
          console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
  }

  addUser(user: string, login:string, password:string) {
    this.tablewalkuser.action = "insert";
    this.tablewalkuser.username = user;
    this.tablewalkuser.login = login;
    this.tablewalkuser.password = password;
    this.httpClient
      .post("http://localhost/ionicDB/guidedwalk/addusershapes.php", JSON.stringify(this.tablewalkuser))
      .subscribe(
        (data) => {
          console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
  }

  addEmotion(sad:number, happy:number, anger:number,surprise:number,neutral:number) {
    this.emotion.action = "insert";
    this.emotion.sad_emotion = sad;
    this.emotion.happy_emotion = happy;
    this.emotion.anger_emotion = anger;
    this.emotion.surprise_emotion = surprise;
    this.emotion.neutral_emotion = neutral;
    this.emotion.time_emotion = new Date().toLocaleTimeString();
    console.log(this.emotion);
    this.httpClient
      .post("http://localhost/ionicDB/emotions/addemotion.php", JSON.stringify(this.emotion))
      .subscribe(
        (data) => {
          console.log(data);
          this.senddataemotion();
        },
        (err) => {
          console.log(err);
        }
      );
  }


  senddataemotion(){

    this.httpClient
      .post("http://localhost/ionicDB/emotions/getemotion.php","")
      .subscribe(
        (data) => {
          console.log(data);
          console.log(data[0].id_emotion);
          var datenow = new Date(Date.now());
          console.log(datenow.toISOString());
          var dateemotion = datenow;
          var listtime = data[0].time_emotion.split(':');
          dateemotion.setHours(listtime[0],listtime[1],listtime[2]) ;
          console.log(dateemotion.toISOString());
          const body = {
            "anger_emotion": data[0].anger_emotion,
            "devId": this.robot.devID_ASAPA,
            "happy_emotion": data[0].happy_emotion,
            "id_emotion": data[0].id_emotion,
            "id_walklogs": data[0].id_walklogs,
            "neutral_emotion": data[0].neutral_emotion,
            "sad_emotion": data[0].sad_emotion,
            "surprise_emotion": data[0].surprise_emotion,
            "time": datenow.toISOString(),
            "time_emotion": dateemotion.toISOString()
          };

          console.log(body);

          const headers = { 'X-Authorisation':this.tokenAsapa  };
          this.httpClient.post("https://symbiote.shapes.f-in.io:9071/v1/kompai/emotion",body,{headers}).subscribe( (data) =>{
            console.log(data);
            //enregistrer token
            //console.log(data[items][0]);
          },(error)=>{
            console.log('error : ', error);
          });

        },
        (err) => {
          console.log(err);
        }
      );
  }


  senddatawalking(){

    this.httpClient
    .post("http://localhost/ionicDB/getdatawalking.php","")
    .subscribe(
      (data) => {
        console.log(data);
        console.log(data[0].walklogs_id);
        var testdate = new Date(data[0].walk_date);
        console.log(testdate);
        var datenow = new Date(Date.now());
        console.log(datenow.toISOString());

        const headers = { 'X-Authorisation':this.tokenAsapa  };

        const body = {
          "devId": this.robot.devID_ASAPA,
          "time": data[0].walk_date,
          "walk_date": data[0].walk_date,
          "walk_map": data[0].walk_map,
          "walk_meters": data[0].walk_meters,
          "walk_numberofpauses": data[0].walk_numberofpauses,
          "walk_round": data[0].walk_round,
          "walk_speed": data[0].walk_speed,
          "walk_time": testdate.toISOString(),
          "walk_timeofpauses": data[0].walk_timeofpauses,
          "walklogs_id": data[0].walklogs_id,
          "walkuser_id": data[0].walkuser_id
        };


        this.httpClient.post("https://symbiote.shapes.f-in.io:9071/v1/kompai/walking",body,{headers}).subscribe( (data) =>{
          console.log(data);
          //enregistrer token
          //console.log(data[items][0]);
        },(error)=>{
          console.log('error : ', error);
        });

        console.log(body);

      },
      (err) => {
        console.log(err);
      }
    );

  }

  deleteMail(m: string) {
    this.mail.action = "delete";
    this.mail.mail = m;
    this.httpClient
      .post(
        "http://localhost/ionicDB/deletemail.php",
        JSON.stringify(this.mail)
      )
      .subscribe(
        (data) => {
          console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
  }

  deleteUser(user: string) {
    this.tablewalkuser.action = "delete";
    this.tablewalkuser.username = user;
    this.httpClient
      .post(
        "http://localhost/ionicDB/guidedwalk/deleteusershapes.php",
        JSON.stringify(this.tablewalkuser)
      )
      .subscribe(
        (data) => {
          console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
  }

  addPhone(m: string) {
    this.phone.action = "insert";
    this.phone.phonenumber = m;
    this.phone.serialnumber = this.serialnumber;
    this.httpClient
      .post("http://localhost/ionicDB/addphone.php", JSON.stringify(this.phone))
      .subscribe(
        (data) => {
          console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
  }

  deletePhone(m: string) {
    this.phone.action = "delete";
    this.phone.phonenumber = m;
    this.httpClient
      .post(
        "http://localhost/ionicDB/deletephone.php",
        JSON.stringify(this.phone)
      )
      .subscribe(
        (data) => {
          console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
  }

  getMail() {
    this.httpClient.get("http://localhost/ionicDB/getmail.php").subscribe(
      (data) => {
        if (this.maillist.length === 0) {
          for (let value in data) {
            this.maillist.push(data[value].mail);
          }
        }
      },
      (err) => {
        console.log(err);
      }
    );
  }

  getWalkUser(){
    console.log("get user")
    this.httpClient.get("http://localhost/ionicDB/guidedwalk/getwalkusershapes.php").subscribe(
      (data) => {
        this.users = [];
        for (let value in data) {
          this.users.push(data[value].username);
        }
        this.walkuser=data;
        console.log(data);
        }
      ,
      (err) => {
        console.log(err);
      }
    );
  }

  getWalkLogs(){
    return this.httpClient.get("http://localhost/ionicDB/guidedwalk/getwalklogs.php");
  }

  getWalkLogsByUser(e : any)
  {
    //console.log(e);
    return this.httpClient.get("http://localhost/ionicDB/guidedwalk/getwalklogsbyid.php/?user=" + e);
  }

  addwalklog(map:string,speed:number){
    this.tablewalklogs.action = "insert";
    //this.tablewalklogs.walkuser_id = user;
    this.tablewalklogs.walk_meters = 0;
    this.tablewalklogs.walk_numberofpauses = 0;
    this.tablewalklogs.walk_time = 0;
    this.tablewalklogs.walk_timeofpauses = 0;
    this.tablewalklogs.walk_map = map;
    //this.tablewalklogs.walk_round = round;
    this.tablewalklogs.walk_speed = speed;
    this.tablewalklogs.date = new Date().toLocaleDateString("fr-CA");
    this.httpClient
      .post(
        "http://localhost/ionicDB/guidedwalk/addwalklog.php",
        JSON.stringify(this.tablewalklogs)
      )
      .subscribe(
        (data) => {
          console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
  }

  updatewalklog() {
    this.tablewalklogs.action = "update";

    this.httpClient
      .post(
        "http://localhost/ionicDB/guidedwalk/updatewalklog.php",
        JSON.stringify(this.tablewalklogs)
      )
      .subscribe(
        (data) => {
          console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
  }


  addDuration() {
    this.tableduration.action = "insert";
    this.tableduration.serialnumber = this.serialnumber;
    this.tableduration.round = this.currentduration.round;
    this.tableduration.battery = this.currentduration.battery;
    this.tableduration.walk = this.currentduration.walk;
    this.tableduration.patrol = this.currentduration.patrol;
    this.tableduration.date = this.currentduration.date;
    this.httpClient
      .post(
        "http://localhost/ionicDB/addduration.php",
        JSON.stringify(this.tableduration)
      )
      .subscribe(
        (data) => {
          console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
  }

  getDurationNS(){
    this.httpClient.get("http://localhost/ionicDB/duration/getduration.php").subscribe(
      (data) => {
        this.durationNS = data;  // NS =not send
        //console.log(data);
        //console.log(this.durationNS.length);
      },
      (err) => {
        console.log(err);
      }
    );
  }

  updateDurationNS(id:number){


    this.tabledurationNS.action="update";
    this.tabledurationNS.id_duration=id;
    this.httpClient
      .post(
        "http://localhost/ionicDB/duration/updateduration.php",
        JSON.stringify( this.tabledurationNS)
      )
      .subscribe(
        (data) => {
          console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
  }


  getDuration() {
    this.httpClient.get("http://localhost/ionicDB/getduration.php").subscribe(
      (data) => {
        this.duration = data;
      },
      (err) => {
        console.log(err);
      }
    );
  }

  updateDuration() {
    this.tableduration.action = "update";
    this.tableduration.serialnumber = this.serialnumber;
    this.tableduration.round = this.currentduration.round;
    this.tableduration.battery = this.currentduration.battery;
    this.tableduration.walk = this.currentduration.walk;
    this.tableduration.patrol = this.currentduration.patrol;
    this.tableduration.date = this.currentduration.date;
    this.httpClient
      .post(
        "http://localhost/ionicDB/updateduration.php",
        JSON.stringify(this.tableduration)
      )
      .subscribe(
        (data) => {
          console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
  }

  getBattery(){
    this.httpClient.get("http://localhost/ionicDB/getbattery.php").subscribe(
      (data) => {
        this.battery = data[0];
      },
      (err) => {
        console.log(err);
      }
    );
  }

  fillData() {
    this.name = this.robot.name;
    this.allowspeech = this.robot.allowspeech;
    this.maildatapassw = atob(this.robot.maildatapass);
    this.mailrobotpassw = atob(this.robot.mailrobotpass);
    this.localhost = this.robot.localhost;
    this.serialnumber = this.robot.serialnumber;
    this.robotmail = this.robot.mailrobot;
    this.datamail = this.robot.maildata;
    this.datamaillist = ["data@kompai.com"];
    this.source = this.sanitizer.bypassSecurityTrustResourceUrl(this.datatext.URL_assistancemobility);
    if(this.duration.length>0){
      if (
        this.duration[this.duration.length - 1].date ===
        new Date().toLocaleDateString("fr-CA")
      ) {
        this.currentduration.date = this.duration[this.duration.length - 1].date;
        this.currentduration.round = this.duration[
          this.duration.length - 1
        ].round;
        this.currentduration.battery = this.duration[
          this.duration.length - 1
        ].battery;
        this.currentduration.patrol = this.duration[
          this.duration.length - 1
        ].patrol;
        this.currentduration.walk = this.duration[this.duration.length - 1].walk;
        this.currentduration.toolbox = this.duration[this.duration.length - 1].toolbox;
      } else {
        if(!this.sendduration){
          this.sendduration = true;
          this.init_currentduration();
          this.addDuration();
        }
      }
    }else{
      if(!this.sendduration){
        this.sendduration = true;
        this.init_currentduration();
        this.addDuration();
      }

    }

  }
  init_currentduration(){
    this.currentduration.round = 0;
    this.currentduration.battery = 0;
    this.currentduration.patrol = 0;
    this.currentduration.walk = 0;
    this.currentduration.toolbox = 0;
    this.currentduration.date = new Date().toLocaleDateString("fr-CA");
  }

  cptDuration() {
    if (this.currentduration.date === new Date().toLocaleDateString("fr-CA")) {
      this.cpt = parseInt(this.currentduration.walk);
      this.currentduration.walk = this.cpt + 2;
      this.updateDuration();
    } else {
      this.init_currentduration();
      this.addDuration();
    }
  }

  // ASAPA function

  savetok(){

    var tableupdate : any = {};
    tableupdate.action = "update";
    tableupdate.tokenASAPA = this.tokenAsapa;
    var dateno = new Date(Date.now());
    tableupdate.tokenASAPA_date = dateno.toISOString().slice(0, 10) ;
    console.log(tableupdate.tokenASAPA_date);



    this.httpClient
      .post(
        "http://localhost/ionicDB/settoken.php",
        JSON.stringify(tableupdate)
      )
      .subscribe(
        (data) => {
          console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
  }
}
