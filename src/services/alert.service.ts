// to send mail and sms alert

import { Injectable, OnInit, ViewChild, ElementRef} from '@angular/core';
import { App } from 'ionic-angular';
import { ParamService } from './param.service';
import { HttpClient } from '@angular/common/http';


// function send mail in index.html
declare var envoyerSMS;

// var in index.html
declare var messageAenvoyer;
declare var numTelToElement;


@Injectable()
export class AlertService implements OnInit {

  constructor(public app:App, public param :ParamService,private http:HttpClient){
  
  }

  ngOnInit()
  {
  
  }

  
  @ViewChild('video') videoElement: ElementRef;
  @ViewChild('canvas') canvas: ElementRef;
  videoWidth = 0;
  videoHeight = 0;
  allowmail:boolean;
  constraints = {
    //audio: false,
    //video: {deviceId: "f9860b260e18d7fd1687d8567e21b49c4bb26f87606020741717e11f6179f409"},
    video: true,
    facingMode:"environment",
    width:{ideal : 1920},
    height:{ideal:1080}
  };

 

  handleError(error) {
    console.log('Error: ', error);
  }



 // function send mail without image
  // bod : corps du mail
  // suj : sujet du mail
  // dest : adresse mail du destinataire
  sendmailPerso(bod,suj,dest,data,usern,passw,robotn){ //client mail without picture
    //console.log("sendmail");
    var usermailData = {
    username: usern,
    email: data,
    bcc: dest,
    date: new Date (Date.now()).toDateString(),
    subject:suj,
    message:bod,
    password: passw,
    robotname : robotn
  };

    const link = "http://localhost/ionicDB/SendMail/sendmail.php";

    const postParams = JSON.stringify(usermailData);
    //console.log(postParams);

    if(this.allowmail){
      this.http.post(link,postParams).subscribe( response => {
        //console.log(response);
        //console.log(this.param.durationNS);
        if(response=="OK"){
          var s=this.param.serialnumber +' : Duration App'
          
          if(suj == s){
            
            this.param.updateDurationNS(this.param.durationNS[0].id_duration);
            this.param.durationNS.shift();
            //console.log(this.param.durationNS);
          }
        }
      },error =>{
        console.log(error);
      });
    }

  }

  sendAlertData(bod,suj){ //data mail without picture
    this.sendmailPerso(bod,suj,this.param.datamaillist,this.param.datamail,this.param.datamail, this.param.maildatapassw,this.param.datamail);
  }


  sendAlertClient(bod,suj){ // data mail without picture
    if(this.param.maillist.length){
      this.sendmailPerso(bod,suj,this.param.maillist,this.param.datamail,this.param.robotmail, this.param.mailrobotpassw,this.param.name);
  }}


  checkwifi(bool){
    this.allowmail=bool;
  }


  // function send mail with image
  // bod : corps du mail
  // suj : sujet du mail
  // dest : adresse mail du destinataire
  // url : image attachment
  sendmailImgperso(url,bod,suj,dest,data,usern,passw,robotn){
    //console.log("sendmail");
    var usermailData = {
    username: usern,
    email: data,
    bcc: dest,
    date: new Date (Date.now()).toDateString(),
    subject:suj,
    message:bod,
    password: passw,
    robotname : robotn,
    url : url
    };

    const link = "http://localhost/ionicDB/SendMail/sendmailImg.php";

    const postParams = JSON.stringify(usermailData);
    //console.log(postParams);

    if(this.allowmail){
      this.http.post(link,postParams).subscribe( response => {
        //console.log(response);
      });
    }
  }

  sendAlertImgData(url,bod,suj){
    this.sendmailImgperso(url,bod,suj,this.param.datamail,this.param.datamail,this.param.datamail, this.param.maildatapassw,this.param.datamail);
  }

  sendAlertImgClient(url,bod,suj){
    if(this.param.maillist.length){
    this.sendmailImgperso(url,bod,suj,this.param.maillist,this.param.datamail,this.param.robotmail, this.param.mailrobotpassw,this.param.name);
  }}

// function send sms
sendsmsperso(mess,num){
  messageAenvoyer = mess;
  numTelToElement = num;
  envoyerSMS();

}

appError(info:string){
  this.sendAlertData(
    '<br> An error occurred, the round application has been reload automatically'+
    '<br> Code alert : 1'+
    info,
    this.param.serialnumber+' : Error (App Guided Walk)');
}

charging(info:string){
  this.sendAlertData(
    '<br> The robot has docked successfully'+
    '<br> Code alert : 2'+
    info,
    this.param.serialnumber+' : Charging');
}



noLongerBlocked(info:string){
  this.sendAlertData(
    '<br> The robot is no longer blocked'+ 
    '<br> Code alert : 3'+
    info,
    this.param.serialnumber+' : Automatic release');
}

manualintervention(info:string){
  this.sendAlertData(
    '<br> Someone unlocked the robot'+
    '<br> Code alert : 25'+
    info,
    this.param.serialnumber+' : Manual release');
}

appOpen(info:string){
  this.sendAlertData(
    '<br> Someone oppened the walkassist app.'+
    '<br> Code alert : 24'+
    info,
    this.param.serialnumber+' : App opened (Guided walk)');
}

duration(olddata :any, info:string){
  console.log("duration alert");
  this.sendAlertData(
    '<br> Here is a count of the duration of use of the apps in minutes'+
    '<br> Date duration : '+ olddata.date +
    '<br> Round duration : '+ olddata.round +
    '<br> Patrol duration : '+ olddata.patrol +
    '<br> Walk duration : '+ olddata.walk +
    '<br> Battery duration : '+ olddata.battery +
    '<br> Toolbox duration : '+ olddata.toolbox +
    '<br> Code alert : 26' +
    info,
    this.param.serialnumber+' : Duration App');
}


naverror(info:string){
  this.sendAlertData(
    '<br> A navigation error has occurred. The robot may be lost. The nominal current may have been exceeded.'+
    '<br> Code alert : 4'+
    info,
    this.param.serialnumber+' : Navigation error');
}

lowBattery(info:string){
  this.sendAlertData(
    '<br> The robot must be sent to the docking station.'+
    '<br> Code alert : 5'+
    info,
    this.param.serialnumber+' : Battery Critical');
}

errorblocked(info:string){
  this.sendAlertData(
    '<br> The robot has been stopped because the rated current is exceeded'+
    '<br> Code alert : 6'+
    info,
    this.param.serialnumber+' : High current');
}

robotLost(info:string){
  this.sendAlertData(
    '<br> The robot is lost. It must be relocated'+
    '<br> Code alert : 7'+
    info,
    this.param.serialnumber+' : Robot Lost');
}

blockingdocking(info:string){
  this.sendAlertData(
    '<br> The robot encountered an obstacle while heading for the docking station'+
    '<br> Code alert : 8'+
    info,
    this.param.serialnumber+" : Can't reach docking");
}

mobAssistSOS(info:string){
  this.sendAlertData(
    '<br> Somebody is asking for help'+
    '<br> Code alert : 9'+
    info,
    this.param.serialnumber+' : SOS');
}



mobAssistActivated(info:string){
  this.sendAlertData(
    '<br> Somebody starts walking with Kompai'+
    '<br> Code alert : 22'+
    info,
    this.param.serialnumber+' : Mobility assistance activated');
}


appInUse(info:string){
  this.sendAlertData(
    '<br> Somebody is walking with Kompai'+
    '<br> Code alert : 23'+
    info,
    this.param.serialnumber+' : Mobility assistance in use');
}

displayTuto(info:string){
  this.sendAlertData(
    '<br> Someone is reading the tuto'+
    '<br> Code alert : 10'+
    info,
    this.param.serialnumber+' : Tuto opened');
}

leaveDocking(info:string){
  this.sendAlertData(
    '<br> Kompai is no longer on docking'+
    '<br> Code alert : 11'+
    info,
    this.param.serialnumber+' : Leave docking');
  }

askCharge(info:string){
  this.sendAlertData(
    '<br> The robot must move towards docking'+
    '<br> Code alert : 12'+
    info,
    this.param.serialnumber+' : Charging request');
}

appClosed(info:string){
  this.sendAlertData(
    '<br> Somebody has closed the round application'+
    '<br> Code alert : 13'+
    info,
    this.param.serialnumber+' : App Guided walk closed');
}

blocking(info:string){
  this.sendAlertData(
    '<br> The robot is blocked by an obstacle'+
    '<br> Code alert : 19'+
    info,
    this.param.serialnumber+' : Blocking');
  }

SOS(info:string){
  this.sendAlertData(
    '<br> Somebody is asking for help'+
    '<br> Code alert : 9'+
    info,
    this.param.serialnumber+' : SOS');
}

/////////////////////////////////// Mail client
SOS_c(){
  this.sendAlertClient(
    this.param.datatext.mailSOS_body,
    this.param.datatext.mailSOS_suj);
}

Battery_c(){
  this.sendAlertClient(
    this.param.datatext.mailBattery_body,
    this.param.datatext.mailBattery_suj);
}

Blocked_c(url){
this.sendAlertImgClient(url,
this.param.datatext.mailBlocked_body,
this.param.datatext.mailBlocked_suj);

}

}
