// Allows Http communication with komnav
// cf Marc komnav_http_manual for more information
// https://drive.google.com/drive/u/3/folders/1-18kyKv6Ep-fzPU1F-dp0tQ1r8usyuZ7

import { Injectable, OnInit} from "@angular/core";

import { App, ToastController } from "ionic-angular";
import { HttpClient } from "@angular/common/http";
import { AlertService } from "./alert.service";
import { ParamService } from "./param.service";
import { Observable } from "rxjs/Observable";
declare var ROSLIB: any;


export class Walkassit {
  timestamp: any;
  status: number; // Disabled 0 Enabled 1 Error 2 Active 3
  person: Person; // actual position x,y of the person
  origin: Origin; // position x,y of the person when she start walking with the robot
  detected: boolean; // to know if the robot see the person or not
  activationDistance: number;
  targetAngularSpeed: number;
  barsAngle: number;

}

export class Emotion {
  id: any;
  neutral: number;
  happy: number;
  sad: number;
  anger: number;
  surprise: number;

}

export class Person {
  x: number;
  y: number;
}

export class Origin {
  x: number;
  y: number;
}

export class Docking {
  status: number;
  detected: boolean;
}
/*
{ Docking status
  "Unknown    ",
  "Undocked   ",
  "Docking    ",
  "Undocking  ",
  "Error      "
};*/

export class Battery {
  autonomy: number; //the estimated remaining operation time, in minutes.
  current: number; //the actual current drawn from the battery, in Amps.
  remaining: number; //the percentage of energy remaining in the battery, range 0 - 100 %.
  status: number; // Charging 0 Chrged 1 Ok 2 Critical 3
  timestamp: number;
  voltage: number; // the actual battery voltage in Volts.
}

export class Anticollision {
  timestamp: number;
  enabled: boolean;
  locked: boolean;
  forward: number; // 0 ok , 1 speed low , 2 obstacle
  reverse: number;
  left: number;
  right: number;
}

export class Statistics {
  timestamp: number;
  totalTime: number;
  totalDistance: number;
}

export class Iostate {
  // receive robot's boutons input
  timestamp: number;
  dIn: any;
  aIn: any;
}

export class Navigation {
  avoided: number;
  status: number;
  trajectory: any;
}



// The status can have the following values:
// • 0 - Waiting: the robot is ready for a new destination speciﬁed with a PUT /navigation/destination request.
// • 1 - Following: the robot is following the trajectory computed to join the destination.
// • 2 - Aiming: the robot has completed the trajectory and is now rotating to aim the destination accurately
// • 3 - Translating: the robot has ﬁnished aiming and is now translating to reach the destination
// • 4 - Rotating: this is the last rotation, executed only if the destination was speciﬁed with “Rotate” : true
// • 5 - Error: indicates that the robot has encountered an error that prevent edit to join the required destination.
// This can be cleared by sending a new destination.

export class Exercise {
  maxSpeed: number;
  status: number;// 0 - Waiting, 1 - Following, 2 - Error
  trajectory: any;
}

export class Localization {
  positionx: number;
  positiony: number;
  positiont: number;
}
export class Differential {
  status: number;
  TargetLinearSpeed: number;
  CurrentLinearSpeed: number;
  TargetAngularSpeed: number;
  CurrentAngularSpeed: number;
}

export class Round {
  round: any;
  acknowledge: boolean; // send to the next poi
  abort: boolean; // stop round
  pause: boolean; // pause the round
  status: number; //0none   1ready to be executed  2moving  3paused  4onPOI  5Completed
}

export class Trajectory {
  Control:number;
  Status:number;
  DistanceCovered:number;
}

@Injectable()
export class ApiService implements OnInit {
  batteryremaining: number;


  battery_status: Battery;
  battery_socket: WebSocket;
  hour: any;
  differential_status: Differential;
  differential_socket: WebSocket;

  navigation_status: Navigation;
  navigation_socket: WebSocket;

  exercise_status: Exercise;
  exercise_socket: WebSocket;

  statistics_socket: WebSocket;
  statistics_status: Statistics;

  anticollision_status: Anticollision;
  anticollision_socket: WebSocket;

  walker_socket: WebSocket;
  walker_states: Walkassit;

  docking_socket: WebSocket;
  docking_status: Docking;

  io_socket: WebSocket;
  io_status: Iostate;

  localization_status: Localization;
  localization_socket: WebSocket;

  emotion_status: Emotion;
  emotion_socket: WebSocket;
  trajectory_status: Trajectory;
  trajectory_socket: WebSocket;

  statusRobot: number;
  cpt_jetsonOK: number;
  wifiok: boolean;
  metercovered: number;
  traveldebut: number;
  towardDocking: boolean;
  inUse: boolean;
  b_pressed: boolean;
  btnPush: boolean;
  is_connected: boolean;
  close_app: boolean;
  appStart: boolean;
  appOpened: boolean;
  batteryState: number;
  robotok: boolean;
  id_current_map: number;
  name_current_map: string;
  mapdata: any;
  all_locations: any;
  imgMap: any;
  rounddata: any;
  rounds_current_map: any;
  current_round_name: string;
  round_selected: any;
  selectRound: any;
  selectSpeed: any = 0.5;
  selectMode: string = "round";
  selectUser: any = "anonymous";
  selectDestination: string;
  selectWalkZone: any = 0.5;
  position_state: number = 0;//0 ?  1 bad 2 good
  resume: boolean = false;
  cptpoi:number=0;
  ros:any;
  listener_camera:any;
  listener_recoface:any;
  socketok:boolean=false;
  recoface:boolean=false;
  preco:string;
  facedetected:boolean=false;
  reconame:string;
  useradded:boolean=false;
  volumeState: number; //0 mute,  1 low,  2 hight
  httpskomnav: string;
  wsskomnav: string;
  httpsros: string;
  wssros: string;

  constructor(
    public toastCtrl: ToastController,
    public app: App,
    private httpClient: HttpClient,
    public alert: AlertService,
    public param: ParamService
  ) {
    this.is_connected = false; // to know if we are connected to the robot
    this.towardDocking = false; //to know if the robot is moving toward the docking
    this.traveldebut = 0;
    this.statusRobot = 10;
    this.metercovered = 0;
    this.cpt_jetsonOK = 0;
    this.appStart = false;
    this.close_app = false;
    this.appOpened = false;
    this.inUse = false; // to know if the enslavement of the legs is active or not
    this.robotok = false;
  }
 

  instanciate() {
    this.socketok=true;
    var apiserv = this;
    ////////////////////// localization socket
    this.selectUser=this.param.datatext.anonymous;
    this.localization_status = new Localization();

    this.localization_socket = new WebSocket(
      this.wsskomnav + this.param.localhost + "/api/localization/socket",
      "json"
    );

    this.localization_socket.onerror = function (event) {
      console.log("error localization socket");
    };

    this.localization_socket.onopen = function (event) {
      console.log("localization socket opened");

      this.onmessage = function (event) {
        var test = JSON.parse(event.data);
        apiserv.localization_status.positionx = test["Pose"]["X"];
        apiserv.localization_status.positiony = test["Pose"]["Y"];
        apiserv.localization_status.positiont = test["Pose"]["T"];
      };

      this.onclose = function () {
        console.log("localization socket closed");
      };
    };

    ////////////////////// differential socket

    this.differential_status = new Differential();

    this.differential_socket = new WebSocket(
      this.wsskomnav + this.param.localhost + "/api/differential/socket",
      "json"
    );

    this.differential_socket.onerror = function (event) {
      console.log("error differential socket");
    };

    this.differential_socket.onopen = function (event) {
      console.log("differential socket opened");

      this.onmessage = function (event) {
        var test = JSON.parse(event.data);
        apiserv.differential_status.status = test["Status"];
        apiserv.differential_status.TargetAngularSpeed = test["TargetAngularSpeed"];
        apiserv.differential_status.TargetLinearSpeed = test["TargetLinearSpeed"];
        apiserv.differential_status.CurrentAngularSpeed = test["CurrentAngularSpeed"];
        apiserv.differential_status.CurrentLinearSpeed = test["CurrentLinearSpeed"];

      };

      this.onclose = function () {
        console.log("differential socket closed");
      };
    };

    ////////////////////// navigation socket

    this.navigation_status = new Navigation();

    this.navigation_socket = new WebSocket(
      this.wsskomnav + this.param.localhost + "/api/navigation/socket",
      "json"
    );

    this.navigation_socket.onerror = function (event) {
      console.log("error navigation socket");
    };

    this.navigation_socket.onopen = function (event) {
      console.log("navigation socket opened");

      this.onmessage = function (event) {
        var test = JSON.parse(event.data);
        apiserv.navigation_status.status = test["Status"];
        apiserv.navigation_status.avoided = test["Avoided"];
        apiserv.navigation_status.trajectory = test["Trajectory"];
      };

      this.onclose = function () {
        console.log("navigation socket closed");
      };
    };

    ////////////////////// exercise socket

    this.exercise_status = new Exercise();

    this.exercise_socket = new WebSocket(
      this.wsskomnav + this.param.localhost + "/api/exercise/socket",
      "json"
    );

    this.exercise_socket.onerror = function (event) {
      console.log("error exercise socket");
    };

    this.exercise_socket.onopen = function (event) {
      console.log("exercise socket opened");

      this.onmessage = function (event) {
        var test = JSON.parse(event.data);
        apiserv.exercise_status.status = test["Status"];
        apiserv.exercise_status.maxSpeed = test["MaxSpeed"];
        apiserv.exercise_status.trajectory = test["Trajectory"];
      };

      this.onclose = function () {
        console.log("exercise socket closed");
      };
    };
    ////////////////////// docking socket

    this.docking_status = new Docking();

    this.docking_socket = new WebSocket(
      this.wsskomnav + this.param.localhost + "/api/docking/socket",
      "json"
    );

    this.docking_socket.onerror = function (event) {
      console.log("error docking socket");
    };

    this.docking_socket.onopen = function (event) {
      console.log("docking socket opened");

      this.onmessage = function (event) {
        var test = JSON.parse(event.data);
        apiserv.docking_status.status = test["Status"];
        apiserv.docking_status.detected = test["Detected"];
      };

      this.onclose = function () {
        console.log("docking socket closed");
      };
    };

    ////////////////////// anticollision socket

    this.anticollision_status = new Anticollision();

    this.anticollision_socket = new WebSocket(
      this.wsskomnav + this.param.localhost + "/api/anticollision/socket",
      "json"
    );

    this.anticollision_socket.onerror = function (event) {
      console.log("error anticollision socket");
    };

    this.anticollision_socket.onopen = function (event) {
      console.log("anticollision socket opened");

      this.onmessage = function (event) {
        var test = JSON.parse(event.data);
        apiserv.anticollision_status.timestamp = test["Timestamp"];
        apiserv.anticollision_status.enabled = test["Enabled"];
        apiserv.anticollision_status.locked = test["Locked"];
        apiserv.anticollision_status.forward = test["Forward"];
        apiserv.anticollision_status.right = test["Right"];
        apiserv.anticollision_status.left = test["Left"];
      };

      this.onclose = function () {
        console.log("anticollision socket closed");
      };
    };
    ////////////////////// statistics socket

    this.statistics_status = new Statistics();

    this.statistics_socket = new WebSocket(
      this.wsskomnav + this.param.localhost + "/api/statistics/socket",
      "json"
    );

    this.statistics_socket.onerror = function (event) {
      console.log("error statistic socket");
    };

    this.statistics_socket.onopen = function (event) {
      console.log("statistic socket opened");

      this.onmessage = function (event) {
        var test = JSON.parse(event.data);
        apiserv.statistics_status.totalTime = test["TotalTime"];
        apiserv.statistics_status.totalDistance = test["TotalDistance"];
        apiserv.statistics_status.timestamp = test["Timestamp"];
      };

      this.onclose = function () {
        console.log("statistic socket closed");
      };
    };

    ////////////////////walker socket

    this.walker_states = new Walkassit();
    this.walker_socket = new WebSocket(
      this.wsskomnav + this.param.localhost + "/api/walker/socket",
      "json"
    );
    this.walker_socket.onerror = function (event) {
      console.log("error walker socket");
    };

    this.walker_socket.onopen = function (event) {
      console.log("walker socket opened");
      this.onmessage = function (event) {
        var test = JSON.parse(event.data);
        apiserv.walker_states.detected = test["Detected"];
        apiserv.walker_states.origin = test["Origin"];
        apiserv.walker_states.timestamp = test["Timestamp"];
        apiserv.walker_states.person = test["Person"];
        apiserv.walker_states.status = test["Status"];
        apiserv.walker_states.barsAngle = test["BarsAngle"];
        apiserv.walker_states.targetAngularSpeed = test["TargetAngularSpeed"];
        apiserv.walker_states.activationDistance = test["ActivationDistance"];
      };

      this.onclose = function () {
        console.log("walker socket closed");
      };
    };

    ///////////////////////// battery socket
    this.battery_status = new Battery();

    this.battery_socket = new WebSocket(
      this.wsskomnav + this.param.localhost + "/api/battery/socket",
      "json"
    );

    this.battery_socket.onerror = function (event) {
      console.log("error battery socket");
    };

    this.battery_socket.onopen = function (event) {
      console.log("battery socket opened");

      this.onmessage = function (event) {
        var test = JSON.parse(event.data);
        apiserv.battery_status.autonomy = test["Autonomy"];
        apiserv.battery_status.current = test["Current"];
        apiserv.battery_status.remaining = test["Remaining"];
        apiserv.battery_status.status = test["Status"];
        apiserv.battery_status.timestamp = test["Timestamp"];
        apiserv.battery_status.voltage = test["Voltage"];
      };

      this.onclose = function () {
        console.log("battery socket closed");
      };
    };


    //////////////////////////////// io socket
    this.b_pressed = false;
    this.btnPush = false;
    this.io_status = new Iostate();
    this.io_socket = new WebSocket(
      this.wsskomnav + this.param.localhost + "/api/io/socket",
      "json"
    );
    this.io_socket.onerror = function (event) {
      console.log("error iosocket");
    };

    this.io_socket.onopen = function (event) {
      console.log("io socket opened");

      this.onclose = function () {
        console.log("io socket closed");
      };
    };

    this.io_socket.onmessage = function (event) {
      var test = JSON.parse(event.data);
      apiserv.io_status.timestamp = test["Timestamp"];
      apiserv.io_status.dIn = test["DIn"];
      apiserv.io_status.aIn = test["AIn"];

      if (apiserv.io_status.dIn[2] || apiserv.io_status.dIn[8]) {
        //smart button and button A of the game pad
        if (!apiserv.b_pressed) {
          apiserv.b_pressed = true;
          apiserv.btnPush = true;
        }
      } else {
        if (apiserv.b_pressed) {
          apiserv.b_pressed = false;
        }
      }
    };

    ///////////////////////// emotions socket
    this.emotion_status = new Emotion();

    this.emotion_socket = new WebSocket(
      "ws://localhost:7007/",
      "json"
    );

    this.emotion_socket.onerror = function (event) {
      console.log("error emotion socket");
    };

    this.emotion_socket.onopen = function (event) {
      console.log("emotion socket opened");

      this.onmessage = function (event) {
        var test = JSON.parse(event.data);
        apiserv.emotion_status.id = test["id"];
        apiserv.emotion_status.neutral = Math.round(test["neutral"] * 100);
        apiserv.emotion_status.sad = Math.round(test["sad"] * 100);
        apiserv.emotion_status.happy = Math.round(test["happy"] * 100);
        apiserv.emotion_status.anger = Math.round(test["anger"] * 100);
        apiserv.emotion_status.surprise = Math.round(test["surprise"] * 100);
      };

      this.onclose = function () {
        console.log("emotion socket closed");
      };
    };

      ///////////////////////// trajectory socket
      this.trajectory_status = new Trajectory();

      this.trajectory_socket = new WebSocket(
        this.wsskomnav + this.param.localhost + "/api/trajectory/socket",
        "json"
      );
  
      this.trajectory_socket.onerror = function (event) {
        console.log("error trajectory socket");
      };
  
      this.trajectory_socket.onopen = function (event) {
        console.log("trajectory socket opened");
  
        this.onmessage = function (event) {
          var test = JSON.parse(event.data);
          //console.log(test);
          apiserv.trajectory_status.Control = test["Control"];
          apiserv.trajectory_status.Status = test["Status"];
          apiserv.trajectory_status.DistanceCovered = test["DistanceCovered"];
        };
  
        this.onclose = function () {
          console.log("trajectory socket closed");
        };
      };

       //////////////////////////////// ROS TOPIC connection
     this.ros = new ROSLIB.Ros({
      url : this.wssros+this.param.robot.rosip
      });

      this.ros.on('connection', function() {
        console.log('Connected to ros websocket server.');
      });

      this.ros.on('error', function(error) {
        console.log('Error connecting to ros websocket server: ', error);
      });

      this.ros.on('close', function() {
        console.log('Connection to ros websocket server closed.');
      });

 
    //////////////////////////////// ROS TOPIC cam
    this.listener_camera = new ROSLIB.Topic({
      ros : this.ros,
      name :'facecog/face_detection_image/compressed',
      //name : '/top_webcam/image_raw/compressed',
      //name : '/fisheye_cam/image_raw',
      //name : '/D435_camera_FWD/color/image_raw',
      //name : '/fisheye_cam/compressed',

      messageType : 'sensor_msgs/CompressedImage'
    });

    //////////////////////////////// ROS TOPIC recoface
    this.listener_recoface = new ROSLIB.Topic({
      ros : this.ros,
      name : '/facecog/user_id',
      messageType : 'std_msgs/String'
    });
  
    }
  

  mailAddInformationWalk() {
    var now = new Date().toLocaleString("en-GB", {
      day: "numeric",
      month: "numeric",
      year: "numeric",
      hour: "numeric",
      minute: "numeric",
      second: "numeric",
    });
    return (
      "<br> Time : " +
      now +
      "<br> Serial number : " +
      this.param.serialnumber +
      "<br> Battery remaining: " +
      this.battery_status.remaining +
      "%" +
      "<br> Battery state : " +
      this.battery_status.status +
      "<br> Battery voltage : " +
      this.battery_status.voltage +
      "<br> Battery current : " +
      this.battery_status.current +
      "<br> Battery autonomy : " +
      this.battery_status.autonomy +
      "<br> Docking state : " +
      this.docking_status.status +
      "<br> Status Forward : " +
      this.anticollision_status.forward +
      "<br> Status Right : " +
      this.anticollision_status.right +
      "<br> Status Left : " +
      this.anticollision_status.left +
      "<br> Status Reverse : " +
      this.anticollision_status.reverse +
      "<br> Navigation state : " +
      this.navigation_status.status +
      "<br> Differential state : " +
      this.differential_status.status +
      "<br> Odometer : " +
      this.statistics_status.totalDistance +
      "<br> Position X : " +
      this.localization_status.positionx +
      "<br> Position Y : " +
      this.localization_status.positiony +
      "<br> Position T : " +
      this.localization_status.positiont +
      "<br> Application : 1" +
      "<br> Walker Status : " +
      this.walker_states.status +
      "<br> Walker Detect : " +
      this.walker_states.detected +
      "<br> Meter covered : " +
      this.metercovered +
      "<br>"
    );
  }
  mailAddInformationBasic() {
    var now = new Date().toLocaleString("en-GB", {
      day: "numeric",
      month: "numeric",
      year: "numeric",
      hour: "numeric",
      minute: "numeric",
      second: "numeric",
    });
    return (
      "<br> Time : " +
      now +
      "<br> Serial number : " +
      this.param.serialnumber +
      "<br> Battery remaining: " +
      this.battery_status.remaining +
      "%" +
      "<br> Battery state : " +
      this.battery_status.status +
      "<br> Battery voltage : " +
      this.battery_status.voltage +
      "<br> Battery current : " +
      this.battery_status.current +
      "<br> Battery autonomy : " +
      this.battery_status.autonomy +
      "<br> Docking state : " +
      this.docking_status.status +
      "<br> Status Forward : " +
      this.anticollision_status.forward +
      "<br> Status Right : " +
      this.anticollision_status.right +
      "<br> Status Left : " +
      this.anticollision_status.left +
      "<br> Status Reverse : " +
      this.anticollision_status.reverse +
      "<br> Navigation state : " +
      this.navigation_status.status +
      "<br> Differential state : " +
      this.differential_status.status +
      "<br> Odometer : " +
      this.statistics_status.totalDistance +
      "<br> Position X : " +
      this.localization_status.positionx +
      "<br> Position Y : " +
      this.localization_status.positiony +
      "<br> Position T : " +
      this.localization_status.positiont +
      "<br> Application : 1" +
      "<br>"
    );
  }

  checkrobot() {
    this.httpClient
      .get(this.httpskomnav + this.param.localhost + "/api/battery/state")
      .subscribe(
        (data) => {
          this.robotok = true;
        },
        (err) => {
          console.log(err);
          this.robotok = false;
        }
      );
  }


  jetsonOK() {
    this.checkInternet();
    this.alert.checkwifi(this.wifiok);
    this.httpClient
      .get(this.httpskomnav + this.param.localhost + "/api/battery/state", {
        observe: "response",
      })
    
      .subscribe(
        (resp) => {
          if (this.statusRobot === 2) {
            this.alert.appError(this.mailAddInformationBasic());
            document.location.reload(); //if error then refresh the page and relaunch websocket
          }
          if (resp.status === 200) {
            this.cpt_jetsonOK += 1;
            if (this.walker_states.status === 2) {
              // knee laser in error
              this.statusRobot = 1;
              this.connectionLost();
              this.walkercmdstopdetect(); //if we don't do that the knee laser will go wrong
            } else {
              this.statusRobot = 0; // everything is ok
              this.connect();
            }
          } else {
            this.cpt_jetsonOK += 1;
            if (this.cpt_jetsonOK > 5) {
              this.statusRobot = 2; //no connection
              this.connectionLost();
            }
          }
        },
        (err) => {
          console.log(err); //no connection
          this.cpt_jetsonOK += 1;
          if (this.cpt_jetsonOK > 5) {
            this.statusRobot = 2; //no connection
            this.connectionLost();
          }
        }
      );
  }

  async isConnectedInternet(): Promise<boolean> {
    try {
      const response = await fetch('http://localhost/ionicDB/internetping.php');
      const text = await response.text(); // Récupère le contenu texte de la réponse
      //console.log(text);
      
      // Analyse du texte pour déterminer la connectivité Internet
      return text.trim() === 'true'; // Renvoie true si le texte est 'true', sinon false
      
    } catch (error) {
      console.error('Error checking internet connectivity:', error);
      return false;
    }
  }
  checkInternet() {
    this.isConnectedInternet().then(connecte => {
      if (connecte) {
        //console.log("L'ordinateur est connecté à Internet");
        this.wifiok = true;
      } else {
        //console.log("L'ordinateur n'est pas connecté à Internet");
        this.wifiok = false;
      }
    });

  }

  walkercmdstart(speed: number) {
    // start legs enslavement
    console.log("walk assist start");
    var test = {
      Guided: true,
      Assist: true,
      Enable: true,
      MaxSpeed: speed,
      OriginX: this.selectWalkZone
    };

    var jsonstring = JSON.stringify(test);
    this.walker_socket.send(jsonstring);
  }

  walkercmdstartdetect() {//pause
    console.log("walk detect active");
    // start leg detection

    var test = {
      Guided: true,
      Assist: false,
      Enable: true,
    };

    var jsonstring = JSON.stringify(test);
    this.walker_socket.send(jsonstring);
  }



  walkercmdstopdetect() {
    console.log("walk total stop");
    // stop leg enslavement + detection
    var test = {
      Guided: false,
      Assist: false,
      Enable: false,
    };

    var jsonstring = JSON.stringify(test);
    this.walker_socket.send(jsonstring);
  }

  connectHttp() {
    this.httpClient
      .put(this.httpskomnav + this.param.localhost + "/api/docking/connect", {
        observe: "response",
      })
    
      .subscribe(
        (resp) => {
          console.log(resp);
          console.log("connectOK");
        },
        (err) => {
          console.log(err);
          console.log("connectPBM");
        }
      );
  }

  disconnectHttp() {
    this.httpClient
      .put(this.httpskomnav + this.param.localhost + "/api/docking/disconnect", {
        observe: "response",
      })
    
      .subscribe(
        (resp) => {
          console.log(resp);
          console.log("disconnectOK");
        },
        (err) => {
          console.log(err);
          console.log("disconnectPBM");
        }
      );
  }

  abortNavHttp() {
    // stop the navigation
    this.httpClient
      .put(this.httpskomnav + this.param.localhost + "/api/navigation/abort", {
        observe: "response",
      })
    
      .subscribe(
        (resp) => {
          console.log(resp);
          console.log("abortNavOK");
        },
        (err) => {
          console.log(err);
          console.log("abortNavPBM");
        }
      );
  }

  abortDockingHttp() {
    this.httpClient
      .put(this.httpskomnav + this.param.localhost + "/api/docking/abort", {
        observe: "response",
      })
    
      .subscribe(
        (resp) => {
          console.log(resp);
          console.log("abortdockingOK");
        },
        (err) => {
          console.log(err);
          console.log("abortdockingPBM");
        }
      );
  }

  reachHttp(poiname: string) {
    this.httpClient
      .put(
        this.httpskomnav +
        this.param.localhost +
        "/api/navigation/destination/name/" +
        poiname,
        { observe: "response" }
      )
    
      .subscribe(
        (resp) => {
          console.log(resp);
          console.log("reachOK");
        },
        (err) => {
          console.log(err);
          console.log("reachPBM");
        }
      );
  }

  getCurrentMap() {
    this.httpClient
      .get(this.httpskomnav + this.param.localhost + "/api/maps/current/properties")
      .subscribe(
        (data) => {
          //console.log(data);
          this.mapdata = data;
          //console.log(this.mapdata.Id);
          if (this.mapdata) {
            this.id_current_map = this.mapdata.Id;
            this.name_current_map = this.mapdata.Name;
          }
          console.log("get current map ok");
        },
        (err) => {
          console.log(err);
        }
      );
  }

  ngOnInit() { }


  connect() {
    this.is_connected = true;
  }

  connectionLost() {
    this.is_connected = false;
  }

  okToast(m: string, n: number) {
    const toast = this.toastCtrl.create({
      message: m,
      duration: n,
      position: "middle",
      cssClass: "toastam",
    });
    toast.present();
  }

  getCurrentLocations() {
    //avoir les poi de la map actuelle
    this.httpClient
      .get(this.httpskomnav + this.param.localhost + "/api/maps/current/locations")
      .subscribe(
        (data) => {
          console.log("get locations ok");
          this.all_locations = data;
          //console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
  }

  // pour affichage map

  getImgMapHttp(): Observable<Blob> {
    return this.httpClient.get(
      this.httpskomnav + this.param.localhost + "/api/maps/current/image",
      { responseType: "blob" }
    );
  }

  getRoundList() {
    this.httpClient
      .get(this.httpskomnav + this.param.localhost + "/api/rounds/list")
      .subscribe(
        (data) => {
          this.rounddata = data;
          if (this.id_current_map && this.rounddata) {
            this.rounds_current_map = this.rounddata.filter(
              (round) => round.Map === this.id_current_map
            );
          }
        },
        (err) => {
          console.log(err);
        }
      );
  }

  launchRound() {
    this.httpClient
      .put(
        this.httpskomnav +
        this.param.localhost +
        "/api/exercise/start/" +
        this.selectRound,
        { observe: "response" }
      )
    
      .subscribe(
        (resp) => {
          console.log(resp);
          console.log("launchROK");
        },
        (err) => {
          console.log(err);
          console.log("launchRPBM");
        }
      );
  }

  pauseRound() {
    this.httpClient
      .put(
        this.httpskomnav +
        this.param.localhost +
        "/api/exercise/pause",
        { observe: "response" }
      )
    
      .subscribe(
        (resp) => {
          console.log(resp);
          console.log("pauseROK");
        },
        (err) => {
          console.log(err);
          console.log("pauseRPBM");
        }
      );
  }

  resumeRound() {
    this.httpClient
      .put(
        this.httpskomnav +
        this.param.localhost +
        "/api/exercise/resume",
        { observe: "response" }
      )
    
      .subscribe(
        (resp) => {
          console.log(resp);
          console.log("resumeROK");
        },
        (err) => {
          console.log(err);
          console.log("resumeRPBM");
        }
      );
  }


  puttrajectory(x1:number,y1:number,x2:number,y2:number) {

    const body = JSON.stringify([{

      Start:
      {
        X: x1,
        Y: y1
      },
      End:
      {
        X: x2,
        Y: y2
      },
      MaxSpeed: 1
    }]);
    console.log(body);
    this.httpClient
      .put(this.httpskomnav + this.param.localhost + "/api/trajectory/follow", body)
      .subscribe(
        (response) => {
          console.log(response);
          console.log("trajectory ok");
        },
        (err) => {
          console.log(err);
        }
      );

  }

  putStopDetecthttp() {
    const body = JSON.stringify({
        Guided: false,
        Assist: false,
        Enable: false,
      }
    );
    console.log(body);
    this.httpClient
      .put(this.httpskomnav + this.param.localhost + "/api/walker/command", body)
      .subscribe(
        (response) => {
          console.log(response);
          console.log("stop detect ok");
        },
        (err) => {
          console.log(err);
        }
      );

  }

  launchreco(start:Boolean){
    console.log("reco launch");

    var serviceRecoface = new ROSLIB.Service({
      ros : this.ros,
      name : '/toggle_face_recognition_node',
      serviceType : 'std_srvs/SetBool'
    });
  
    var request = new ROSLIB.ServiceRequest({
      data:start,
    });
  
    serviceRecoface.callService(request, function(result) {
      console.log("*********service recoface****************")
      console.log(result);
    });

    if(start){
      console.log("reco true");
      this.listener_recoface.subscribe((message) => {
        //console.log('Received message on ' + this.listener_recoface.name + ': ' + message.data);
        this.preco=message.data;
      });
    }else{
      console.log("reco false");
      this.listener_recoface.unsubscribe();
      this.listener_camera.unsubscribe();
    }
  }

  addUserJetson(name:string){
    console.log("adduserjet");

    var serviceAddUser = new ROSLIB.Service({
      ros : this.ros,
      name : '/add_user_data',
      serviceType : 'kompai_ros/add_user_data_server'
    });
  
    var request = new ROSLIB.ServiceRequest({
      user_id_add:name,
    });
  
    var apiserv = this;
    serviceAddUser.callService(request, function(result) {
      console.log("*********service add user****************")
      console.log(result.result);
      if(result.result){
        apiserv.useradded=true;
      }
    });
  }

  deleteUserJetson(name:string){
    var serviceDeleteUser = new ROSLIB.Service({
      ros : this.ros,
      name : '/remove_user',
      serviceType : 'kompai_ros/add_user_data_server'
    });
  
    var request = new ROSLIB.ServiceRequest({
      user_id_add:name,
    });
  
    serviceDeleteUser.callService(request, function(result) {
      console.log("*********service delete user****************")
      console.log(result);
    });
  }

}
