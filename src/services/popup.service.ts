// to send mail and sms alert

import { Injectable, OnInit } from "@angular/core";
import { App } from "ionic-angular";
import { AlertService } from "./alert.service";
import { AlertController, Alert,ToastController } from "ionic-angular";
import { ApiService } from "./api.service";
import { ParamService } from "./param.service";

@Injectable()
export class PopupService implements OnInit {
  alert_blocked: Alert;
  private accessparam: () => void;
  private accesswalklog: () => void;
  private addMail: (ev) => void;
  onSomethingHappened1(fn : () => void) {
    this.accessparam = fn;
  }
  onSomethingHappened2(fn : () => void) {
    this.addMail = fn;
  }
  onSomethingHappened3(fn : () => void) {
    this.accesswalklog = fn;
  }

  constructor(
    public app: App,
    public alert: AlertService,
    public param: ParamService,
    public api: ApiService,
    private alertCtrl: AlertController,
    public toastCtrl:ToastController
  ) {}

  ngOnInit() {}

  startFailedAlert() {
    this.alert_blocked = this.alertCtrl.create({
      title: this.param.datatext.statusRedPresent_title,
      message: this.param.datatext.cantstart,
      cssClass: "alertstyle",
      enableBackdropDismiss: false,
      buttons: [
        {
          text: this.param.datatext.btn_ok,
          handler: () => {
            console.log("Oui clicked");
            window.close();
          },
        },
      ],
    });
    this.alert_blocked.present();
  }

  leaveDockingAlert() {
    // pop up if the robot is in the docking when you start the round
    let alert = this.alertCtrl.create({
      title: this.param.datatext.leaveDockingAlert_title,
      message: this.param.datatext.leaveDockingAlert_message,
      cssClass: "alertstyle",
      buttons: [
        {
          text: this.param.datatext.btn_ok,
          handler: () => {
            console.log("OK clicked");
          },
        },
      ],
    });

    alert.present();
  }

  errorNavAlert() {
    this.alert_blocked = this.alertCtrl.create({
      title: this.param.datatext.errorNavAlert_title,
      message: this.param.datatext.errorNavAlert_message,
      cssClass: "alertstyle",
      enableBackdropDismiss: true,
      buttons: [
        {
          text: this.param.datatext.btn_ok,
          handler: () => {
            //console.log('Oui clicked');
          },
        },
      ],
    });
    this.alert_blocked.present();
  }

  blockedAlert() {
    this.alert_blocked = this.alertCtrl.create({
      title: this.param.datatext.blockedAlert_title,
      message: this.param.datatext.blockedAlert_message,
      cssClass: "alertstyle",
      enableBackdropDismiss: true,
      buttons: [
        {
          text: this.param.datatext.btn_ok,
          handler: () => {
            console.log("Oui clicked");
          },
        },
      ],
    });
    this.alert_blocked.present();
  }

  quitConfirm() {
    let alert = this.alertCtrl.create({
      title: this.param.datatext.quitConfirm_title,
      cssClass: "alertstyle",
      enableBackdropDismiss: false,
      buttons: [
        {
          text: this.param.datatext.btn_no,
          role: "cancel",
          handler: () => {
            //console.log('Non clicked');
          },
        },
        {
          text: this.param.datatext.btn_yes,
          handler: () => {
            //console.log('Oui clicked');
            this.api.close_app = true;
            this.alert.appClosed(this.api.mailAddInformationBasic());
            //stop the round and quit the app
            this.api.abortNavHttp();
            setTimeout(() => {
              window.close();
            }, 1000);
          },
        },
      ],
    });
    alert.present();
  }

  lowBattery() {
    let alert = this.alertCtrl.create({
      title: this.param.datatext.lowBattery_title,
      message: this.param.datatext.lowBattery_message,
      cssClass: "alertstyle",
      enableBackdropDismiss: false,
      buttons: [
        {
          text: this.param.datatext.btn_no,
          role: "cancel",
          handler: () => {
            //console.log('Non clicked');
            this.api.towardDocking = false;
          },
        },
        {
          text: this.param.datatext.btn_yes,
          handler: () => {
            console.log("Oui clicked");
            this.api.towardDocking = true;
            //console.log(this.api.towardDocking=true);
            this.api.reachHttp("docking");
            this.alert.askCharge(this.api.mailAddInformationBasic());
          },
        },
      ],
    });
    alert.present();
  }

  errorBlocked() {
    let alert = this.alertCtrl.create({
      title: this.param.datatext.errorBlocked_title,
      message: this.param.datatext.errorBlocked_message,
      cssClass: "alertstyle",
      enableBackdropDismiss: true,
      buttons: [
        {
          text: this.param.datatext.btn_ok,
          handler: () => {
            //console.log('OK clicked');
          },
        },
      ],
    });
    alert.present();
  }

  lostAlert() {
    this.alert_blocked = this.alertCtrl.create({
      title: this.param.datatext.lostAlert_title,
      message: this.param.datatext.lostAlert_message,
      cssClass: "alertstyle",
      enableBackdropDismiss: true,
      buttons: [
        {
          text: this.param.datatext.btn_ok,
          handler: () => {
            //console.log('Oui clicked');
          },
        },
      ],
    });
    this.alert_blocked.present();
  }

  robotmuststayondocking() {
    // pop up if the robot is in the docking when you start the round
    this.alert_blocked = this.alertCtrl.create({
      title: this.param.datatext.robotmuststayondocking_title,
      message: this.param.datatext.robotmuststayondocking_message,
      cssClass: "alertstyle",
      enableBackdropDismiss: true,
      buttons: [
        {
          text: this.param.datatext.btn_ok,
          handler: () => {
            //console.log('Oui clicked');
          },
        },
      ],
    });
    this.alert_blocked.present();
  }

  goDockingConfirm() {
    // pop up to ask if the robot must go to the docking
    let alert = this.alertCtrl.create({
      title:
        this.param.datatext.goDockingConfirm_title +
        this.api.battery_status.remaining +
        "%",
      message: this.param.datatext.goDockingConfirm_message,
      cssClass: "alertstyle",
      enableBackdropDismiss: false,
      buttons: [
        {
          text: this.param.datatext.btn_no,
          role: "cancel",
          handler: () => {
            //console.log('Non clicked');
            this.api.walkercmdstartdetect();
            this.api.towardDocking = false;
          },
        },
        {
          text: this.param.datatext.btn_yes,
          handler: () => {
            //console.log('Oui clicked');
            this.api.towardDocking = true;
            this.api.reachHttp("docking");
            this.alert.askCharge(this.api.mailAddInformationBasic());
          },
        },
      ],
    });
    alert.present();
  }

  statusRedPresent() {
    let alert = this.alertCtrl.create({
      title: this.param.datatext.statusRedPresent_title,
      message: this.param.datatext.statusRedPresent_message,
      cssClass: "alertstyle",
      enableBackdropDismiss: true,
      buttons: [
        {
          text: this.param.datatext.btn_ok,
          handler: () => {
            console.log("OK clicked");
          },
        },
      ],
    });
    alert.present();
  }

  statusGreenPresent() {
    let alert = this.alertCtrl.create({
      title: this.param.datatext.statusGreenPresent_title,
      message: this.param.datatext.statusGreenPresent_message,
      cssClass: "alertstyle",
      enableBackdropDismiss: true,
      buttons: [
        {
          text: this.param.datatext.btn_ok,
          handler: () => {
            console.log("OK clicked");
          },
        },
      ],
    });
    alert.present();
  }

  askHelp() {
    // pop up displayed when someone push the sos btn
    let alert = this.alertCtrl.create({
      title: this.param.datatext.askHelp_title,
      message: this.param.datatext.askHelp_message,
      cssClass: "alertstyle",
      enableBackdropDismiss: true,
      buttons: [
        {
          text: this.param.datatext.btn_ok,
          handler: () => {
            console.log("OK clicked");
          },
        },
      ],
    });
    alert.present();
  }

  leaveDockingConfirm() {
    // pop up to ask if the robot must leave the docking
    let alert = this.alertCtrl.create({
      title:
        this.param.datatext.leaveDockingConfirm_title +
        this.api.battery_status.remaining +
        "%",
      message: this.param.datatext.leaveDockingConfirm.mes,
      cssClass: "alertstyle",
      enableBackdropDismiss: true,
      buttons: [
        {
          text: this.param.datatext.btn_no,
          role: "cancel",
          handler: () => {
            console.log("Non clicked");
          },
        },
        {
          text: this.param.datatext.btn_yes,
          handler: () => {
            console.log("Oui clicked");
            this.api.disconnectHttp();
            this.alert.leaveDocking(this.api.mailAddInformationBasic());
            setTimeout(() => {
              this.api.walkercmdstartdetect();
            }, 8000);
          },
        },
      ],
    });
    alert.present();
  }

  askpswd() {
    let alert = this.alertCtrl.create({
      title: this.param.datatext.password,
      message: this.param.datatext.enterPassword,
      cssClass: "alertstyle",
      enableBackdropDismiss: true,
      inputs: [
        {
          name: "password",
          placeholder: this.param.datatext.password,
          type: "password",
        },
      ],
      buttons: [
        {
          text: this.param.datatext.btn_cancel,
          role: "cancel",
          handler: (data) => {
            console.log("Cancel clicked");
          },
        },
        {
          text: this.param.datatext.btn_save,
          role: "backdrop",
          handler: (data) => {
            if (data.password === atob(this.param.robot.password) ) {

              this.accessparam();
            } else {
              this.wrongPassword();
            }
          },
        },
      ],
    });
    alert.present();
  }

  wrongPassword() {
    let alert = this.alertCtrl.create({
      title: this.param.datatext.password,
      message: this.param.datatext.wrongPassword,
      cssClass: "alertstyle_wrongpass",
      enableBackdropDismiss: true,
      inputs: [
        {
          name: "password",
          placeholder: this.param.datatext.password,
          type: "password",
        },
      ],
      buttons: [
        {
          text: this.param.datatext.btn_cancel,
          role: "cancel",
          handler: (data) => {
            console.log("Cancel clicked");
          },
        },
        {
          text: this.param.datatext.btn_save,
          role: "backdrop",
          handler: (data) => {
            if (data.password === atob(this.param.robot.password)){
              this.accessparam();
            } else {
              this.wrongPassword();
            }
          },
        },
      ],
    });
    alert.present();
  }

  askpswdWalk() {
    let alert = this.alertCtrl.create({
      title: this.param.datatext.password,
      message: this.param.datatext.enterPassword,
      cssClass: "alertstyle",
      enableBackdropDismiss: true,
      inputs: [
        {
          name: "password",
          placeholder: this.param.datatext.password,
          type: "password",
        },
      ],
      buttons: [
        {
          text: this.param.datatext.btn_cancel,
          role: "cancel",
          handler: (data) => {
            console.log("Cancel clicked");
          },
        },
        {
          text: this.param.datatext.btn_save,
          role: "backdrop",
          handler: (data) => {
            if (data.password === atob(this.param.robot.password) ) {

              this.accesswalklog();
            } else {
              this.wrongPasswordWalk();
            }
          },
        },
      ],
    });
    alert.present();
  }

  wrongPasswordWalk() {
    let alert = this.alertCtrl.create({
      title: this.param.datatext.password,
      message: this.param.datatext.wrongPassword,
      cssClass: "alertstyle_wrongpass",
      enableBackdropDismiss: true,
      inputs: [
        {
          name: "password",
          placeholder: this.param.datatext.password,
          type: "password",
        },
      ],
      buttons: [
        {
          text: this.param.datatext.btn_cancel,
          role: "cancel",
          handler: (data) => {
            console.log("Cancel clicked");
          },
        },
        {
          text: this.param.datatext.btn_save,
          role: "backdrop",
          handler: (data) => {
            if (data.password === atob(this.param.robot.password)){
              this.accesswalklog();
            } else {
              this.wrongPasswordWalk();
            }
          },
        },
      ],
    });
    alert.present();
  }


  displayrgpd(ev) {
    ev.preventDefault();
    let alert = this.alertCtrl.create({
      title: "RGPD",
      message: this.param.datatext.rgpd_txt,
      cssClass: "alertstyle",
      enableBackdropDismiss: true,

      buttons: [
        {
          text: this.param.datatext.btn_cancel,
          role: "cancel",
          handler: (data) => {
            console.log("Cancel clicked");
          },
        },
        {
          text: this.param.datatext.btn_save,
          role: "backdrop",
          handler: (data) => {
              this.addMail(ev);
            }
          },

      ],
    });
    alert.present();
  }

  relocAlert() {
    this.alert_blocked = this.alertCtrl.create({
      title: this.param.datatext.relocAlert_title,
      message: this.param.datatext.relocAlert_message,
      cssClass: "alertstyle",
      enableBackdropDismiss: false,
      buttons: [
        {
          text: this.param.datatext.btn_ok,
          handler: () => {
            console.log("Oui clicked");
            window.close();
          },
        },
      ],
    });
    this.alert_blocked.present();
  }

  async showToast(msg,duration,position) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: duration,
      position: position,//top, middle, bottom
      cssClass:"toastok"
    });
    toast.present();
  }
  
  async showToastRed(msg,duration,position) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: duration,
      position: position,//top, middle, bottom
      cssClass:"toast"
    });
    toast.present();
  }
}
