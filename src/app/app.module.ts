import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { ComponentsModule } from '../components/components.module';
import { MyApp } from './app.component';
import { FirstPage } from '../pages/first/first';
import { RecofacePage } from '../pages/recoface/recoface';
import { WalkPage } from '../pages/walk/walk';
import { HeadpageComponent } from '../components/headpage/headpage';
import { ChooseAssistancePage } from '../pages/choose_assistance/choose_assistance';
import { ApiService } from '../services/api.service';
import { HttpClientModule } from '@angular/common/http';
import { TutoPage } from '../pages/tuto/tuto';
import { PopupService } from '../services/popup.service';
import { AlertService } from '../services/alert.service';
import { ParamService } from '../services/param.service';
import { SpeechService } from '../services/speech.service';
import { ParamPage } from '../pages/param/param';
import { LanguePage } from '../pages/langue/langue';
import { SMSPage } from '../pages/sms/sms';
import { MailPage } from '../pages/mail/mail';
import { StartAssistancePage } from '../pages/start_assistance/start_assistance';
import { PasswordPage } from '../pages/password/password';
import { WalkLogsPage } from '../pages/walklogs/walklogs';
import { UserPage } from '../pages/user/user';

@NgModule({
  declarations: [
    MyApp,
    WalkPage,
    FirstPage,
    RecofacePage,
    ChooseAssistancePage,
    StartAssistancePage,
    PasswordPage,
    TutoPage,
    LanguePage,
    MailPage,
    SMSPage,
    ParamPage,
    WalkLogsPage,
    UserPage


  ],
  imports: [
    BrowserModule,
    ComponentsModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    WalkPage,
    FirstPage,
    RecofacePage,
    ChooseAssistancePage,
    StartAssistancePage,
    TutoPage,
    ParamPage,
    PasswordPage,
    LanguePage,
    MailPage,
    SMSPage,
    WalkLogsPage,
    UserPage,
    HeadpageComponent

  ],
  providers: [
    StatusBar,
    SplashScreen,
    ApiService,
    PopupService,
    AlertService,
    ParamService,
    SpeechService,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
