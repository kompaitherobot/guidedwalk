import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { FirstPage } from '../pages/first/first';
import { ApiService } from '../services/api.service';
import { AlertService } from '../services/alert.service';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = FirstPage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen,public api: ApiService, public alert: AlertService) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();

      platform.pause.subscribe(() => {
        // stop the enslavement of the legs if we do something else on the robot tablet
        //this.api.walkercmdstopdetect();
        this.api.abortNavHttp();
        window.close();
    });
    });
  }
}

